<?php
function password_codificado($aux_password)
{
	$salt = cadena_aleatoria(64);
	$hash = hash("sha256", $salt . $aux_password);
	$password = $salt . $hash;
	return $password;
}
function comparar_clave($passwordIntroducido, $passwordBD)
{
	$salt = substr($passwordBD, 0, 64);
	$validHash = substr($passwordBD, 64, 64);
	$testHash = hash("sha256", $salt.$passwordIntroducido);
	return ($validHash == $testHash) ? true : false;
}

function generar_sesion($datos_usuario){
    $_SESSION['logueo'] = '';
    $_SESSION['logueo']['id_usuario'] = $datos_usuario['id_usuario'];
    $_SESSION['logueo']['nombre'] = $datos_usuario['nombre'];
    $_SESSION['logueo']['apellidos'] = $datos_usuario['apellidos'];
    $_SESSION['logueo']['email'] = $datos_usuario['email'];
    $_SESSION['logueo']['rol'] = $datos_usuario['rol_nombre'];
    $_SESSION['logueo']['siglas'] = $datos_usuario['rol_siglas'];
}
?>
