<?php
/**
 * Created by PhpStorm.
 * User: chema
 * Date: 05/06/2018
 * Time: 19:23
 */
include("../../includes/include_base2.php");
include('../../functions/php/functions.php');

function guardar($parametros)
{
    $web='';
    $link = $GLOBALS['link'];
    $update_user = $link->prepare("UPDATE usuario SET nombre=:nombre, apellidos=:apellidos, email=:email, telefono=:telefono WHERE id_usuario = :id");
    try
    {
        $link->beginTransaction();
        $update_user->execute(array(':id'=>$parametros['id'],':nombre'=>$parametros['nombre'],':apellidos'=>$parametros['apellidos'], ':email'=>$parametros['email'],':telefono'=>$parametros['telefono']));
        $web.="<script>getInformation('user',1);";
        $link->commit();
	}
	catch(PDOException $e)
    {
        $link->rollBack();
        $aux_texto=_("Se ha producido un error al tratar de guardar la información en la base de datos").': ';
        $web.='<script>alert("'.$aux_texto.$e->getMessage() . '");';
        $web.='<script>error_sistema("'.$e->getMessage(). ', '.$e->getFile(). ', '.$e->getLine(). '");';

    }
	return $web;
}
function informacion($parametros){
    $web= '';
    if($parametros['number']  === '1')
    {
        $web .= '   <div class="avisos_legales">
                    <br><h1>Aviso Cookies</h1>
                    <hr class="hr">
                    <div class="group">
                    <p>A cookie is a file that is downloaded to and stored on a user\'s computer when accessing certain websites, in order to store and retrieve information regarding the browsing done from that computer.
                        Cookies are essential for the operation of the internet, providing benefits for the provision of interactive services and facilitating the navigation and usability of our website.
                        Note that cookies cannot damage your computer and, instead, those that are enabled help us to identify and resolve errors.</p>
                    <br>
                    <p>GestApp S.L. uses the following cookies detailed below on this website:</p>
                        <p> - Cookies that are strictly necessary for providing certain services specifically requested by the user and that may help you to navigate properly or ensure that the content of a web page loads effectively. If disabled, the user will not be able to receive the specified services and content correctly.</p>
                        <p> - Own cookies: these are sent to the user’s computer terminal from a computer or domain managed by the editor and from which the service requested by the user is provided.</p>
                        <p> - Third-party cookies: these are sent to the user’s computer terminal from a computer or domain that is not managed by the editor, but by another entity that treats the data obtained through cookies.</p>
                        <p> - Analytical cookies: for monitoring and collecting statistical data on the activity, with the aim of ensuring the best service for the user.</p>
                        <p> - Advertising cookies: for managing advertising spaces based on criteria such as the frequency with which ads are displayed.</p>
                    <br>
                     <p>If you continue to navigate after being informed about the cookies policy, we will understand that you accept the use of cookies by our website and applications. However, if you want, you can change the cookies settings at any time by configuring your browser to accept the cookies it receives or not, or get the browser to notify you when a server wants to save a cookie.
                        We should inform you that if you block or do not accept the installation of cookies, it is possible that certain services may not be available without their use or you may not be able to access certain services or take full advantage of everything that our websites and applications offer.
                        GestApp appreciates you agreeing to use and accept cookies, since this helps us to obtain more precise data that allow us to improve the content and design of our websites and applications to suit your preferences.
                        If you agree to use cookies, continue to browse or click on a link, it shall be understood that you accept our cookies policy and therefore the installation of cookies on your computer or device.</p>
                    </div>
                    <div class="group">
                        <input type="button" style="margin-top: 20px;" value="Descargar" onclick="openPDF(1)";
                    </div>
                    </div>';
    }
    elseif ($parametros['number']  === '2')
    {
        $web .= '   <div class="avisos_legales">
                    <br><h1>Política Privacidad</h1>
                    <hr class="hr">
                    <div class="group">
                        <p>De conformidad con lo establecido en la normativa vigente en Protección de Datos de Carácter Personal, le informamos que sus datos serán incorporados al sistema de tratamiento titularidad de GestApp SL con CIF B39------ y domicilio social en Doctor Madrazo 4 - 2º A Astillero (CANTABRIA), con la finalidad de poder remitirle la correspondiente factura. En cumplimiento con la normativa vigente, GestApp SL informa que los datos serán conservados durante el plazo legalmente establecido.</p>
                        <br><p>Con la presente cláusula queda informado de que sus datos serán comunicados en caso de ser necesario a: Administraciones públicas y a todas aquellas entidades con las que sea necesaria la comunicación con la finalidad de cumplir con la prestación del servicio anteriormente mencionado.</p>
                        <br><p>El hecho de no facilitar los datos a las entidades mencionadas implica que no se pueda cumplir con las prestación de los servicios.</p>
                        <br><p>A su vez, le informamos que puede contactar con el Delegado de Protección de Datos de GestApp SL, dirigiéndose por escrito a la dirección de correo  dpo.cliente@conversia.es o al teléfono 902877192.</p>
                        <br><p>GestApp SL informa que procederá a tratar los datos de manera lícita, leal, transparente, adecuada, pertinente, limitada, exacta y actualizada. Es por ello que  GestApp SL se compromete a adoptar las medidas razonables para que estos se supriman o rectifiquen sin dilación cuando sean inexactos.</p>
                        <br><p>Podrá ejercer los derechos de acceso, rectificación, limitación de tratamiento, supresión, portabilidad y oposición/revocación, en los términos que establece la normativa vigente en materia de protección de datos, dirigiendo su petición a la dirección postal Doctor Madrazo 4 - 2º A Astillero (CANTABRIA) o bien a través de correo electrónico admon@gestapp.com.</p>
                        <br><p>Podrá dirigirse a la Autoridad de Control competente para presentar la reclamación que considere oportuna.</p>
                    </div>
                     <div class="group">
                        <input type="button" style="margin-top: 20px; " value="Descargar" onclick="openPDF(2)";
                    </div>
                    </div>';
    }
    elseif ($parametros['number']  === '3')
    {
        $web .= '   <div class="avisos_legales">
                    <br><h1>Aviso Legal</h1>
                    <hr class="hr">
                    <div class="group">
                        <p>GestApp,S.L. está domiciliada en la calle C/Doctor Madrazo 4, 2ºa de Astillero Cantabria , con CIF  B39------. Inscrita en el Registro Mercantil de Santander ,Tomo 688 Folio 100 S10263 con correo electrónico  <a href="#">admon@gestapp.com</a>.</p>
                        <br><p>GestApp,S.L. se reserva el derecho a actualizar, modificar o eliminar la información contenida en su web site, y la configuración o presentación del mismo, en cualquier momento, sin previo aviso, y sin asumir responsabilidad alguna por ello. </p>
                        <br><p>En la web de GestApp,S.L. <a href="#">https://gestapp.com/</a> hay una serie de contenidos de carácter informativo sobre nuestra empresa y correspondiendo el ejercicio exclusivo de los derechos de explotación de los mismos. Por tanto queda prohibida su reproducción, distribución, comunicación pública y transformación, total o parcial, sin la autorización expresa de Trámites Fáciles Santander S.L.L. Igualmente, todos los nombres comerciales, marcas o signos distintos de cualquier clase contenidos en este web site están protegidos por ley. </p>
                        <br><p>Su principal objetivo es facilitar a los clientes y al público en general, la información relativa a la empresa, a los productos y servicios que se ofrecemos reservandose el derecho a actualizar, modificar o eliminar la información contenida en su web site, y la configuración o presentación del mismo, en cualquier momento, sin previo aviso, y sin asumir responsabilidad alguna por ello.  </p>
                    </div>
                    <div class="group">
                        <input type="button" style="margin-top: 20px; " value="Descargar" onclick="openPDF(3)";
                    </div>
                    </div>';
    }
    elseif ($parametros['number']  === '4')
    {
        $web .= '   <div class="avisos_legales">
                    <br><h1>Ayuda de la Aplicación</h1>
                    <hr class="hr">
                    <div class="group">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed augue et lorem accumsan suscipit. Curabitur placerat, tellus eu maximus sollicitudin, nulla quam vehicula nibh, quis tempus eros urna vitae lacus. Praesent rutrum malesuada consequat. Aenean maximus, diam quis volutpat porttitor, enim nibh pellentesque neque, eget tempus felis ipsum vel metus. Fusce ut felis ante. Curabitur egestas mattis consectetur. Praesent vel ipsum ut erat commodo imperdiet. Proin facilisis enim tortor, non suscipit purus faucibus et. Nunc porta justo semper mauris accumsan pharetra. Maecenas tempus dignissim scelerisque. Aenean enim eros, imperdiet eu metus at, dignissim accumsan tortor.</p>
                        <br><p>Vivamus euismod nibh non felis sagittis faucibus. Maecenas blandit metus nec porta sodales. In pulvinar justo ex, laoreet gravida diam sollicitudin sed. Fusce blandit iaculis nibh ut posuere. Integer purus ligula, commodo a orci non, hendrerit dignissim dui. Ut id diam ac lacus tristique sodales. Fusce rhoncus urna ornare, placerat sem eget, consequat libero. Sed porttitor vel sem at viverra.</p>
                        <br><p>Nam faucibus gravida tellus, nec vehicula est. Vestibulum eget suscipit enim, sed vehicula ligula. Cras suscipit et mi sed pretium. Suspendisse nisi ex, mollis sed lorem vitae, posuere volutpat quam. Nunc eget mauris lorem. Fusce ultricies ante id mauris congue, at pulvinar odio tempor. Donec vestibulum nisl lectus, vitae vehicula nibh sollicitudin vel. Suspendisse nibh felis, posuere eu lorem a, imperdiet mollis leo. In luctus dolor a ligula tincidunt, eu bibendum dolor hendrerit. Praesent tempus quis urna vel auctor.</p>
                        <br><p>Praesent at odio ac enim tincidunt mattis. In aliquet, est ut auctor porta, magna libero ornare dui, a dictum quam massa efficitur nulla. Proin maximus a turpis eu molestie. Vestibulum rutrum condimentum dui non consequat. Suspendisse ut nibh vel odio finibus ornare. Cras dapibus imperdiet convallis. Aliquam hendrerit, metus eu fermentum pharetra, massa nulla dignissim mi, et blandit velit massa quis eros. Praesent facilisis non est eget vestibulum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vel tellus pharetra, vestibulum elit eget, accumsan lorem. Pellentesque finibus tempus nisi, eget suscipit arcu vestibulum sit amet. Etiam ac odio nec nunc tincidunt mattis. Phasellus convallis mi eu sodales laoreet. Quisque arcu felis, sollicitudin eget posuere sed, faucibus eu nibh. Phasellus tristique dignissim dignissim. Cras porta nunc eu augue blandit, quis vulputate orci cursus.</p>
                    </div>
                   <div class="group">
                        <input type="button" style="margin-top: 20px; " value="Descargar" onclick="openPDF(4)";
                    </div>                    
                    </div>';
    }
    return $web;
}
function contact(){
    $web= '';
    $web .= '   <div class="avisos_legales">
                    <br><h1>Formulario de Contacto</h1>
                    <hr class="hr">
                        <div><br>
                          <form action="#">
                            <label for="name">Tu Nombre</label>
                            <input type="text" id="name" name="name">
                        
                            <label for="surname">Tus Apellidos</label>
                            <input type="text" id="surname" name="surname">
                            
                            <label for="email">Tu Email</label>
                            <input type="email" id="email" name="email">
                        
                            <label for="theme">Tus</label>
                            <select id="theme" name="theme">
                              <option value="theme">Seleccione</option>
                              <option value="improvements">Mejoras</option>
                              <option value="problems">Problemas</option>
                              <option value="others">Otros</option>
                            </select>
                            
                            <label for="message">Tus comentarios</label>
                            <textarea id="message" name="message"></textarea>
                            
                            <p class="alt"><input type="checkbox" id="condiciones" name="condiciones"/><span class="error_legal"> He leido las <a href="#" onclick="showInformation(2)">condiciones de uso</a> y hago constar que acepto dichas condiciones.</span></p></br></br>
                          
                            <input type="button" value="Enviar" onclick="recogerDatos()">
                          </form>
                        </div>
                    </div>';
    return $web;
}
function sendContact($parametros){
    $web ='';
    $exito = enviar_email('localhost', $smtpauth=1, 'gestappsl@gmail.com', 'password', $parametros['email'], $parametros['name'], $destinatariosTo=array(), $destinatariosCC=array(), $destinatariosBCC=array(), $parametros['theme'], $parametros['message'], $adjuntos=array(), $replyTo='', $mandrill=1);
    if ($exito === 'ok') {
        $web .= '<br><div class="avisos_legales"><br>';
        $web .= '<br><h1>Formulario de Contacto</h1><br>';
        $web .= '<br></hr><br>';
        $web .= '<br><h1>El mensaje se ha enviado correctamente, en breve nos pondremos en contacto.</h1><br></div>';
    } else {
        $web .= '<br><div class="avisos_legales"><br>';
        $web .= '<br><h1>Formulario de Contacto</h1><br>';
        $web .= '<br></hr><br>';
        $web .= '<br><h1>Ha habido un error al enviar el mensaje, por favor inténtelo de nuevo.</h1></div><br>';
    }
    return $web;
}
function crear($parametros)
{
    $web= '';
    if($parametros['number']  === '1')
    {
        $web .= "<div class='avisos_legales'>";
        $web .= "<form id='form_crearProyecto' name='form_crearProyecto' action='#' method='POST'>";
        $web .= "<br><h1>Crear Proyecto</h1>
                        <div><br>
                          <form action='#'>
                            <label for='nameProject'>Nombre:</label>
                            <input type='text' id='nameProject' name='nameProject'>
                        
                            <label for='description'>Descripción:</label>
                            <input type='text' id='description' name='description'>
                            
                            <label for='fecha_ini'>Fecha Inicio:</label>
                            <input type='date' id='fecha_ini' name='fecha_ini'>
                        
                            <label for='fecha_fin'>Fecha Fin:</label>
                            <input type='date' id='fecha_fin' name='fecha_fin'>
                                                    
                            <input type='button' value='Crear' onclick='createProject()'>
                          </form>
                        </div>";
    }
    else if($parametros['number']  === '2')
    {
        $link = $GLOBALS['link'];
        try
        {
            $link->beginTransaction();
            $id = $_SESSION['login']['id_usuario'];
            $categories = $link->prepare("select * from categoria where activo = 1");
            $priorities = $link->prepare("select * from prioridad where activo = 1");
            $states = $link->prepare("select * from estado where activo = 1");
            $projects = $link->prepare("select * from proyecto p where activo = 1 and p.usuario_id = :id");

            $web .= "<div class='avisos_legales'>";
            $web .= "<form id='form_crearTarea' name='form_crearTarea' action='#' method='POST'>";
            $web .= "<br><h1>Crear Tarea</h1>
                        <div><br>
                          <form action='#' name='form_new_chore'>
                            <label for='nameChore'>Nombre:</label>
                            <input type='text' id='nameChore' name='nameChore'>
                        
                            <label for='descriptionChore'>Descripción:</label>
                            <input type='text' id='descriptionChore' name='descriptionChore'>
                            
                            <label for='projectsChore'>Proyectos:</label>
                            <select id='projectsChore' name='projectsChore'>
                                <option value='seleccione'>...::: Seleccione :::...</option>";
                                    $projects->execute(array(':id' => $id));
                                    while($datos_projects=$projects->fetch())
                                    {
                                        $web .= '
                                         <option value="'.$datos_projects['id_proyecto'].'">'.$datos_projects['nombre'].'</option>';
                                    }
            $web .= "       </select>                            
                            
                            <label for='fecha_iniChore'>Fecha Inicio:</label>
                            <input type='date' id='fecha_iniChore' name='fecha_iniChore'>
                        
                            <label for='fecha_finChore'>Fecha Fin:</label>
                            <input type='date' id='fecha_finChore' name='fecha_finChore'>
                            
                            <label for='categoriesChore'>Categorías:</label>
                            <select id='categoriesChore' name='categoriesChore'>
                                <option value='seleccione'>...::: Seleccione :::...</option>";
                                $categories->execute();
                                    while($datos_categorias=$categories->fetch())
                                    {
                                        $web .= '
                                         <option value="'.$datos_categorias['id_categoria'].'">'.$datos_categorias['nombre'].'</option>';
                                    }
            $web .= "       </select>
       
                            <label for='statesChore'>Estados:</label>
                            <select id='statesChore' name='statesChore'>
                                <option value='seleccione'>...::: Seleccione :::...</option>";
                                    $states->execute();
                                    while($datos_states=$states->fetch())
                                    {
                                        $web .= '
                                        <option value="'.$datos_states['id_estado'].'">'.$datos_states['nombre'].'</option>';
                                    }
            $web .= "       </select>
      
                            <label for='prioritiesChore'>Prioridad:</label>
                            <select id='prioritiesChore' name='prioritiesChore'>
                                <option value='seleccione'>...::: Seleccione :::...</option>";
                                    $priorities->execute();
                                    while($datos_priorities=$priorities->fetch())
                                    {
                                        $web .= '
                                        <option value="'.$datos_priorities['id_prioridad'].'">'.$datos_priorities['nombre'].'</option>';
                                    }
            $web .= "       </select>";

            $web .= "       <input type='button' value='Crear' onclick='createChore()'></form></div>";
            $link->commit();
        }catch(PDOException $e){
            $link->rollBack();
            $aux_texto=_("Se ha producido un error al tratar de guardar la información en la base de datos").': ';
            $web.='<script>alert("'.$aux_texto.$e->getMessage() . '");</script>';
            $web.='<script>error_sistema("'.$e->getMessage(). ', '.$e->getFile(). ', '.$e->getLine(). '");';
        }
    }
    return $web;
}
function createProject($parametros){
    $web='';
    $dataInformation = 'project';
    $link = $GLOBALS['link'];
    $crear_proyecto = $link->prepare("INSERT INTO proyecto (nombre, descripcion, fecha_creacion, fecha_ini, fecha_fin, fecha_update, usuario_id)
                                      VALUES (:nombre, :descripcion, :fecha_creacion, :fecha_ini, :fecha_fin, :fecha_update, :usuario_id)");
    try{
        $link->beginTransaction();
        $crear_proyecto->execute(array(':nombre'=>$parametros['project'], ':descripcion'=>$parametros['description'], ':fecha_creacion'=>date('Y-m-d H:i:s'),
                ':fecha_ini'=>$parametros['fecha_ini'], ':fecha_fin'=>$parametros['fecha_fin'], ':fecha_update'=>date('Y-m-d H:i:s'), ':usuario_id'=>$_SESSION['login']['id_usuario']));
        $aux_texto=_("Proyecto creado correctamente.").'';
        $web.='<script>alert("'.$aux_texto.'");</script>';
        $web.='<script>getInformation("'.$dataInformation.'",1);</script>';
        $link->commit();
    }
    catch(PDOException $e){
        $link->rollBack();
        $aux_texto=_("Se ha producido un error al tratar de guardar la información en la base de datos").': ';
        $web.='<script>alert("'.$aux_texto.$e->getMessage() . '");</script>';
        $web.='<script>error_sistema("'.$e->getMessage(). ', '.$e->getFile(). ', '.$e->getLine(). '");';
    }
    return $web;
}
function createChore($parametros){
    $web='';
    $dataInformation = 'chore';
    $link = $GLOBALS['link'];


    $crear_tarea = $link->prepare("INSERT INTO tarea (nombre, descripcion, fecha_creacion, fecha_inicio, fecha_final, fecha_update, categoria_id, estado_id, proyecto_id, prioridad_id)
                                      VALUES (:nombre, :descripcion, :fecha_creacion, :fecha_inicio, :fecha_final, :fecha_update, :categoria_id, :estado_id, :proyecto_id, :prioridad_id)");

    try{
        $link->beginTransaction();
        $crear_tarea->execute(array(':nombre'=>$parametros['nameChore'], ':descripcion'=>$parametros['descriptionChore'], ':fecha_creacion'=>date('Y-m-d H:i:s'),
            ':fecha_inicio'=>$parametros['fecha_iniChore'], ':fecha_final'=>$parametros['fecha_finChore'], ':fecha_update'=>date('Y-m-d H:i:s'), ':categoria_id'=>$parametros['categoriesChore'],
            ':estado_id'=>$parametros['statesChore'], ':proyecto_id'=>$parametros['projectsChore'], ':prioridad_id'=>$parametros['prioritiesChore']));
        $aux_texto=_("Tarea creada correctamente.").'';
        $web.='<script>alert("'.$aux_texto.'");</script>';
        $web.='<script>getInformation("'.$dataInformation.'",1);</script>';
        $link->commit();
    }
    catch(PDOException $e){
        $link->rollBack();
        $aux_texto=_("Se ha producido un error al tratar de guardar la información en la base de datos").': ';
        $web.='<script>alert("'.$aux_texto.$e->getMessage() . '");</script>';
        $web.='<script>error_sistema("'.$e->getMessage(). ', '.$e->getFile(). ', '.$e->getLine(). '");';
    }
    return $web;
}
function editar($parametros)
{
    $web = '';
    $link = $GLOBALS['link'];
    $id = $parametros['id'];
    $action = $parametros['editar'];
    $id_usuario = $_SESSION['login']['id_usuario'];

    $categories = $link->prepare("select * from categoria where activo = 1");
    $priorities = $link->prepare("select * from prioridad where activo = 1");
    $states = $link->prepare("select * from estado where activo = 1");
    $projects = $link->prepare("select * from proyecto p where activo = 1 and p.usuario_id = :id");
    $select_proyecto=$link->prepare("SELECT * from proyecto where id_proyecto=:id");
    $select_tarea=$link->prepare("select t.*, p.nombre as nProyecto, p.id_proyecto as idProyecto, pr.nombre as nPrioridad,
                                 pr.id_prioridad as idPrioridad, c.nombre as nCategoria, c.id_categoria as idCategoria,
                                 e.nombre as nEstado, e.id_estado as idEstado
                                from tarea t
                                inner join proyecto p on p.id_proyecto = t.proyecto_id
                                inner join prioridad pr on pr.id_prioridad = t.prioridad_id
                                inner join categoria c on c.id_categoria = t.categoria_id
                                inner join estado e on e.id_estado = t.estado_id
                                where t.id_tarea =:id");

    try
    {
        $link->beginTransaction();
        if($action ==='proyecto')
        {
            $select_proyecto->execute(array(':id' => $id));
            if ($datos_projects = $select_proyecto->fetch())
            {
                $web .= "<div class='avisos_legales'>";
                $web .= "<form id='form_editarProyecto' name='form_crearProyecto' action='#' method='POST'>";
                $web .= "<br><h1>Editar Proyecto</h1>
                        <div><br>
                          <form action='#'>
                            <input type='hidden' id='id_oculta' name='id_oculta' value='" . $datos_projects['id_proyecto'] . "'
                            <label for='nameProject'>Nombre:</label>
                            <input type='text' id='nameProject' name='nameProject' value='" . $datos_projects['nombre'] . "'>
                        
                            <label for='description'>Descripción:</label>
                            <input type='text' id='description' name='description' value='" . $datos_projects['descripcion'] . "'>
                            
                            <label for='fecha_ini'>Fecha Inicio:</label>
                            <input type='date' id='fecha_ini' name='fecha_ini' value='" . $datos_projects['fecha_ini'] . "'>
                        
                            <label for='fecha_fin'>Fecha Fin:</label>
                            <input type='date' id='fecha_fin' name='fecha_fin' value='" . $datos_projects['fecha_fin'] . "'>
                                                    
                            <input type='button' value='Guardar' onclick='updateProject()'>
                          </form>
                        </div>";
        }
        }
        else if($action ==='tarea')
        {

            $select_tarea->execute(array(':id'=>$id));
            if ($datos_chores = $select_tarea->fetch())
            {
                $web .= "<div class='avisos_legales'>";
                $web .= "<form id='form_editarTarea' name='form_editarTarea' action='#' method='POST'>";
                $web .= "<br><h1>Editar Tarea</h1>
                        <div><br>
                          <form action='#' name='form_new_chore'>
                            <input type='hidden' id='od_oculta_chore' name='od_oculta_chore' value='" . $datos_chores['id_tarea'] . "'>
                            <label for='nameChore'>Nombre:</label>
                            <input type='text' id='nameChore' name='nameChore' value='" . $datos_chores['nombre'] . "'>
                        
                            <label for='descriptionChore'>Descripción:</label>
                            <input type='text' id='descriptionChore' name='descriptionChore' value='" . $datos_chores['descripcion'] . "'>
                            
                            <label for='projectsChore'>Proyectos:</label>
                            <select id='projectsChore' name='projectsChore'>";
                                    $projects->execute(array(':id' => $id_usuario));
                                    while($datos_projects=$projects->fetch())
                                    {
                                        if($datos_chores['idProyecto'] === $datos_projects['id_proyecto'])
                                        {
                                            $web .= '
                                            <option value="'.$datos_projects['id_proyecto'].'" selected>'.$datos_projects['nombre'].'</option>';
                                        }
                                        else
                                        {
                                            $web .= '
                                            <option value="'.$datos_projects['id_proyecto'].'">'.$datos_projects['nombre'].'</option>';
                                        }

                                    }
                                    $web .= "       </select>
                            
                            <label for='fecha_iniChore'>Fecha Inicio:</label>
                            <input type='date' id='fecha_iniChore' name='fecha_iniChore' value='" . $datos_chores['fecha_inicio'] . "'>
                        
                            <label for='fecha_finChore'>Fecha Fin:</label>
                            <input type='date' id='fecha_finChore' name='fecha_finChore' value='" . $datos_chores['fecha_final'] . "'>
                            
                            <label for='categoriesChore'>Categorías:</label>
                            <select id='categoriesChore' name='categoriesChore'>";

                                    $categories->execute();
                                    while($datos_categorias=$categories->fetch())
                                    {
                                        if($datos_chores['idCategoria'] === $datos_categorias['id_categoria'])
                                        {
                                            $web .= '
                                            <option value="'.$datos_categorias['id_categoria'].'" selected>'.$datos_categorias['nombre'].'</option>';
                                        }
                                        else
                                        {
                                            $web .= '
                                            <option value="'.$datos_categorias['id_categoria'].'">'.$datos_categorias['nombre'].'</option>';
                                        }

                                    }
                                    $web .= "       </select>
       
                            <label for='statesChore'>Estados:</label>
                            <select id='statesChore' name='statesChore'>";
                                    $states->execute();
                                    while($datos_states=$states->fetch())
                                    {
                                        if($datos_chores['idEstado'] === $datos_states['id_estado'])
                                        {
                                            $web .= '
                                            <option value="'.$datos_states['id_estado'].'" selected>'.$datos_states['nombre'].'</option>';
                                        }
                                        else
                                        {
                                            $web .= '
                                            <option value="'.$datos_states['id_estado'].'">'.$datos_states['nombre'].'</option>';
                                        }

                                    }
                                    $web .= "       </select>
      
                            <label for='prioritiesChore'>Prioridad:</label>
                            <select id='prioritiesChore' name='prioritiesChore'>";
                                    $priorities->execute();
                                    while($datos_priorities=$priorities->fetch())
                                    {
                                        if($datos_chores['idPrioridad'] === $datos_priorities['id_prioridad'])
                                        {
                                            $web .= '
                                            <option value="'.$datos_priorities['id_prioridad'].'" selected>'.$datos_priorities['nombre'].'</option>';
                                        }
                                        else
                                        {
                                            $web .= '
                                            <option value="'.$datos_priorities['id_prioridad'].'">'.$datos_priorities['nombre'].'</option>';
                                        }
                                    }
                                    $web .= "       </select>";

                $web .= "       <input type='button' value='Guardar' onclick='updateChore()'></form></div>";
            }
        }
        $link->commit();
    }
catch(PDOException $e){
    $link->rollBack();
    $aux_texto=_("Se ha producido un error al tratar de guardar la información en la base de datos").': ';
    $web.='<script>alert("'.$aux_texto.$e->getMessage() . '");</script>';
    $web.='<script>error_sistema("'.$e->getMessage(). ', '.$e->getFile(). ', '.$e->getLine(). '");';
}
    return $web;
}
function borrar($parametros)
{
    $web = '';
    $link = $GLOBALS['link'];
    $id = $parametros['id'];
    $tabla = $parametros['borrar'];
    $delete_proyecto=$link->prepare("UPDATE proyecto SET activo=0 WHERE id_proyecto=:id");
    $delete_tarea=$link->prepare("UPDATE tarea SET activo=0 WHERE id_tarea=:id");
    try{
        $link->beginTransaction();
        if($tabla ==='proyecto')
        {
            $delete_proyecto->execute(array(':id'=>$id));
            $web.='<script>getInformation("project",1);</script>';
        }else if($tabla ==='tarea')
        {
            $delete_tarea->execute(array(':id'=>$id));
            $web.='<script>getInformation("chore",1);</script>';
        }

        $link->commit();
    }
    catch(PDOException $e){
        $link->rollBack();
        $aux_texto=_("Se ha producido un error al tratar de guardar la información en la base de datos").': ';
        $web.='<script>alert("'.$aux_texto.$e->getMessage() . '");</script>';
        $web.='<script>error_sistema("'.$e->getMessage(). ', '.$e->getFile(). ', '.$e->getLine(). '");';
    }
    return $web;
}
function updateProject($parametros){
    $web = '';
    $link = $GLOBALS['link'];
    $update_project = $link->prepare("UPDATE proyecto SET nombre=:nombre, descripcion=:descripcion, fecha_ini=:fecha_ini, fecha_fin=:fecha_fin, fecha_update=:fecha_update WHERE id_proyecto=:id");
    try
    {
        $link->beginTransaction();
        $update_project->execute(array(':nombre' => $parametros['nameProject'], ':descripcion' => $parametros['description'], ':fecha_ini' => $parametros['fecha_ini'],
                                            ':fecha_fin' => $parametros['fecha_fin'], ':fecha_update' => date('Y-m-d H:i:s'), ':id' => $parametros['id']));
        $web.='<script>getInformation("project",1);</script>';
        $link->commit();
    }
    catch(PDOException $e)
    {
        $link->rollBack();
        $aux_texto=_("Se ha producido un error al tratar de guardar la información en la base de datos").': ';
        $web.='<script>alert("'.$aux_texto.$e->getMessage() . '");</script>';
        $web.='<script>error_sistema("'.$e->getMessage(). ', '.$e->getFile(). ', '.$e->getLine(). '");';
    }
    return $web;
}
function updateChore($parametros){
    $web = '';
    $link = $GLOBALS['link'];
    $update_project = $link->prepare("UPDATE tarea SET nombre=:nombre, descripcion=:descripcion, fecha_inicio=:fecha_ini, fecha_final=:fecha_fin, fecha_update=:fecha_update,
                              categoria_id=:categoriaid, estado_id=:estadoid, proyecto_id=:proyectoid, prioridad_id=:prioridadid WHERE id_tarea=:id");
    try
    {
        $link->beginTransaction();
        $update_project->execute(array(':nombre' => $parametros['nameChore'], ':descripcion' => $parametros['descriptionChore'], ':fecha_ini' => $parametros['fecha_iniChore'],
            ':fecha_fin' => $parametros['fecha_finChore'], ':fecha_update' => date('Y-m-d H:i:s'),'categoriaid' => $parametros['categoriesChore'],
            'estadoid' => $parametros['statesChore'], 'proyectoid' => $parametros['projectsChore'], 'prioridadid' => $parametros['prioritiesChore'],':id' => $parametros['id']));
        $web.='<script>getInformation("chore",1);</script>';
        $link->commit();
    }
    catch(PDOException $e)
    {
        $link->rollBack();
        $aux_texto=_("Se ha producido un error al tratar de guardar la información en la base de datos").': ';
        $web.='<script>alert("'.$aux_texto.$e->getMessage() . '");</script>';
        $web.='<script>error_sistema("'.$e->getMessage(). ', '.$e->getFile(). ', '.$e->getLine(). '");';
    }
    return $web;
}
function createNew($parametros){
    $web='';
    $link = $GLOBALS['link'];
    $insert_categories=$link->prepare("INSERT INTO categoria (nombre, descripcion, activo) VALUES (:nombre, :descripcion, :activo)");
    $insert_states=$link->prepare("INSERT INTO estado (nombre, descripcion, activo) VALUES (:nombre, :descripcion, :activo)");
    $insert_roles=$link->prepare("INSERT INTO rol (nombre, siglas, descripcion, activo) VALUES (:nombre, :siglas, :descripcion, :activo)");
    $inser_priorities=$link->prepare("INSERT INTO prioridad (nombre, descripcion, activo) VALUES (:nombre, :descripcion, :activo)");
    try
    {
        $link->beginTransaction();
        if($parametros['action'] === '3'){
            $insert_categories->execute(array(':nombre' => '', ':descripcion' => '', ':activo' => 0));
            $web.='<script>getInformation("categories",1)';
        }elseif ($parametros['action'] === '4'){
            $insert_states->execute(array(':nombre' => '', ':descripcion' => '', ':activo' => 0));
            $web.='<script>getInformation("states",1)';
        }elseif ($parametros['action'] === '5'){
            $insert_roles->execute(array(':nombre' => '', ':siglas' => '', ':descripcion' => '', ':activo' => 0));
            $web .= '<script>getInformation("roles",1)';
        }elseif ($parametros['action'] === '6'){
            $inser_priorities->execute(array(':nombre' => '', ':descripcion' => '', ':activo' => 0));
            $web .= '<script>getInformation("priorities",1)';
        }
        $link->commit();
    }
    catch(PDOException $e){
        $link->rollBack();
        $aux_texto=_("Se ha producido un error al tratar de guardar la información en la base de datos").': ';
        $web.='<script>alert("'.$aux_texto.$e->getMessage() . '");';
        $web.='<script>error_sistema("'.$e->getMessage(). ', '.$e->getFile(). ', '.$e->getLine(). '");';
    }
    return $web;
}
function updateCategoria($parametros)
{
    $web = '';
    $link = $GLOBALS['link'];

    $update_Categoria = $link->prepare("UPDATE categoria SET nombre=:nombre, descripcion=:descripcion, activo=:activo WHERE id_categoria=:id");

    try
    {
        $link->beginTransaction();
        $update_Categoria->execute(array(':nombre' => $parametros['nombreCategoria'], ':descripcion' => $parametros['descripcionCategoria'], 'activo' => $parametros['activoCategoria'],':id' => $parametros['id']));
        $web .="<div class='avisos_legales'><br><h1>Actualización Información</h1><br><div class='group'>";
        $web .="<div style='width: 60%; margin: 40px auto; padding: 60px; background-color: rgba(31, 50, 110, 0.2); '> ";
        $web .= '<br><h3 style="text-align: center;">Los datos se actualizazon correctamente.</h3>';
        $web .= '<br><h3 style="text-align: center;">Recuerde que por seguridad los datos solo se pueden borrar directamente desde la base de datos.</h3>';
        $link->commit();
    }
    catch(PDOException $e)
    {
        $link->rollBack();
        $aux_texto=_("Se ha producido un error al tratar de guardar la información en la base de datos").': ';
        $web.='<script>alert("'.$aux_texto.$e->getMessage() . '");</script>';
        $web.='<script>error_sistema("'.$e->getMessage(). ', '.$e->getFile(). ', '.$e->getLine(). '");';
    }
    return $web;
}
function updateEstado($parametros)
{
    $web = '';
    $link = $GLOBALS['link'];
    $update_estado = $link->prepare("UPDATE estado SET nombre=:nombre, descripcion=:descripcion, activo=:activo WHERE id_estado=:id");
    try
    {
        $link->beginTransaction();
        $update_estado->execute(array(':nombre' => $parametros['nombreEstado'], ':descripcion' => $parametros['descripcionEstado'], 'activo' => $parametros['activoEstado'],':id' => $parametros['id']));
        $web .="<div class='avisos_legales'><br><h1>Actualización Información</h1><br><div class='group'>";
        $web .="<div style='width: 60%; margin: 40px auto; padding: 60px; background-color: rgba(31, 50, 110, 0.2); '> ";
        $web .= '<br><h3 style="text-align: center;">Los datos se actualizazon correctamente.</h3>';
        $web .= '<br><h3 style="text-align: center;">Recuerde que por seguridad los datos solo se pueden borrar directamente desde la base de datos.</h3>';
        $link->commit();
    }
    catch(PDOException $e)
    {
        $link->rollBack();
        $aux_texto=_("Se ha producido un error al tratar de guardar la información en la base de datos").': ';
        $web.='<script>alert("'.$aux_texto.$e->getMessage() . '");</script>';
        $web.='<script>error_sistema("'.$e->getMessage(). ', '.$e->getFile(). ', '.$e->getLine(). '");';
    }
    return $web;
}
function updateRol($parametros)
{
    $web = '';
    $link = $GLOBALS['link'];
    $update_rol = $link->prepare("UPDATE rol SET nombre=:nombre, descripcion=:descripcion, siglas=:siglas, activo=:activo WHERE id_rol=:id");
    try
    {
        $link->beginTransaction();
        $update_rol->execute(array(':nombre' => $parametros['nombreRoles'], ':descripcion' => $parametros['descripcionRoles'], ':siglas' => $parametros['siglasRoles'], 'activo' => $parametros['activoRoles'],':id' => $parametros['id']));
        $web .="<div class='avisos_legales'><br><h1>Actualización Información</h1><br><div class='group'>";
        $web .="<div style='width: 60%; margin: 40px auto; padding: 60px; background-color: rgba(31, 50, 110, 0.2); '> ";
        $web .= '<br><h3 style="text-align: center;">Los datos se actualizazon correctamente.</h3>';
        $web .= '<br><h3 style="text-align: center;">Recuerde que por seguridad los datos solo se pueden borrar directamente desde la base de datos.</h3>';
        $link->commit();
    }
    catch(PDOException $e)
    {
        $link->rollBack();
        $aux_texto=_("Se ha producido un error al tratar de guardar la información en la base de datos").': ';
        $web.='<script>alert("'.$aux_texto.$e->getMessage() . '");</script>';
        $web.='<script>error_sistema("'.$e->getMessage(). ', '.$e->getFile(). ', '.$e->getLine(). '");';
    }
    return $web;
}
function updatePrioridad($parametros)
{
    $web = '';
    $link = $GLOBALS['link'];
    $update_prioridad = $link->prepare("UPDATE prioridad SET nombre=:nombre, descripcion=:descripcion, activo=:activo WHERE id_prioridad=:id");
    try
    {
        $link->beginTransaction();
        $update_prioridad->execute(array(':nombre' => $parametros['nombrePrioridad'], ':descripcion' => $parametros['descripcionPrioridad'], 'activo' => $parametros['activoPrioridad'],':id' => $parametros['id']));
        $web .="<div class='avisos_legales'><br><h1>Actualización Información</h1><br><div class='group'>";
        $web .="<div style='width: 60%; margin: 40px auto; padding: 60px; background-color: rgba(31, 50, 110, 0.2); '> ";
        $web .= '<br><h3 style="text-align: center;">Los datos se actualizazon correctamente.</h3>';
        $web .= '<br><h3 style="text-align: center;">Recuerde que por seguridad los datos solo se pueden borrar directamente desde la base de datos.</h3>';
        $link->commit();
    }
    catch(PDOException $e)
    {
        $link->rollBack();
        $aux_texto=_("Se ha producido un error al tratar de guardar la información en la base de datos").': ';
        $web.='<script>alert("'.$aux_texto.$e->getMessage() . '");</script>';
        $web.='<script>error_sistema("'.$e->getMessage(). ', '.$e->getFile(). ', '.$e->getLine(). '");';
    }
    return $web;
}
function updateProyecto($parametros)
{
    $web = '';
    $link = $GLOBALS['link'];
    $update_proyecto = $link->prepare("UPDATE proyecto SET nombre=:nombre, descripcion=:descripcion, fecha_creacion=:fecha_creacion, fecha_ini=:fecha_ini, 
                                      fecha_fin=:fecha_fin, fecha_update=:fecha_update, activo=:activo WHERE id_proyecto=:id");
    try
    {
        $link->beginTransaction();
        $update_proyecto->execute(array(':nombre' => $parametros['nombreProyecto'], ':descripcion' => $parametros['descripcionProyecto'], ':fecha_creacion' => $parametros['creacionProyecto'], 'fecha_ini' => $parametros['inicioProyecto'],
            ':fecha_fin' => $parametros['finProyecto'], ':fecha_update' => $parametros['actualizacionProyecto'], ':activo' => $parametros['activoProyecto'], ':id' => $parametros['id']));
        $web .="<div class='avisos_legales'><br><h1>Actualización Información</h1><br><div class='group'>";
        $web .="<div style='width: 60%; margin: 40px auto; padding: 60px; background-color: rgba(31, 50, 110, 0.2); '> ";
        $web .= '<br><h3 style="text-align: center;">Los datos se actualizazon correctamente.</h3>';
        $web .= '<br><h3 style="text-align: center;">Recuerde que por seguridad los datos solo se pueden borrar directamente desde la base de datos.</h3>';
        $link->commit();
    }
    catch(PDOException $e)
    {
        $link->rollBack();
        $aux_texto=_("Se ha producido un error al tratar de guardar la información en la base de datos").': ';
        $web.='<script>alert("'.$aux_texto.$e->getMessage() . '");</script>';
        $web.='<script>error_sistema("'.$e->getMessage(). ', '.$e->getFile(). ', '.$e->getLine(). '");';
    }
    return $web;
}
function updateTarea($parametros)
{

    $web = '';
    // los meto así porque me estaba dando un error raro al ejecutar las preparadas.
    $link = $GLOBALS['link'];
    $nombre = $parametros['nombreTarea'];
    $descripcion = $parametros['descripcionTarea'];    $inicio = $parametros['inicioTarea'];    $fin = $parametros['finTarea'];
    $categoria = $parametros['idCategoriaTarea'];    $estado = $parametros['idEstadoTarea'];    $prioridad = $parametros['idPrioridadTarea'];
    $activo = $parametros['activoTarea'];    $id = $parametros['id'];


    $update_tarea = $link->prepare("UPDATE tarea SET nombre=:nombre, descripcion=:descripcion, fecha_inicio=:fecha_inicio, fecha_final=:fecha_fin, 
                                    categoria_id=:categoria,  estado_id=:estado, prioridad_id=:prioridad, activo=:activo WHERE id_tarea=:id");

    try
    {
        $link->beginTransaction();
        $update_tarea->execute(array(':nombre' => $nombre, ':descripcion' => $descripcion, ':fecha_inicio' => $inicio, ':fecha_fin' => $fin,
            ':categoria' => $categoria, ':estado' => $estado, ':prioridad' => $prioridad, ':activo' => $activo, ':id' => $id));
        $web .="<div class='avisos_legales'><br><h1>Actualización Información</h1><br><div class='group'>";
        $web .="<div style='width: 60%; margin: 40px auto; padding: 60px; background-color: rgba(31, 50, 110, 0.2); '> ";
        $web .= '<br><h3 style="text-align: center;">Los datos se actualizazon correctamente.</h3>';
        $web .= '<br><h3 style="text-align: center;">Recuerde que por seguridad los datos solo se pueden borrar directamente desde la base de datos.</h3>';
        $link->commit();
    }
    catch(PDOException $e)
    {
        $link->rollBack();
        $aux_texto=_("Se ha producido un error al tratar de guardar la información en la base de datos").': ';
        $web.='<script>alert("'.$aux_texto.$e->getMessage() . '");</script>';
        $web.='<script>error_sistema("'.$e->getMessage(). ', '.$e->getFile(). ', '.$e->getLine(). '");';
    }
    return $web;
}
function updateUsuario($parametros)
{
    $web = '';
    $link = $GLOBALS['link'];
    $update_usuarios = $link->prepare("UPDATE usuario SET nombre=:nombre, apellidos=:apellidos, email=:email, telefono=:telefono, ultimo_acceso=:acceso,
                                      contador_accesos=:contador_accesos, contador_accesos_fallidos=:fallidos, rol_id=:rol, bloqueado=:bloqueado, activo=:activo WHERE id_usuario=:id");

//    var_dump(print_query($update_usuarios,array(':nombre' => $parametros['nombreUsuario'], ':apellidos' => $parametros['apellidoUsuario'], ':email' => $parametros['emailUsuario'], ':telefono' => $parametros['telefonoUsuario'],
//        ':acceso' => $parametros['ultimoAcceso'], ':contador_accesos' => $parametros['contadorAccesos'], ':fallidos' => $parametros['contadorAccesosFallidos'],
//        ':rol' => $parametros['rolUsuario'], ':bloqueado' => $parametros['bloqueadoUsuario'], ':activo' => $parametros['activoUsuario'] , ':id' => $parametros['id']) ));
//    exit();
    try
    {
        $link->beginTransaction();
        $update_usuarios->execute(array(':nombre' => $parametros['nombreUsuario'], ':apellidos' => $parametros['apellidoUsuario'], ':email' => $parametros['emailUsuario'],
            'telefono' => $parametros['telefonoUsuario'], ':acceso' => $parametros['ultimoAcceso'], ':contador_accesos' => $parametros['contadorAccesos'],
            ':fallidos' => $parametros['contadorAccesosFallidos'], ':rol' => $parametros['rolUsuario'], ':bloqueado' => $parametros['bloqueadoUsuario'],
            ':activo' => $parametros['activoUsuario'] , ':id' => $parametros['id']));
        $web .="<div class='avisos_legales'><br><h1>Actualización Información</h1><br><div class='group'>";
        $web .="<div style='width: 60%; margin: 40px auto; padding: 60px; background-color: rgba(31, 50, 110, 0.2); '> ";
        $web .= '<br><h3 style="text-align: center;">Los datos se actualizazon correctamente.</h3>';
        $web .= '<br><h3 style="text-align: center;">Recuerde que por seguridad los datos solo se pueden borrar directamente desde la base de datos.</h3>';
        $link->commit();
    }
    catch(PDOException $e)
    {
        $link->rollBack();
        $aux_texto=_("Se ha producido un error al tratar de guardar la información en la base de datos").': ';
        $web.='<script>alert("'.$aux_texto.$e->getMessage() . '");</script>';
        $web.='<script>error_sistema("'.$e->getMessage(). ', '.$e->getFile(). ', '.$e->getLine(). '");';
    }
    return $web;
}
function errores($pametros)
{
    $web='';
    $link = $GLOBALS['link'];
    $insert_error = $link->prepare("INSERT INTO error (mensaje, pagina, linea, fecha, usuario_id) VALUES (:mensaje, :pagina, :linea, :fecha, :usuario)");
    try
    {
        $link->beginTransacction();
        $insert_error->execute(array(':mensaje' => $pametros['mensaje'], ':pagina' => $pametros['pagina'], ':linea' => $pametros['linea'], ':fecha' => date('Y-m-d H:i:s'), ':usuario_id' => $pametros['id']));
        $link->commit();
    }
    catch(PDOException $e)
    {

        $link->rollBack();
        $aux_texto=_("Se ha producido un error al tratar de guardar la información en la base de datos").': ';
        $web.='<script>alert("'.$aux_texto.$e->getMessage() . '");';
        $web.='<script>error_sistema("'.$e->getMessage(). ', '.$e->getFile(). ', '.$e->getLine(). '");';

    }
    return $web;
}
switch($_POST['post_accion']) {
    case 'guardar':
        $web=guardar($_POST);
        echo $web;
        break;
    case 'informacion':
        $web=informacion($_POST);
        echo $web;
        break;
    case 'contact':
        $web=contact($_POST);
        echo $web;
        break;
    case 'sendContact':
        $web=sendContact($_POST);
        echo $web;
        break;
    case 'crear':
        $web=crear($_POST);
        echo $web;
        break;
    case 'createProject':
        $web=createProject($_POST);
        echo $web;
        break;
    case 'createChore':
        $web=createChore($_POST);
        echo $web;
        break;
    case 'borrar':
        $web=borrar($_POST);
        echo $web;
        break;
    case 'editar':
        $web=editar($_POST);
        echo $web;
        break;
    case 'updateProject':
        $web=updateProject($_POST);
        echo $web;
        break;
    case 'updateChore':
        $web=updateChore($_POST);
        echo $web;
        break;
    case 'createNew':
        $web=createNew($_POST);
        echo $web;
        break;
    case 'updateCategoria':
        $web=updateCategoria($_POST);
        echo $web;
        break;
    case 'updateEstado':
        $web=updateEstado($_POST);
        echo $web;
        break;
    case 'updateRol':
        $web=updateRol($_POST);
        echo $web;
        break;
    case 'updatePrioridad':
        $web=updatePrioridad($_POST);
        echo $web;
        break;
    case 'updateProyecto':
        $web=updateProyecto($_POST);
        echo $web;
        break;
    case 'updateTarea':
        $web=updateTarea($_POST);
        echo $web;
        break;
    case 'updateUsuario':
        $web=updateUsuario($_POST);
        echo $web;
        break;
    case 'error':
        $web=error($_POST);
        echo $web;
        break;
    case 'calendar':
        $web=calendar($_POST);
        echo $web;
        break;

}
?>
