<?php
$mensaje .= _("Error de Sistema").'<br>';

function comprobarFormatoImagen($imagen, $formato)
{
    switch($formato)
    {
        case 'IMAGETYPE_GIF': $codigo = 1;
            break;
        case 'IMAGETYPE_JPEG': $codigo = 2;
            break;
        case 'IMAGETYPE_PNG': $codigo = 3;
            break;
        case 'IMAGETYPE_SWF': $codigo = 4;
            break;
        case 'IMAGETYPE_PSD': $codigo = 5;
            break;
        case 'IMAGETYPE_BMP': $codigo = 6;
            break;
        case 'IMAGETYPE_TIFF_II': $codigo = 7;
            break;
        case 'IMAGETYPE_TIFF_MM': $codigo = 8;
            break;
        case 'IMAGETYPE_JPC': $codigo = 9;
            break;
        case 'IMAGETYPE_JP2': $codigo = 10;
            break;
        case 'IMAGETYPE_JPX': $codigo = 11;
            break;
        case 'IMAGETYPE_JB2': $codigo = 12;
            break;
        case 'IMAGETYPE_SWC': $codigo = 13;
            break;
        case 'IMAGETYPE_IFF': $codigo = 14;
            break;
        case 'IMAGETYPE_WBMP': $codigo = 15;
            break;
        case 'IMAGETYPE_XBM': $codigo = 16;
            break;
        case 'IMAGETYPE_ICO': $codigo = 17;
            break;
    }
    $codigo_img = exif_imagetype($imagen);
    if( $codigo_img  == $codigo ) return array($codigo_img, true, $imagen, $formato, $codigo);
    else return array($codigo_img, false, $imagen, $formato, $codigo);
}
function generarAleatorio($min, $max)
{
    static $allowedCharacters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_';
    $len = rand($min, $max);
    $aleatorio = '';
    for($i = 1; $i <= $len; $i++) $aleatorio .= $allowedCharacters[rand(0, strlen($allowedCharacters) - 1)];
    return $aleatorio;
}
function validar_email($email)
{
    if (function_exists('filter_var')) //Introduced in PHP 5.2
    {
        if(filter_var($email, FILTER_VALIDATE_EMAIL) === FALSE) return false;
        else return true;
    }
    else
    {
        return preg_match('/^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9_](?:[a-zA-Z0-9_\-](?!\.)){0,61}[a-zA-Z0-9_-]?\.)+[a-zA-Z0-9_](?:[a-zA-Z0-9_\-](?!$)){0,61}[a-zA-Z0-9_]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/', $email);
    }
}
function cadena_aleatoria($length=10,$c=TRUE,$uc=TRUE,$n=TRUE,$sc=FALSE)
{
    if($c==1) $source .= 'abcdefghijklmnopqrstuvwxyz';
    if($uc==1) $source .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    if($n==1) $source .= '1234567890';
    if($sc==1) $source .= '|@#~$%()=^*+[]{}-_';
    if($length>0){
        $rstr = "";
        $source = str_split($source,1);
        for($i=1; $i<=$length; $i++){
            mt_srand((double)microtime() * 1000000);
            $num = mt_rand(1,count($source));
            $rstr .= $source[$num-1];
        }
    }
    return $rstr;
}
function numero_aleatorio($maximo)
{
    $numero = mt_rand(0,$maximo);
    return $numero;
}
function comprobar_hora($hora) //HH:MM:SS
{
    list($horas,$minutos,$segundos) = explode(':',$hora);

    if ($horas > -1 && $horas < 24 && $minutos > -1 && $minutos < 60 && $segundos > -1 && $segundos < 60)
    {
        return true;
    }else{
        return false;
    }
}
function validar_fecha( $tipo, $fecha )
{
    if( $tipo == "e" ) 		$separator_used="/";
    else if( $tipo == "a" ) $separator_used="-";

    $input_array = explode( $separator_used, $fecha );

    if( $tipo == "e" ) 		return checkdate( $input_array[1], $input_array[0], $input_array[2] );
	elseif ( $tipo == "a" ) return checkdate( $input_array[1], $input_array[2], $input_array[0] );
}
function formatear_fecha($tipo, $fecha)
{
    $lafecha = "";

    if ($tipo == "e") //Pasa de formato americano (yyyy-mm-dd) a formato español (dd/mm/yyyy)
    {
        ereg("([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})", $fecha, $mifecha);
        $lafecha = $mifecha[3] . "/" . $mifecha[2] . "/" . $mifecha[1];
    }
	elseif ($tipo == "el") //Pasa de formato americano (yyyy-mm-dd) a formato español largo (dd "de" nombremes "de" yyyy)
    {
        ereg("([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})", $fecha, $mifecha);

        $array_meses['01'] = _("Enero");
        $array_meses['02'] = _("Febrero");
        $array_meses['03'] = _("Marzo");
        $array_meses['04'] = _("Abril");
        $array_meses['05'] = _("Mayo");
        $array_meses['06'] = _("Junio");
        $array_meses['07'] = _("Julio");
        $array_meses['08'] = _("Agosto");
        $array_meses['09'] = _("Septiembre");
        $array_meses['10'] = _("Octubre");
        $array_meses['11'] = _("Noviembre");
        $array_meses['12'] = _("Diciembre");

        $lafecha = $mifecha[3] . " de " . $array_meses[$mifecha[2]] . " de " . $mifecha[1];
    }
	elseif ($tipo == "a") //Pasa de formato español (dd/mm/yyyy) a formato americano (yyyy-mm-dd)
    {
        ereg("([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $fecha, $mifecha);
        $lafecha = $mifecha[3] . "-" . $mifecha[2] . "-" . $mifecha[1];
    }
	elseif ($tipo == "sql") //Pasa de formato español (dd-mm-yyyy) del sql server a formato español (dd/mm/yyyy)
    {
        ereg("([0-9]{1,2})-([0-9]{1,2})-([0-9]{2,4})", $fecha, $mifecha);
        $lafecha = str_pad($mifecha[1], 2, 0, STR_PAD_LEFT) . "/" . str_pad($mifecha[2], 2, 0, STR_PAD_LEFT) . "/" . $mifecha[3];
    }
	elseif ($tipo == "ee") //Pasa de formato americano (yyyy-mm-dd) a formato español (dd/mm/yy)
    {
        ereg("([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})", $fecha, $mifecha);
        $lafecha = $mifecha[3] . "/" . $mifecha[2] . "/" . substr($mifecha[1], 2, strlen($mifecha[1]));
    }
	elseif ($tipo == "pre") //Pasa de formato americano (yyyy-mm-dd HH:ii:ss) a formato presentable en listados (hora, solo día)
    {
        $aux_fecha=explode(' ',$fecha);
        if($aux_fecha[0]==date("Y-m-d")) $lafecha=substr($aux_fecha[1], 0, -3);
        else $lafecha=formatear_fecha('e',$aux_fecha[0]);
    }
	elseif ($tipo == "pre2") //Pasa de formato americano (yyyy-mm-dd HH:ii:ss) a formato presentable en listados (dia spa y hora)
    {
        $aux_fecha=explode(' ',$fecha);
        $lafecha=formatear_fecha('e',$aux_fecha[0]).' '.$aux_fecha[1];
    }
	elseif ($tipo == "pre3") //Pasa de formato americano (yyyy-mm-dd HH:ii:ss) a formato presentable en listados (dd/mm/YYYY)
    {
        $aux_fecha=explode(' ',$fecha);
        $lafecha=formatear_fecha('e',$aux_fecha[0]);
    }
	elseif ($tipo == "csv1") //Pasa de formato dd-mmm-yy (donde mmm es texto) a formato compatible con MySQL (yyyy-mm-dd)
    {
        $aux_fecha=explode('-',$fecha);

        $array_meses['ene'] = "01";
        $array_meses['feb'] = "02";
        $array_meses['mar'] = "03";
        $array_meses['abr'] = "04";
        $array_meses['may'] = "05";
        $array_meses['jun'] = "06";
        $array_meses['jul'] = "07";
        $array_meses['ago'] = "08";
        $array_meses['sep'] = "09";
        $array_meses['oct'] = "10";
        $array_meses['nov'] = "11";
        $array_meses['dic'] = "12";

        $aux_fecha[2] += 2000;

        $lafecha = $aux_fecha[2] . "-" . $array_meses[$aux_fecha[1]] . "-" . $aux_fecha[0];
    }
	elseif ($tipo == "csv2") //Pasa de formato yyyy-mm-dd a formato excel dd-mmm-yy (donde mmm es texto)
    {
        $aux_fecha=explode('-',$fecha);

        $array_meses['01'] = "ene";
        $array_meses['02'] = "feb";
        $array_meses['03'] = "mar";
        $array_meses['04'] = "abr";
        $array_meses['05'] = "may";
        $array_meses['06'] = "jun";
        $array_meses['07'] = "jul";
        $array_meses['08'] = "ago";
        $array_meses['09'] = "sep";
        $array_meses['10'] = "oct";
        $array_meses['11'] = "nov";
        $array_meses['12'] = "dic";

        $aux_fecha[0] -= 2000;

        $lafecha = $aux_fecha[2] . "-" . $array_meses[$aux_fecha[1]] . "-" . $aux_fecha[0];
    }

    return $lafecha;
}
function encriptar ($cadena)
{
    $resultado = base64_encode($cadena);
    $resultado = strrev($resultado);
    $resultado = str_rot13($resultado);
    //$resultado = convert_uuencode($resultado);

    return($resultado);
}
function desencriptar ($cadena)
{
    //$resultado = convert_uudecode($cadena);
    $resultado = str_rot13($cadena);
    $resultado = strrev($resultado);
    $resultado = base64_decode($resultado);

    return($resultado);
}
function diadelasemana ($dia)
{
    if ($dia==1) return(_("Lunes"));
    if ($dia==2) return(_("Martes"));
    if ($dia==3) return(_("Miércoles"));
    if ($dia==4) return(_("Jueves"));
    if ($dia==5) return(_("Viernes"));
    if ($dia==6) return(_("Sábado"));
    if ($dia==0) return(_("Domingo"));
}
function nombredelmes ($mes)
{
    if ($mes=='01') return(_("Enero"));
    if ($mes=='02') return(_("Febrero"));
    if ($mes=='03') return(_("Marzo"));
    if ($mes=='04') return(_("Abril"));
    if ($mes=='05') return(_("Mayo"));
    if ($mes=='06') return(_("Junio"));
    if ($mes=='07') return(_("Julio"));
    if ($mes=='08') return(_("Agosto"));
    if ($mes=='09') return(_("Septiembre"));
    if ($mes=='10') return(_("Octubre"));
    if ($mes=='11') return(_("Noviembre"));
    if ($mes=='12') return(_("Diciembre"));
}
function number_format_general ($numero, $destino="bd", $numero_decimales='', $separador_decimales='', $separador_miles='')
{
    $datos_parametros = $GLOBALS['datos_parametros'];

    if (trim($numero)=="")
    {
        $numero=0;
    }
    else
    {
        $numero = str_replace(",", ".", $numero);

        //Código para quitar el separado de miles
        $aux = explode('.',$numero);
        $aux_numero = '';
        if(count($aux) > 1) {
            for($i=0;$i<count($aux); $i++){
                $aux_numero = ($i < count($aux)-1) ? $aux_numero.$aux[$i] : $aux_numero.'.'.$aux[$i];
            }
            $numero = $aux_numero;
        }
        //

        if (is_numeric($numero))
        {
            if ($destino=="bd")
            {
                if ($numero_decimales=='')
                {
                    $numero_decimales = intval($datos_parametros['numero_decimales']);
                }
                $separador_decimales = ".";
                $separador_miles = "";

                $numero = number_format($numero, $numero_decimales, $separador_decimales, $separador_miles);
            }
			elseif ($destino=="pantalla")
            {
                if ($numero_decimales=='')
                {
                    $numero_decimales = intval($datos_parametros['numero_decimales']);
                }
                if ($separador_decimales=='')
                {
                    $separador_decimales = $datos_parametros['separador_decimales'];
                }
                if ($separador_miles=='')
                {
                    $separador_miles = $datos_parametros['separador_miles'];
                }

                $numero = number_format($numero, $numero_decimales, $separador_decimales, $separador_miles);
            }
            else
            {
                $numero = 0;
            }
        }
        else
        {
            $numero = 0;
        }
    }

    return($numero);
}
function valida_nif_cif_nie($cif) {
    $cif = mayusculas($cif);
    for ($i = 0; $i < 9; $i ++)
    {
        $num[$i] = substr($cif, $i, 1);
    }
    //si no tiene un formato valido devuelve error
    if (!preg_match('/((^[A-Z]{1}[0-9]{7}[A-Z0-9]{1}$|^[T]{1}[A-Z0-9]{8}$)|^[0-9]{8}[A-Z]{1}$)/', $cif))
    {
        return 0;
    }
    //comprobacion de NIFs estandar
    if (preg_match('/(^[0-9]{8}[A-Z]{1}$)/', $cif))
    {
        if ($num[8] == substr('TRWAGMYFPDXBNJZSQVHLCKE', substr($cif, 0, 8) % 23, 1))
        {
            return 1;
        }
        else
        {
            return -1;
        }
    }
    //algoritmo para comprobacion de codigos tipo CIF
    $suma = $num[2] + $num[4] + $num[6];
    for ($i = 1; $i < 8; $i += 2)
    {
        $suma += substr((2 * $num[$i]),0,1) + substr((2 * $num[$i]), 1, 1);
    }
    $n = 10 - substr($suma, strlen($suma) - 1, 1);
    //comprobacion de NIFs especiales (se calculan como CIFs o como NIFs)
    if (preg_match('/^[KLM]{1}/', $cif))
    {
        if ($num[8] == chr(64 + $n) || $num[8] == substr('TRWAGMYFPDXBNJZSQVHLCKE', substr($cif, 1, 8) % 23, 1))
        {
            return 2;
        }
        else
        {
            return -2;
        }
    }
    //comprobacion de CIFs
    if (preg_match('/^[ABCDEFGHJNPQRSUVW]{1}/', $cif))
    {
        if ($num[8] == chr(64 + $n) || $num[8] == substr($n, strlen($n) - 1, 1))
        {
            return 2;
        }
        else
        {
            return -2;
        }
    }
    //comprobacion de NIEs
    if (preg_match('/^[XYZ]{1}/', $cif))
    {
        if ($num[8] == substr('TRWAGMYFPDXBNJZSQVHLCKE', substr(str_replace(array('X','Y','Z'), array('0','1','2'), $cif), 0, 8) % 23, 1))
        {
            return 3;
        }
        else
        {
            return -3;
        }
    }
    //si todavia no se ha verificado devuelve error
    return 0;
}
function get_fechas_en_intervalo ( $fecha_inicio, $fecha_fin)
{
    $fecha_inicio = str_replace('/', '-', $fecha_inicio);
    $fecha_fin = str_replace('/', '-', $fecha_fin);

    $fechas_en_intervalo = array();
    $fecha_aux = $fecha_inicio;

    while(strtotime($fecha_aux) <= strtotime($fecha_fin)) {
        $fechas_en_intervalo[] = date("Y-m-d", strtotime($fecha_aux));
        $fecha_aux = date("Y-m-d", strtotime($fecha_aux . " + 1 day"));
    }

    return $fechas_en_intervalo;
}
function validateTime($time)
{
    $pattern="/^([0-1][0-9]|[2][0-3])[\:]([0-5][0-9])$/";

    if(preg_match($pattern,$time))

        return true;

    return false;
}
function minusculas( $texto ) { return mb_strtolower ($texto, 'UTF-8');}
function mayusculas( $texto ) { return mb_strtoupper ($texto, 'UTF-8');}
function print_query($pdo, $params){
    if(!empty($pdo->queryString)){

        $params = array_map(
            function ($value) {
                return "'".$value."'";
            },
            $params
        );

        $params_keys = array();
        foreach($params as $key => $param){
            $params_keys[] = $key;
        }

        return str_replace($params_keys, $params, $pdo->queryString);
    }
}
function verfecha($vfecha)
{
    $fch=explode("-",$vfecha);
    $tfecha=$fch[2]."-".$fch[1]."-".$fch[0];
    return $tfecha;
}
function primerNumero($fijo, $numero){
    $a = ($fijo * $numero) - $fijo;
    return $a;
}

