<?php
/**
 * Created by PhpStorm.
 * User: chema
 * Date: 05/06/2018
 * Time: 19:23
 */
include("../../includes/include_base2.php");
include('../../functions/php/functions.php');

function cargar($parametros){

    $web='';
    $filtro ='';
    $link = $GLOBALS['link'];
    $sql_categories = $link->prepare("SELECT * FROM categoria ORDER BY id_categoria DESC ");
    $sql_states = $link->prepare("SELECT * FROM estado ORDER BY id_estado DESC");
    $sql_priorities = $link->prepare("SELECT * FROM prioridad ORDER BY id_prioridad DESC");
    $sql_projects = $link->prepare("SELECT * FROM proyecto ORDER BY id_proyecto DESC");
    $sql_roles = $link->prepare("SELECT * FROM rol ORDER BY id_rol DESC ");
    $sql_chores = $link->prepare("SELECT * FROM tarea ORDER BY id_tarea DESC ");
    $sql_users = $link->prepare("SELECT * FROM usuario ORDER BY id_usuario DESC ");
    $sql_sessions = $link->prepare("SELECT * FROM sesion ORDER BY id_sesion DESC ");
    $sql_logs = $link->prepare("SELECT * FROM log ORDER BY id_log DESC ");
    $sql_errors = $link->prepare("SELECT * FROM error ORDER BY id_error DESC ");
    $sql_user = $link->prepare("SELECT * FROM usuario WHERE id_usuario=:id");
    $sql_project = $link->prepare("SELECT p.* FROM proyecto p INNER JOIN usuario u ON p.usuario_id= u.id_usuario WHERE u.id_usuario=:id AND p.activo = 1");
    $sql_chore = $link->prepare("SELECT t.*, c.nombre as categoria, e.nombre as estado, p.nombre as proyecto, pr.nombre as prioridad FROM tarea t 
                                INNER JOIN proyecto p ON t.proyecto_id= p.id_proyecto 
                                INNER JOIN categoria c ON c.id_categoria = t.categoria_id
                                INNER JOIN estado e ON e.id_estado = t.estado_id
                                INNER JOIN prioridad pr ON pr.id_prioridad = t.prioridad_id
                                INNER JOIN usuario u ON p.usuario_id= u.id_usuario WHERE u.id_usuario=:id AND t.activo = 1");
//estadísticas
    $sql_contadorLogs = $link->prepare("select count(*)  as contador from log");
    $sql_contadorSesiones = $link->prepare("select count(*) as contador  from sesion");
    $sql_contadorErrores = $link->prepare("select count(*) as contador from error");
    $sql_contadorUsuarios = $link->prepare("select count(*) as contador from usuario");
    $sql_contadorTareas = $link->prepare("select count(*) as contador from tarea");
    $sql_contadorProyectos = $link->prepare("select count(*) as contador from proyecto");
    $sql_contadorTareasEliminadas = $link->prepare("select count(*)  as contador from tarea where estado_id = 4");
    $sql_contadorUsuariosBloqueados = $link->prepare("select count(*)  as contador from usuario where bloqueado = 1");
    $sql_contadorLogs2018 = $link->prepare("select count(*)  as contador from log where fecha between '20180101' and '20181231'");
    $sql_contadorSesiones2018 = $link->prepare("select count(*)  as contador from sesion where fecha between '20180101' and '20181231'");
    $sql_contadorErrores2018 = $link->prepare("select count(*)  as contador from error where fecha between '20180101' and '20181231'");
    $sql_contadorUsuarios2018 = $link->prepare("select count(*)  as contador from usuario where fecha_registro between '20180101' and '20181231'");
    $sql_contadorTareas2018 = $link->prepare("select count(*)  as contador from tarea where fecha_inicio between '20180101' and '20181231'");
    $sql_contadorProyecto2018s = $link->prepare("select count(*)  as contador from proyecto where fecha_ini between '20180101' and '20181231'");
    $rol = $parametros['action'];
    $id = $parametros['id'];
    $seguridad = $parametros['rol'];

    try{

        $link->beginTransaction();
        if ($rol === 'user'){

            $sql_user->execute(array(':id'=>$parametros['id']));
            $web.= "<div id='form_user' name='form_user' action='' method=''> ";
            while($data_user = $sql_user->fetch()){

                $web .="<div class='avisos_legales'><br><h1>Datos del Usuario</h1><br><div class='group'>";
                $web.= "<input type='hidden' name='user_id' id='user_id' value='".$parametros['id']."'>";
                $web.= "<label>Nombre:</label>";
                $web.= "<input type='text' name='user_nombre' id='user_nombre' value='".$data_user['nombre']."'>";
                $web.= "<label>Apellidos:</label>";
                $web.= "<input type='text' name='user_apellidos' id='user_apellidos' value='".$data_user['apellidos']."'>";
                $web.= "<label>Email:</label>";
                $web.= "<input type='text' name='user_email' id='user_email' value='".$data_user['email']."'>";
                $web.= "<label>Teléfono:</label>";
                $web.= "<input type='text' name='user_telefono' id='user_telefono' value='".$data_user['telefono']."'>";
                $web.= "<label>Fecha Alta:</label>";
                $web.= "<input type='date' name='user_registro' id='user_registro' disabled value='".$data_user['fecha_registro']."'>";
                $web.= "<input type='button' value='Guardar' onclick='guardarRegistro()'>";
                $web .= "</div><div>";

            }

            $web.= "</div>";

        }elseif ($rol === 'project'){

            $project = 1;
            $sql_project->execute(array(':id'=>$parametros['id']));
            $total = $sql_project->rowCount();
            $numero = (int)$parametros['numero'];
            $primerNumero = primerNumero($_SESSION['cantidad'], $numero);
            $segundoNumero = $primerNumero + 4;
            $web .="<div class='avisos_legales'><br><h1>Proyectos del Usuario</h1><br><div class='group'>";
            $web.= "<table class='blueTable'><thead><tr><th>Nombre</th><th>Descripción</th><th>Fecha Creación</th><th>Fecha Inicio</th><th>Fecha Fin</th><th>Fecha Actualización</th><th>Editar</th><th>Borrar</th></tr></thead>";
            $web.= "<tbody><tr>";
            $i=0;
            while($data_project = $sql_project->fetch()){
                if($i >= $primerNumero && $i <= $segundoNumero) {
                    $web .= '<tr>';
                    $web .= '<td>' . $data_project['nombre'] . '</td>';
                    $web .= '<td>' . $data_project['descripcion'] . '</td>';
                    $web .= '<td>' . $data_project['fecha_creacion'] . '</td>';
                    $web .= '<td>' . $data_project['fecha_ini'] . '</td>';
                    $web .= '<td>' . $data_project['fecha_fin'] . '</td>';
                    $web .= '<td>' . $data_project['fecha_update'] . '</td>';
                    $web .= '<td><a href="#" onclick="actionEdit(' . $data_project['id_proyecto'] . ', ' . $project . ')"><img id="acciones" src="img/edit.png"></a></td>';
                    $web .= '<td><a href="#" onclick="actionDelete(' . $data_project['id_proyecto'] . ', ' . $project . ')"><img id="acciones" src="img/borrar.png"></a></td>';
                    $web .= '</tr>';
                    $web .= '<tr>';
                }
                $i =$i+1;

            }

            $web.= "</tbody></tr></table>";
            $web .= "<div id='paginador'>";
            for ($i = 1; $i <= $total; $i+=5) {
                $numero = intval(($i / 5)) +1;
                $web .= "<input type='button' class='botonPaginador' value='".$numero."' onclick=cargarPaginador('$rol',$numero)>";
            }
            $web .="</div>";
            $web .= "</div><div>";

        }elseif ($rol === 'chore'){

            $chore = 2;
            $sql_chore->execute(array(':id'=>$parametros['id']));
            $total = $sql_chore->rowCount();
            $numero = (int)$parametros['numero'];
            $primerNumero = primerNumero($_SESSION['cantidad'], $numero);
            $segundoNumero = $primerNumero + 4;
            $web .="<div class='avisos_legales'><br><h1>Tareas del Usuario</h1><br><div class='group'>";
            $web.= "<table class='blueTable'><thead><tr><th>Nombre</th><th>Descripción</th><th>Fecha Creación</th><th>Fecha Inicio</th><th>Fecha Fin</th><th>Fecha Actualización</th><th>Categoría</th><th>Estado</th><th>Proyecto</th><th>Prioridad</th><th>Editar</th><th>Borrar</th></tr></thead>";
            $web.= "<tbody><tr>";
            $i=0;
            while($data_chore = $sql_chore->fetch()){
                if($i >= $primerNumero && $i <= $segundoNumero) {
                    $web .= '<tr>';
                    $web .= '<td>' . $data_chore['nombre'] . '</td>';
                    $web .= '<td>' . $data_chore['descripcion'] . '</td>';
                    $web .= '<td>' . $data_chore['fecha_creacion'] . '</td>';
                    $web .= '<td>' . $data_chore['fecha_inicio'] . '</td>';
                    $web .= '<td>' . $data_chore['fecha_final'] . '</td>';
                    $web .= '<td>' . $data_chore['fecha_update'] . '</td>';
                    $web .= '<td>' . $data_chore['categoria'] . '</td>';
                    $web .= '<td>' . $data_chore['estado'] . '</td>';
                    $web .= '<td>' . $data_chore['proyecto'] . '</td>';
                    $web .= '<td>' . $data_chore['prioridad'] . '</td>';
                    $web .= '<td><a href="#" onclick="actionEdit(' . $data_chore['id_tarea'] . ', ' . $chore . ')"><img id="acciones" src="img/edit.png"></a></td>';
                    $web .= '<td><a href="#" onclick="actionDelete(' . $data_chore['id_tarea'] . ', ' . $chore . ')"><img id="acciones" src="img/borrar.png"></a></td>';
                    $web .= '</tr>';
                    $web .= '<tr>';
                }
                $i= $i +1;
            }

            $web.= "</tbody></tr></table>";
            $web .= "<div id='paginador'>";
            for ($i = 1; $i <= $total; $i+=5) {
                $numero = intval(($i / 5)) +1;
                $web .= "<input type='button' class='botonPaginador' value='".$numero."' onclick=cargarPaginador('$rol',$numero)>";
            }
            $web .="</div>";
            $web .= "</div><div>";

        }elseif($rol === 'logs'){

            $sql_logs->execute(array(':id'=>$parametros['id']));
            $total = $sql_logs->rowCount();
            $numero = (int)$parametros['numero'];
            $primerNumero = primerNumero($_SESSION['cantidad'], $numero);
            $segundoNumero = $primerNumero + 4;

            $web .="<div class='avisos_legales'><br><h1>Información detallada sobre todos los logueos creados.</h1><br><div class='group'>";
            $web.= "<table class='blueTable'><thead><tr><th>ID Log</th><th>Fecha</th><th>Hora</th><th>Usuario Id</th></tr></thead>";
            $web.= "<tbody><tr>";
            $i=0;
            while($data_logs = $sql_logs->fetch()){
                if($i >= $primerNumero && $i <= $segundoNumero){
                    $web .= '<tr>';
                    $web .= '<td><p>'.$data_logs['id_log'].'</p></td>';
                    $web .= '<td><p>'.$data_logs['fecha'].'</p></td>';
                    $web .= '<td><p>'.$data_logs['hora'].'</p></td>';
                    $web .= '<td><p>'.$data_logs['usuario_id'].'</p></td>';
                    $web .= '</tr>';
                    $web .= '<tr>';
                }
                $i= $i +1;
            }

            $web.= "</tbody></tr></table>";
            $web .= "<div id='paginador'>";
            for ($i = 1; $i <= $total; $i+=5) {
                $numero = intval(($i / 5)) +1;
                $web .= "<input type='button' class='botonPaginador' value='".$numero."' onclick=cargarPaginador('$rol',$numero)>";
            }
            $web .="</div>";
            $web .="</div><div>";

        }elseif($rol === 'stadistics'){
            $web .="<div class='avisos_legales'><br><h1>Información detallada de algunos datos de GestApp.</h1><br><div class='group'>";
            $web .="<div class='informacion' '>";
            $web .="<h3>Contadores:</h3><br>";
            $sql_contadorLogs->execute();
            $logueos = $sql_contadorLogs->fetch();
            $web.= '<p> - Número de logs actualmente de la aplicación: '.$logueos['contador'].'</p>';
            $sql_contadorSesiones->execute();
            $sesiones = $sql_contadorSesiones->fetch();
            $web.= '<p> - Número de sesiones actualmente de la aplicación: '.$sesiones['contador'].'</p>';
            $sql_contadorErrores->execute();
            $errores = $sql_contadorErrores->fetch();
            $web.= '<p> - Número de errores actualmente de la aplicación: '.$errores['contador'].'</p>';
            $sql_contadorUsuarios->execute();
            $usuarios = $sql_contadorUsuarios->fetch();
            $web.= '<p> - Número de ususarios actualmente de la aplicación: '.$usuarios['contador'].'</p>';
            $sql_contadorTareas->execute();
            $tareas = $sql_contadorTareas->fetch();
            $web.= '<p> - Número de tareas actualmente de la aplicación: '.$tareas['contador'].'</p>';
            $sql_contadorProyectos->execute();
            $proyectos = $sql_contadorProyectos->fetch();
            $web.= '<p> - Número de proyectos actualmente de la aplicación: '.$proyectos['contador'].'</p>';
            $web .="<h3>Contadores Específicos:</h3><br>";
            $sql_contadorTareasEliminadas->execute();
            $eliminadas = $sql_contadorTareasEliminadas->fetch();
            $web.= '<p> - Número de tareas eliminadas en la aplicación: '.$eliminadas['contador'].'</p>';
            $sql_contadorUsuariosBloqueados->execute();
            $bloqueados = $sql_contadorUsuariosBloqueados->fetch();
            $web.= '<p> - Número de usuarios bloqueados actualmente en la aplicación: '.$bloqueados['contador'].'</p>';
            $web .="<h3>Contadores Específicos 2018:</h3><br>";
            $sql_contadorLogs2018->execute();
            $logs2018 = $sql_contadorLogs2018->fetch();
            $web.= '<p> - Número de logs en este año: '.$logs2018['contador'].'</p>';
            $sql_contadorSesiones2018->execute();
            $sesiones2018 = $sql_contadorSesiones2018->fetch();
            $web.= '<p> - Número de sesiones en este año: '.$sesiones2018['contador'].'</p>';
            $sql_contadorErrores2018->execute();
            $errores2018 = $sql_contadorErrores2018->fetch();
            $web.= '<p> - Número de errores en este año: '.$errores2018['contador'].'</p>';
            $sql_contadorUsuarios2018->execute();
            $usuarios2018 = $sql_contadorUsuarios2018->fetch();
            $web.= '<p> - Número de usuarios en este año: '.$usuarios2018['contador'].'</p>';
            $sql_contadorTareas2018->execute();
            $tareas2018 = $sql_contadorTareas2018->fetch();
            $web.= '<p> - Número de tareas en este año: '.$tareas2018['contador'].'</p>';
            $sql_contadorProyecto2018s->execute();
            $proyectos2018 = $sql_contadorProyecto2018s->fetch();
            $web .= '<p> - Número de proyectos en este año: '.$proyectos2018['contador'].'</p>';
            $web .= "</div></div><div>";

        }elseif($rol === 'sessions'){

            $sql_sessions->execute(array(':id'=>$parametros['id']));
            $total = $sql_sessions->rowCount();
            $primerNumero = primerNumero($parametros['numero'], $_SESSION['cantidad']);
            $segundoNumero = $primerNumero + 4;
            $web .="<div class='avisos_legales'><br><h1>Información detallada sobre todos las sesiones creados.</h1><br><div class='group'>";
            $web.= "<table class='blueTable'><thead><tr><th>ID Sesion</th><th>Fecha</th><th>Usuario Id</th></tr></thead>";
            $web.= "<tbody><tr>";
            $i=0;
            while($data_sessions = $sql_sessions->fetch()){
                if($i >= $primerNumero && $i <= $segundoNumero) {
                    $web .= '<tr>';
                    $web .= '<td><p>' . $data_sessions['id_sesion'] . '</p></td>';
                    $web .= '<td><p>' . $data_sessions['fecha'] . '</p></td>';
                    $web .= '<td><p>' . $data_sessions['usuario_id'] . '</p></td>';
                    $web .= '</tr>';
                    $web .= '<tr>';
                }
                $i= $i +1;
            }

            $web.= "</tbody></tr></table>";
            $web .= "<div id='paginador'>";
            for ($i = 1; $i <= $total; $i+=5) {
                $numero = intval(($i / 5)) +1;
                $web .= "<input type='button' class='botonPaginador' value='".$numero."' onclick=cargarPaginador('$rol',$numero)>";
            }
            $web .="</div>";
            $web .="</div><div>";

        }elseif($rol === 'errors'){

            $sql_errors->execute(array(':id'=>$parametros['id']));
            $total = $sql_errors->rowCount();
            $numero = (int)$parametros['numero'];
            $primerNumero = primerNumero($_SESSION['cantidad'], $numero);
            $segundoNumero = $primerNumero + 4;

            $web .="<div class='avisos_legales'><br><h1>Información detallada de errores en conexión con BBDD.</h1><br><div class='group'>";
            $web.= "<table class='blueTable'><thead><tr><th>ID Error</th><th>Mensaje</th><th>Página</th><th>Línea</th><th>Fecha</th><th>Usuario Id</th></tr></thead>";
            $web.= "<tbody><tr>";
            $i=0;
            while($data_errors = $sql_errors->fetch()) {


                if($i >= $primerNumero && $i <= $segundoNumero) {

                    $web .= '<tr>';
                    $web .= '<td><p>' . $data_errors['id_error'] . '</p></td>';
                    $web .= '<td><p>' . $data_errors['mensaje'] . '</p></td>';
                    $web .= '<td><p>' . $data_errors['pagina'] . '</p></td>';
                    $web .= '<td><p>' . $data_errors['linea'] . '</p></td>';
                    $web .= '<td><p>' . $data_errors['fecha'] . '</p></td>';
                    $web .= '<td><p>' . $data_errors['usuario_id'] . '</p></td>';
                    $web .= '</tr>';
                    $web .= '<tr>';
                }
                $i= $i +1;
            }
            $web .= "</tbody></tr></table>";
            $web .= "<div id='paginador'>";
            for ($i = 1; $i <= $total; $i+=5) {
                $numero = intval(($i / 5)) +1;
                $web .= "<input type='button' class='botonPaginador' value='".$numero."' onclick=cargarPaginador('$rol',$numero)>";
            }
            $web .="</div>";
            $web .="</div><div>";

        }elseif(($rol === 'categories') && ($seguridad !== 'RQ')){

            $categories = 3;
            $sql_categories->execute(array(':id'=>$parametros['id']));

            $web .="<div class='avisos_legales'><br><h1>Categorias</h1><br><div class='group'>";
            $web .="<input style='margin-left: 10px;' type='button' value='Crear Nueva' onclick='actionCrear($categories);'>";
            $web.= "<table class='blueTable'><thead><tr><th>Nombre</th><th>Descripción</th><th>Activo</th><th>Guardar</th></tr></thead>";
            $web.= "<tbody><tr>";

            while($data_categories = $sql_categories->fetch()) {

                    $web .= '<tr>';
                    $web .= '<td><input type="text" id="nombreCategoria-' . $data_categories['id_categoria'] . '" name="nombreCategoria" value="' . $data_categories['nombre'] . '"></td>';
                    $web .= '<td><input type="text" id="descripcionCategoria-' . $data_categories['id_categoria'] . '" name="descripcionCategoria" value="' . $data_categories['descripcion'] . '"></td>';
                    $web .= '<td><select id="activoCategoria-' . $data_categories['id_categoria'] . '" name="activoCategoria">';
                    if ($data_categories['activo'] == '0') {
                        $web .= '<option  id="activoCategoria-' . $data_categories['id_categoria'] . '" name="activoCategoria" value="0" selected>No</option>';
                        $web .= '<option  id="activoCategoria-' . $data_categories['id_categoria'] . '" name="activoCategoria" value="1" >Si</option>';
                    } else {
                        $web .= '<option  id="activoCategoria-' . $data_categories['id_categoria'] . '" name="activoCategoria" value="0" >No</option>';
                        $web .= '<option  id="activoCategoria-' . $data_categories['id_categoria'] . '" name="activoCategoria" value="1" selected>Si</option>';
                    }

                    $web .= "</select></td>";
                    $web .= '<td><a href="#" onclick="actionGuardar(' . $data_categories['id_categoria'] . ', ' . $categories . ')"><img id="acciones" src="img/save.png"></a></td>';
                    $web .= '</tr>';
                    $web .= '<tr>';
            }

            $web.= "</tbody></tr></table>";
            $web .= "</div><div>";

        }elseif(($rol === 'categories') && ($seguridad === 'RQ')){

            $sql_categories->execute(array(':id'=>$parametros['id']));
            $web .="<div class='avisos_legales'><br><h1>Categorias</h1><br><div class='group'>";
            $web.= "<table class='blueTable'><thead><tr><th>Nombre</th><th>Descripción</th><th>Activo</th></tr></thead>";
            $web.= "<tbody><tr>";

            while($data_categories = $sql_categories->fetch()) {
                    $web .= '<tr>';
                    $web .= '<td>' . $data_categories['nombre'] . '</td>';
                    $web .= '<td>' . $data_categories['descripcion'] . '</td>';
                    $web .= '<td>';
                    if ($data_categories['activo'] == '0') {
                        $web .= 'No';
                    } else {
                        $web .= 'Si';
                    }

                    $web .= '</tr>';
                    $web .= '<tr>';
            }

            $web.= "</tbody></tr></table>";
            $web .= "</div><div>";

        }elseif(($rol === 'states') && ($seguridad !== 'RQ')){

            $states = 4;
            $sql_states->execute(array(':id'=>$parametros['id']));
            $web .="<div class='avisos_legales'><br><h1>Estados</h1><br><div class='group'>";
            $web .="<input style='margin-left: 10px;' type='button' value='Crear Nueva' onclick='actionCrear($states);'>";
            $web.= "<table class='blueTable'><thead><tr><th>Nombre</th><th>Descripción</th><th>Activo</th><th>Guardar</th></tr></thead>";
            $web.= "<tbody><tr>";
            while($data_states = $sql_states->fetch()) {
                    $web .= '<tr>';
                    $web .= '<td><input type="text" id="nombreEstado-' . $data_states['id_estado'] . '" name="nombreEstado" value="' . $data_states['nombre'] . '"></td>';
                    $web .= '<td><input type="text" id="descripcionEstado-' . $data_states['id_estado'] . '" name="descripcionEstado" value="' . $data_states['descripcion'] . '"></td>';
                    $web .= '<td><select id="activoEstado-' . $data_states['id_estado'] . '" name="activoEstado">';
                    if ($data_states['activo'] == '0') {
                        $web .= "<option  id='activoEstado-" . $data_states['id_estado'] . "' name='activoEstado' value='0' selected>No</option>";
                        $web .= "<option id='activoEstado-" . $data_states['id_estado'] . "' name='activoEstado' value='1'>Si</option>";
                    } else {
                        $web .= "<option id='activoEstado-" . $data_states['id_estado'] . "' name='activoEstado' value='0'>No</option>";
                        $web .= "<option id='activoEstado-" . $data_states['id_estado'] . "' name='activoEstado' value='1' selected>Si</option>";
                    }

                    $web .= "</select></td>";
                    $web .= '<td><a href="#" onclick="actionGuardar(' . $data_states['id_estado'] . ', ' . $states . ')"><img id="acciones" src="img/save.png"></a></td>';
                    $web .= '</tr>';
                    $web .= '<tr>';
            }

            $web.= "</tbody></tr></table>";
            $web .= "</div><div>";

        }elseif(($rol === 'states') && ($seguridad === 'RQ')){

            $sql_states->execute(array(':id'=>$parametros['id']));
            $web .="<div class='avisos_legales'><br><h1>Estados</h1><br><div class='group'>";
            $web.= "<table class='blueTable'><thead><tr><th>Nombre</th><th>Descripción</th><th>Activo</th></tr></thead>";
            $web.= "<tbody><tr>";
            while($data_states = $sql_states->fetch()) {
                    $web .= '<tr>';
                    $web .= '<td>' . $data_states['nombre'] . '</td>';
                    $web .= '<td>' . $data_states['descripcion'] . '</td>';
                    $web .= '<td>';

                    if ($data_states['activo'] == '0') {
                        $web .= "No";
                    } else {
                        $web .= "Si";
                    }

                    $web .= '</tr>';
                    $web .= '<tr>';
            }

            $web.= "</tbody></tr></table>";
            $web .= "</div><div>";

        }elseif($rol === 'roles'){

            $roles = 5;
            $sql_roles->execute(array(':id'=>$parametros['id']));
            $web .="<div class='avisos_legales'><br><h1>Roles</h1><br><div class='group'>";
            $web .="<input style='margin-left: 10px;' type='button' value='Crear Nueva' onclick='actionCrear($roles);'>";
            $web.= "<table class='blueTable'><thead><tr><th>Nombre</th><th>Descripción</th><th>Siglas</th><th>Activo</th><th>Guardar</th></tr></thead>";
            $web.= "<tbody><tr>";
            while($data_roles = $sql_roles->fetch()) {
                    $web .= '<tr>';
                    $web .= '<td><input type="text" id="nombreRoles-' . $data_roles["id_rol"] . '" name="nombreRoles" value="' . $data_roles['nombre'] . '"></td>';
                    $web .= '<td><input type="text" id="descripcionRoles-' . $data_roles["id_rol"] . '" name="descripcionRoles" value="' . $data_roles['descripcion'] . '"></td>';
                    $web .= '<td><input type="text" id="siglasRoles-' . $data_roles["id_rol"] . '" name="siglasRoles" value="' . $data_roles['siglas'] . '"></td>';
                    $web .= '<td><select id="activoRoles-' . $data_roles["id_rol"] . '" name="activoRoles">';

                    if ($data_roles['activo'] == '0') {
                        $web .= "<option  id='activoRoles-" . $data_roles['id_rol'] . "' name='activoRoles' value='0' selected>No</option>";
                        $web .= "<option id='activoRoles-" . $data_roles['id_rol'] . "' name='activoRoles' value='1'>Si</option>";
                    } else {
                        $web .= "<option id='activoRoles-" . $data_roles['id_rol'] . "' name='activoRoles' value='0'>No</option>";
                        $web .= "<option id='activoRoles-" . $data_roles['id_rol'] . "' name='activoRoles' value='1' selected>Si</option>";
                    }

                    $web .= "</select></td>";
                    $web .= '<td><a href="#" onclick="actionGuardar(' . $data_roles['id_rol'] . ', ' . $roles . ')"><img id="acciones" src="img/save.png"></a></td>';
                    $web .= '</tr>';
                    $web .= '<tr>';
            }

            $web.= "</tbody></tr></table>";
            $web .= "</div><div>";

        }elseif(($rol === 'priorities') && ($seguridad !== 'RQ')){

            $priorities = 6;
            $sql_priorities->execute(array(':id'=>$parametros['id']));
            $web .="<div class='avisos_legales'><br><h1>Prioridades</h1><br><div class='group'>";
            $web .="<input style='margin-left: 10px;' type='button' value='Crear Nueva' onclick='actionCrear($priorities);'>";
            $web.= "<table class='blueTable'><thead><tr><th>Nombre</th><th>Descripción</th><th>Activo</th><th>Guardar</th></tr></thead>";
            $web.= "<tbody><tr>";
            while($data_priorities = $sql_priorities->fetch()){
                    $web .= '<tr>';
                    $web .= '<td><input type="text" id="nombrePrioridad-' . $data_priorities["id_prioridad"] . '" name="nombrePrioridad" value="' . $data_priorities['nombre'] . '"></td>';
                    $web .= '<td><input type="text" id="descripcionPrioridad-' . $data_priorities["id_prioridad"] . '" name="descripcionPrioridad" value="' . $data_priorities['descripcion'] . '"></td>';
                    $web .= '<td><select id="activoPrioridad-' . $data_priorities["id_prioridad"] . '" name="activoPrioridad">';

                    if ($data_priorities['activo'] == '0') {
                        $web .= "<option  id='activoPrioridad-" . $data_priorities['id_prioridad'] . "' name='activoPrioridad' value='0' selected>No</option>";
                        $web .= "<option id='activoPrioridad-" . $data_priorities['id_prioridad'] . "' name='activoPrioridad' value='1'>Si</option>";
                    } else {
                        $web .= "<option id='activoPrioridad-" . $data_priorities['id_prioridad'] . "' name='activoPrioridad' value='0'>No</option>";
                        $web .= "<option id='activoPrioridad-" . $data_priorities['id_prioridad'] . "' name='activoPrioridad' value='1' selected>Si</option>";
                    }

                    $web .= "</select></td>";
                    $web .= '<td><a href="#" onclick="actionGuardar(' . $data_priorities['id_prioridad'] . ', ' . $priorities . ')"><img id="acciones" src="img/save.png"></a></td>';
                    $web .= '</tr>';
                    $web .= '<tr>';
            }

            $web.= "</tbody></tr></table>";
            $web .= "</div><div>";
        }elseif(($rol === 'priorities') && ($seguridad === 'RQ')){

            $sql_priorities->execute(array(':id'=>$parametros['id']));
            $web .="<div class='avisos_legales'><br><h1>Prioridades</h1><br><div class='group'>";
            $web.= "<table class='blueTable'><thead><tr><th>Nombre</th><th>Descripción</th><th>Activo</th></tr></thead>";
            $web.= "<tbody><tr>";
            while($data_priorities = $sql_priorities->fetch()) {
                    $web .= '<tr>';
                    $web .= '<td>' . $data_priorities['nombre'] . '</td>';
                    $web .= '<td>' . $data_priorities['descripcion'] . '</td>';
                    $web .= '<td> ';
                    if ($data_priorities['activo'] == '0') {
                        $web .= "No";
                    } else {
                        $web .= "Si";
                    }

                    $web .= '</tr>';
                    $web .= '<tr>';
            }

            $web.= "</tbody></tr></table>";
            $web .= "</div><div>";
        }elseif(($rol === 'projects') && ($seguridad !== 'RQ')){

            $projects = 7;
            $sql_projects->execute(array(':id'=>$parametros['id']));
            $total = $sql_projects->rowCount();
            $numero = (int)$parametros['numero'];
            $primerNumero = primerNumero($_SESSION['cantidad'], $numero);
            $segundoNumero = $primerNumero + 4;
            $web .="<div class='avisos_legales'><br><h1>Proyectos</h1><br><div class='group'>";
            $web.= "<table class='blueTable'><thead><tr><th>Nombre</th><th>Descripción</th><th>Creación</th><th>Fecha Inicio</th><th>Fecha Fin</th><th>Actualización</th><th>Email Usuario</th><th>Activo</th><th>Guardar</th></tr></thead>";
            $web.= "<tbody><tr>";
            $i=0;
            while($data_projects = $sql_projects->fetch()) {
                if($i >= $primerNumero && $i <= $segundoNumero) {
                    $web .= '<tr>';
                    $web .= '<td><input type="text" id="nombreProyecto-' . $data_projects["id_proyecto"] . '" name="nombreProyecto" value="' . $data_projects['nombre'] . '"></td>';
                    $web .= '<td><input type="text" id="descripcionProyecto-' . $data_projects["id_proyecto"] . '" name="descripcionProyecto" value="' . $data_projects['descripcion'] . '" ></td>';
                    $web .= '<td><input type="text" id="creacionProyecto-' . $data_projects["id_proyecto"] . '" name="creacionProyecto" value="' . $data_projects['fecha_creacion'] . '" ></td>';
                    $web .= '<td><input type="text" id="inicioProyecto-' . $data_projects["id_proyecto"] . '" name="inicioProyecto" value="' . $data_projects['fecha_ini'] . '" ></td>';
                    $web .= '<td><input type="text" id="finProyecto-' . $data_projects["id_proyecto"] . '" name="finProyecto" value="' . $data_projects['fecha_fin'] . '" ></td>';
                    $web .= '<td><input type="text" id="actualizacionProyecto-' . $data_projects["id_proyecto"] . '" name="actualizacionProyecto" value="' . $data_projects['fecha_update'] . '" ></td>';
                    $sql_users->execute();
                    while ($data_users = $sql_users->fetch()) {

                        if ($data_users['id_usuario'] === $data_projects['usuario_id']) {
                            $web .= '<td><input type="text" id="idUsuarioProyecto-' . $data_projects["id_proyecto"] . '" name="idUsuarioProyecto" value="' . $data_users['email'] . '" readonly></td>';
                        }
                    }

                    $web .= '<td><select id="activoProyecto-' . $data_projects["id_proyecto"] . '" name="activoProyecto">';

                    if ($data_projects['activo'] == '0') {
                        $web .= "<option  id='activoProyecto-" . $data_projects['id_proyecto'] . "' name='activoProyecto' value='0' selected>No</option>";
                        $web .= "<option id='activoProyecto-" . $data_projects['id_proyecto'] . "' name='activoProyecto' value='1'>Si</option>";
                    } else {
                        $web .= "<option id='activoProyecto-" . $data_projects['id_proyecto'] . "' name='activoProyecto' value='0'>No</option>";
                        $web .= "<option id='activoProyecto-" . $data_projects['id_proyecto'] . "' name='activoProyecto' value='1' selected>Si</option>";
                    }

                    $web .= "</select></td>";
                    $web .= '<td><a href="#" onclick="actionGuardar(' . $data_projects['id_proyecto'] . ', ' . $projects . ')"><img id="acciones" src="img/save.png"></a></td>';
                    $web .= '</tr>';
                    $web .= '<tr>';
                }
                $i= $i +1;
            }

            $web.= "</tbody></tr></table>";
            $web .= "<div id='paginador'>";
            for ($i = 1; $i <= $total; $i+=5) {
                $numero = intval(($i / 5)) +1;
                $web .= "<input type='button' class='botonPaginador' value='".$numero."' onclick=cargarPaginador('$rol',$numero)>";
            }
            $web .="</div>";
            $web .= "</div><div>";

        }elseif(($rol === 'projects') && ($seguridad === 'RQ')){

            $sql_projects->execute(array(':id'=>$parametros['id']));
            $total = $sql_projects->rowCount();
            $numero = (int)$parametros['numero'];
            $primerNumero = primerNumero($_SESSION['cantidad'], $numero);
            $segundoNumero = $primerNumero + 4;
            $web .="<div class='avisos_legales'><br><h1>Proyectos</h1><br><div class='group'>";
            $web.= "<table class='blueTable'><thead><tr><th>Nombre</th><th>Descripción</th><th>Creación</th><th>Fecha Inicio</th><th>Fecha Fin</th><th>Actualización</th><th>Email Usuario</th><th>Activo</th></tr></thead>";
            $web.= "<tbody><tr>";
            $i=0;
            while($data_projects = $sql_projects->fetch()) {
                if($i >= $primerNumero && $i <= $segundoNumero) {
                    $web .= '<tr>';
                    $web .= '<td>' . $data_projects['nombre'] . '</td>';
                    $web .= '<td>' . $data_projects['descripcion'] . '</td>';
                    $web .= '<td>' . $data_projects['fecha_creacion'] . '</td>';
                    $web .= '<td>' . $data_projects['fecha_ini'] . '</td>';
                    $web .= '<td>' . $data_projects['fecha_fin'] . '</td>';
                    $web .= '<td>' . $data_projects['fecha_update'] . '</td>';
                    $sql_users->execute();
                    while ($data_users = $sql_users->fetch()) {

                        if ($data_users['id_usuario'] === $data_projects['usuario_id']) {
                            $web .= '<td>' . $data_users['email'] . '</td>';
                        }
                    }

                    $web .= '<td>';

                    if ($data_projects['activo'] == '0') {
                        $web .= "No";
                    } else {
                        $web .= "Si";
                    }
                    $web .= '</tr>';
                    $web .= '<tr>';
                }
                $i= $i +1;
            }

            $web.= "</tbody></tr></table>";
            $web .= "<div id='paginador'>";
            for ($i = 1; $i <= $total; $i+=5) {
                $numero = intval(($i / 5)) +1;
                $web .= "<input type='button' class='botonPaginador' value='".$numero."' onclick=cargarPaginador('$rol',$numero)>";
            }
            $web .="</div>";
            $web .= "</div><div>";

        }elseif(($rol === 'chores') && ($seguridad !== 'RQ')){

            $chores = 8;
            $sql_chores->execute(array(':id'=>$parametros['id']));
            $total = $sql_chores->rowCount();
            $numero = (int)$parametros['numero'];
            $primerNumero = primerNumero($_SESSION['cantidad'], $numero);
            $segundoNumero = $primerNumero + 4;
            $web .="<div class='avisos_legales'><br><h1>Tareas</h1><br><div class='group'>";
            $web.= "<table class='blueTable'><thead><tr><th>Nombre</th><th>Descripción</th><th>Creación</th><th>Inicio</th><th>Fin</th><th>Actualización</th><th>Categoría</th><th>Estado</th><th>Proyecto</th><th>Prioridad</th><th>Activo</th><th>Guardar</th></tr></thead>";
            $web.= "<tbody><tr>";
            $i=0;
            while($data_chores = $sql_chores->fetch()) {
                if($i >= $primerNumero && $i <= $segundoNumero) {
                    $web .= '<tr>';
                    $web .= '<td><input type="text" id="nombreTarea-' . $data_chores["id_tarea"] . '" name="nombreTarea" value="' . $data_chores['nombre'] . '" ></td>';
                    $web .= '<td><input type="text" id="descripcionTarea-' . $data_chores["id_tarea"] . '" name="descripcionTarea" value="' . $data_chores['descripcion'] . '" ></td>';
                    $web .= '<td><input type="text" id="creacionTarea-' . $data_chores["id_tarea"] . '" name="creacionTarea" value="' . $data_chores['fecha_creacion'] . '" readonly></td>';
                    $web .= '<td><input type="date" id="inicioTarea-' . $data_chores["id_tarea"] . '" name="inicioTarea" value="' . $data_chores['fecha_inicio'] . '" ></td>';
                    $web .= '<td><input type="date" id="finTarea-' . $data_chores["id_tarea"] . '" name="finTarea" value="' . $data_chores['fecha_final'] . '" ></td>';
                    $web .= '<td><input type="text" id="actualizacionTarea-' . $data_chores["id_tarea"] . '" name="actualizacionTarea" value="' . $data_chores['fecha_update'] . '" readonly></td>';

                    $web .= "<td><select id='id_categoria' name='id_categoria'>";
                    $sql_categories->execute();
                    while ($data_categories = $sql_categories->fetch()) {
                        if ($data_categories['id_categoria'] === $data_chores['categoria_id']) {
                            $web .= "<option  id='idCategoriaTarea-" . $data_chores['id_tarea'] . "' name='idCategoriaTarea' value='" . $data_chores['categoria_id'] . "' selected>'" . $data_categories['nombre'] . "'</option>";
                        }else {
                            $web .= "<option  id='idCategoriaTarea-" . $data_chores['id_tarea'] . "' name='idCategoriaTarea' value='" . $data_chores['categoria_id'] . "'>'" . $data_categories['nombre'] . "'</option>";
                        }
                    }
                    $web .= "</select></td>";

                    $web .= "<td><select id='id_estado' name='id_estado'>";
                    $sql_states->execute();
                    while ($data_states = $sql_states->fetch()) {
                        if ($data_states['id_estado'] === $data_chores['estado_id']) {
                            $web .= "<option  id='idEstadoTarea-" . $data_chores['id_tarea'] . "' name='idEstadoTarea' value='" . $data_chores['estado_id'] . "' selected>'" . $data_states['nombre'] . "'</option>";
                        }else {
                            $web .= "<option  id='idEstadoTarea-" . $data_chores['id_tarea'] . "' name='idEstadoTarea' value='" . $data_chores['estado_id'] . "'>'" . $data_states['nombre'] . "'</option>";
                        }
                    }
                    $web .= "</select></td>";

                    $sql_projects->execute();
                    while ($data_projects = $sql_projects->fetch()) {
                        if ($data_projects['id_proyecto'] === $data_chores['proyecto_id']) {
                            $web .= '<td><input type="text" id="idProyectoTarea-' . $data_chores["id_tarea"] . '" name="idProyectoTarea" value="' . $data_projects['nombre'] . '" readonly></td>';
                        }
                    }

                    $web .= "<td><select id='id_prioridad' name='id_prioridad'>";
                    $sql_priorities->execute();
                    while ($data_priorities = $sql_priorities->fetch()) {
                        if ($data_priorities['id_prioridad'] === $data_chores['prioridad_id']) {
                            $web .= "<option  id='idPrioridadTarea-" . $data_chores['id_tarea'] . "' name='idPrioridadTarea' value='" . $data_chores['prioridad_id'] . "' selected>'" . $data_priorities['nombre'] . "'</option>";
                        }else {
                            $web .= "<option  id='idPrioridadTarea-" . $data_chores['id_tarea'] . "' name='idPrioridadTarea' value='" . $data_chores['prioridad_id'] . "'>'" . $data_priorities['nombre'] . "'</option>";
                        }
                    }
                    $web .= "</select></td>";

                    $web .= '<td><select id="activoTarea-' . $data_chores["id_tarea"] . '" name="activoTarea">';

                    if ($data_chores['activo'] == '0') {
                        $web .= "<option  id='activoTarea-" . $data_chores['id_tarea'] . "' name='activoTarea' value='0' selected>No</option>";
                        $web .= "<option id='activoTarea-" . $data_chores['id_tarea'] . "' name='activoTarea' value='1'>Si</option>";
                    } else {
                        $web .= "<option id='activoTarea-" . $data_chores['id_tarea'] . "' name='activoTarea' value='0'>No</option>";
                        $web .= "<option id='activoTarea-" . $data_chores['id_tarea'] . "' name='activoTarea' value='1' selected>Si</option>";
                    }

                    $web .= "</select></td>";
                    $web .= '<td><a href="#" onclick="actionGuardar(' . $data_chores['id_tarea'] . ', ' . $chores . ')"><img id="acciones" src="img/save.png"></a></td>';
                    $web .= '</tr>';
                    $web .= '<tr>';
                }
                $i= $i +1;
            }

            $web.= "</tbody></tr></table>";
            $web .= "<div id='paginador'>";
            for ($i = 1; $i <= $total; $i+=5) {
                $numero = intval(($i / 5)) +1;
                $web .= "<input type='button' class='botonPaginador' value='".$numero."' onclick=cargarPaginador('$rol',$numero)>";
            }
            $web .="</div>";
            $web .= "</div><div>";

        }elseif(($rol === 'chores') && ($seguridad === 'RQ')){

            $sql_chores->execute(array(':id'=>$parametros['id']));
            $total = $sql_chores->rowCount();
            $numero = (int)$parametros['numero'];
            $primerNumero = primerNumero($_SESSION['cantidad'], $numero);
            $segundoNumero = $primerNumero + 4;
            $web .="<div class='avisos_legales'><br><h1>Tareas</h1><br><div class='group'>";
            $web.= "<table class='blueTable'><thead><tr><th>Nombre</th><th>Descripción</th><th>Creación</th><th>Fecha Inicio</th><th>Fecha Fin</th><th>Actualización</th><th>Categoría</th><th>Estado</th><th>Proyecto</th><th>Prioridad</th><th>Activo</th></tr></thead>";
            $web.= "<tbody><tr>";
            $i=0;
            while($data_chores = $sql_chores->fetch()) {
                if($i >= $primerNumero && $i <= $segundoNumero) {
                    $web .= '<tr>';
                    $web .= '<td>' . $data_chores['nombre'] . '</td>';
                    $web .= '<td>' . $data_chores['descripcion'] . '</td>';
                    $web .= '<td>' . $data_chores['fecha_creacion'] . '</td>';
                    $web .= '<td>' . $data_chores['fecha_inicio'] . '</td>';
                    $web .= '<td>' . $data_chores['fecha_final'] . '</td>';
                    $web .= '<td>' . $data_chores['fecha_update'] . '</td>';
                    $sql_categories->execute();

                    while ($data_categories = $sql_categories->fetch()) {
                        if ($data_categories['id_categoria'] === $data_chores['categoria_id']) {
                            $web .= '<td>' . $data_categories['nombre'] . '</td>';
                        }
                    }
                    $sql_states->execute();
                    while ($data_states = $sql_states->fetch()) {
                        if ($data_states['id_estado'] === $data_chores['estado_id']) {
                            $web .= '<td>' . $data_states['nombre'] . '</td>';
                        }
                    }

                    $sql_projects->execute();
                    while ($data_projects = $sql_projects->fetch()) {
                        if ($data_projects['id_proyecto'] === $data_chores['proyecto_id']) {
                            $web .= '<td>' . $data_projects['nombre'] . '</td>';
                        }
                    }

                    $sql_priorities->execute();
                    while ($data_priorities = $sql_priorities->fetch()) {
                        if ($data_priorities['id_prioridad'] === $data_chores['prioridad_id']) {
                            $web .= '<td>' . $data_priorities['nombre'] . '</td>';
                        }
                    }

                    $web .= '<td>';
                    if ($data_chores['activo'] == '0') {
                        $web .= "No";
                    } else {
                        $web .= "Si";
                    }
                    $web .= '</tr>';
                    $web .= '<tr>';
                }
                $i= $i +1;
            }

            $web.= "</tbody></tr></table>";
            $web .= "<div id='paginador'>";
            for ($i = 1; $i <= $total; $i+=5) {
                $numero = intval(($i / 5)) +1;
                $web .= "<input type='button' class='botonPaginador' value='".$numero."' onclick=cargarPaginador('$rol',$numero)>";
            }
            $web .="</div>";
            $web .= "</div><div>";

        }elseif(($rol === 'users') && ($seguridad !== 'RQ')){

            $users = 9;
            $sql_users->execute(array(':id'=>$parametros['id']));
            $total = $sql_users->rowCount();
            $numero = (int)$parametros['numero'];
            $primerNumero = primerNumero($_SESSION['cantidad'], $numero);
            $segundoNumero = $primerNumero + 4;
            $web .="<div class='avisos_legales'><br><h1>Usuarios</h1><br><div class='group'>";
            $web.= "<div class=buscador>";
            $web.= "<input type=text id=search maxlength=25><input type=button value=Introduce el nombre a buscar onclick=buscarNombre('$rol')></div>";

                if($seguridad == 'PM'){
                    $web.= "<table class='blueTable'><thead><tr><th>Nombre</th><th>Apellidos</th><th>Email</th><th>Teléfono</th><th>Último</th><th>Registro</th><th>Nº OK</th><th>Nª Error</th><th>Roles</th><th>Bloqueado</th><th>Activo</th><th>Guardar</th></tr></thead>";
                }else{
                    $web.= "<table class='blueTable'><thead><tr><th>Nombre</th><th>Apellidos</th><th>Email</th><th>Teléfono</th><th>Último</th><th>Registro</th><th>Nº OK</th><th>Nª Error</th><th>Bloqueado</th><th>Activo</th><th>Guardar</th></tr></thead>";
                }

            $web.= "<tbody><tr>";
            $i=0;
            while($data_users = $sql_users->fetch()) {
                if($i >= $primerNumero && $i <= $segundoNumero) {
                    $web .= '<tr>';
                    $web .= '<td><input type="text" id="nombreUsuario-' . $data_users["id_usuario"] . '" name="nombreUsuario" value="' . $data_users['nombre'] . '"></td>';
                    $web .= '<td><input type="text" id="apellidoUsuario-' . $data_users["id_usuario"] . '" name="apellidoUsuario" value="' . $data_users['apellidos'] . '"></td>';
                    $web .= '<td><input type="text" id="emailUsuario-' . $data_users["id_usuario"] . '" name="emailUsuario" value="' . $data_users['email'] . '"></td>';
                    $web .= '<td><input type="text" id="telefonoUsuario-' . $data_users["id_usuario"] . '" name="telefonoUsuario" value="' . $data_users['telefono'] . '"></td>';
                    $web .= '<td><input type="text" id="ultimoAcceso-' . $data_users["id_usuario"] . '" name="ultimoAcceso" value="' . $data_users['ultimo_acceso'] . '" readonly></td>';
                    $web .= '<td><input type="text" id="fechaRegistro-' . $data_users["id_usuario"] . '" name="fechaRegistro" value="' . $data_users['fecha_registro'] . '" readonly></td>';
                    $web .= '<td><input type="text" id="contadorAccesos-' . $data_users["id_usuario"] . '" name="contadorAccesos" value="' . $data_users['contador_accesos'] . '" readonly></td>';
                    $web .= '<td><input type="text" id="contadorAccesosFallidos-' . $data_users["id_usuario"] . '" name="contadorAccesosFallidos" value="' . $data_users['contador_accesos_fallidos'] . '"></td>';
                    $sql_roles->execute();

                    if ($seguridad == 'PM') {
                        $web .= "<td><select id='rolUsuario' name='rolUsuario'>";
                        while ($data_roles = $sql_roles->fetch()) {
                            if ($data_roles['id_rol'] === $data_users['rol_id']) {
                                $web .= "<option  id='rolUsuario-" . $data_users['id_usuario'] . "' name='rolUsuario' value='" . $data_roles['id_rol'] . "' selected>'" . $data_roles['siglas'] . "'</option>";
                            } else {
                                $web .= "<option  id='rolUsuario-" . $data_users['id_usuario'] . "' name='rolUsuario' value='" . $data_roles['id_rol'] . "'>'" . $data_roles['siglas'] . "'</option>";
                            }
                        }

                        $web .= "</select></td>";
                    }

                    $web .= '<td><select id="bloqueadoUsuario-' . $data_users["id_usuario"] . '" name="bloqueadoUsuario">';

                    if ($data_users['bloqueado'] == '0') {
                        $web .= "<option  id='bloqueadoUsuario-" . $data_users['id_usuario'] . "' name='bloqueadoUsuario' value='0' selected>No</option>";
                        $web .= "<option id='bloqueadoUsuario-" . $data_users['id_usuario'] . "' name='bloqueadoUsuario' value='1'>Si</option>";
                    } else {
                        $web .= "<option id='bloqueadoUsuario-" . $data_users['id_usuario'] . "' name='bloqueadoUsuario' value='0'>No</option>";
                        $web .= "<option id='bloqueadoUsuario-" . $data_users['id_usuario'] . "' name='bloqueadoUsuario' value='1' selected>Si</option>";
                    }

                    $web .= "</select></td>";
                    $web .= '<td><select id="activoUsuario-' . $data_users["id_usuario"] . '" name="activoUsuario">';

                    if ($data_users['activo'] == '0') {
                        $web .= "<option  id='activoUsuario-" . $data_users['id_usuario'] . "' name='activoUsuario' value='0' selected>No</option>";
                        $web .= "<option id='activoUsuario-" . $data_users['id_usuario'] . "' name='activoUsuario' value='1'>Si</option>";
                    } else {
                        $web .= "<option id='activoUsuario-" . $data_users['id_usuario'] . "' name='activoUsuario' value='0'>No</option>";
                        $web .= "<option id='activoUsuario-" . $data_users['id_usuario'] . "' name='activoUsuario' value='1' selected>Si</option>";
                    }

                    $web .= "</select></td>";
                    $web .= '<td><a href="#" onclick="actionGuardar(' . $data_users['id_usuario'] . ', ' . $users . ')"><img id="acciones" src="img/save.png"></a></td>';
                    $web .= '</tr>';
                    $web .= '<tr>';
                }
                $i= $i +1;
            }

            $web.= "</tbody></tr></table>";
            $web .= "<div id='paginador'>";
            for ($i = 1; $i <= $total; $i+=5) {
                $numero = intval(($i / 5)) +1;
                $web .= "<input type='button' class='botonPaginador' value='".$numero."' onclick=cargarPaginador('$rol',$numero)>";
            }
            $web .="</div>";
            $web .= "</div><div>";

        }elseif(($rol === 'users') && ($seguridad === 'RQ')) {

            $sql_users->execute(array(':id' => $parametros['id']));
            $total = $sql_users->rowCount();
            $numero = (int)$parametros['numero'];
            $primerNumero = primerNumero($_SESSION['cantidad'], $numero);
            $segundoNumero = $primerNumero + 4;
            $web .= "<div class='avisos_legales'><br><h1>Usuarios</h1><br><div class='group'>";
            $web .= "<div class='buscador'><input type='text' placeholder='Búsqueda....' value='' id='search' maxlength='25'><input type='button' value='Buscar' onclick='buscar()'></div>";

            $web .= "<table class='blueTable'><thead><tr><th>Nombre</th><th>Apellidos</th><th>Email</th><th>Teléfono</th><th>Último</th><th>Registro</th><th>Nº OK</th><th>Nª Error</th><th>Bloqueado</th><th>Activo</th></tr></thead>";

            $web .= "<tbody><tr>";
            $i=0;
            while ($data_users = $sql_users->fetch()) {
                if($i >= $primerNumero && $i <= $segundoNumero) {
                    $web .= '<tr>';
                    $web .= '<td>' . $data_users['nombre'] . '</td>';
                    $web .= '<td>' . $data_users['apellidos'] . '</td>';
                    $web .= '<td>' . $data_users['email'] . '</td>';
                    $web .= '<td>' . $data_users['telefono'] . '</td>';
                    $web .= '<td>' . $data_users['ultimo_acceso'] . '</td>';
                    $web .= '<td>' . $data_users['fecha_registro'] . '</td>';
                    $web .= '<td>' . $data_users['contador_accesos'] . '</td>';
                    $web .= '<td>' . $data_users['contador_accesos_fallidos'] . '</td>';
                    $sql_roles->execute();

                    $web .= '<td>';

                    if ($data_users['bloqueado'] == '0') {
                        $web .= "No";
                    } else {
                        $web .= "Si";
                    }

                    $web .= '<td>';

                    if ($data_users['activo'] == '0') {
                        $web .= "No";
                    } else {
                        $web .= "Si";
                    }
                    $web .= '</tr>';
                    $web .= '<tr>';
                }
                $i= $i +1;
            }

            $web .= "</tbody></tr></table>";
            $web .= "<div id='paginador'>";
            for ($i = 1; $i <= $total; $i+=5) {
                $numero = intval(($i / 5)) +1;
                $web .= "<input type='button' class='botonPaginador' value='".$numero."' onclick=cargarPaginador('$rol',$numero)>";
            }
            $web .="</div>";
            $web .= "</div><div>";

            $link->commit();
        }
	}catch(PDOException $e){

        $link->rollBack();
        $aux_texto=_("Se ha producido un error al tratar de guardar la información en la base de datos").': ';
        $web.='<script>alert("'.$aux_texto.$e->getMessage() . '");';
        $web.='<script>error_sistema("'.$e->getMessage(). ', '.$e->getFile(). ', '.$e->getLine(). '");';

    }

	return $web;
}

switch($_POST['post_accion']) {
    case 'cargar':
        $web=cargar($_POST);
        echo $web;
        break;
}
?>