
var tamanyo_password				=	9;			// definimos el tamaño que tendrá nuestro password
var caracteres_conseguidos			=	0;			// contador de los caracteres que hemos conseguido
var caracter_temporal				=	'';
var array_caracteres				=	new Array();// array para guardar los caracteres de forma temporal
for(var i = 0; i < tamanyo_password; i++){		// inicializamos el array con el valor null
    array_caracteres[i]	=	null;
}
var password_definitivo				=	'';
var numero_minimo_letras_minusculas	=	1;			// en ésta y las siguientes variables definimos cuántos
var numero_minimo_letras_mayusculas	=	1;			// caracteres de cada tipo queremos en cada
var numero_minimo_numeros			=	1;
var numero_minimo_simbolos			=	1;
var letras_minusculas_conseguidas 	=	0;
var	letras_mayusculas_conseguidas	=	0;
var	numeros_conseguidos				=	0;
var	simbolos_conseguidos			=	0;

function disappear(){
    setTimeout(function(){
        $('#error').html('');
        $('#error').css('display','none')},5000);
}
function disappearConfirm(){
    setTimeout(function(){
        $('#confirm').html('');
        $('#confirm').css('display','none')},5000);
}
function validatePhone(){
    var phone = $('#telefono').val();
    var patt=new RegExp("^(\\+34|0034|34)?[\\s|\\-|\\.]?[6|7|9][\\s|\\-|\\.]?([0-9][\\s|\\-|\\.]?){8}$");
    if (!patt.test(phone)){
        $('#error').css('display','block');
        $('#error').html('Por favor introduzca el teléfono correctamente.');
        $('#telefono').css('border', 'thin solid red');
        disappear();
    }else{
        $('#telefono').css('border', 'none');
    }
}
function validatePhone(phone){
    var valor = false;
    var patt=new RegExp("^(\\+34|0034|34)?[\\s|\\-|\\.]?[6|7|9][\\s|\\-|\\.]?([0-9][\\s|\\-|\\.]?){8}$");

    if (!patt.test(phone)){
        valor = true;
    }
    return valor;
}
function validatePhone2(){
    var valor = false;
    var phone = $('#telefono').val();
    var patt=new RegExp("^(\\+34|0034|34)?[\\s|\\-|\\.]?[6|7|9][\\s|\\-|\\.]?([0-9][\\s|\\-|\\.]?){8}$");
    if (!patt.test(phone)){
        valor = true;
    }
    return valor;
}
function validateMail(){
    var email = $('#mail').val();
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(!re.test(email)){
        return false
    }
}
function validateEmail(){
    var email = $('#email').val();
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(!re.test(email)){
        $('#error').css('display','block');
        $('#error').html('Por favor escriba correctamente en email.');
        $('#email').css('border', 'thin solid red');
        disappear();
    }else{
        $('#email').css('border', 'none');
    }
}
function validateEmail2(){
    var email = $('#email').val();
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var valor = false;
    if(!re.test(email)){
        valor = true;
    }
    return valor;
}
function validateEmail3(){
    var email = $('#form_email').val();
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var valor = false;
    if(!re.test(email)){
        valor = true;
    }
    return valor;
}
function cleanEdges(){
    $('#nick_form').css('border', 'none');
    $('#nombre_form').css('border', 'none');
    $('#apellidos_form').css('border', 'none');
    $('#email').css('border', 'none');
    $('#password1').css('border', 'none');
    $('#password2').css('border', 'none');
    $('#telefono').css('border', 'none');
    $('#password').css('border', 'none');
    $('#mail').css('border', 'none');
}
function repeatPassword(){
    if($('#password1').val() != $('#password2').val()){
        $('#error').css('display','block');
        $('#error').html('El segundo password debe coincidir con el primero.');
        $('#password2').css('border', 'thin solid red');
        disappear();
    }else{
        $('#password2').css('border', 'none');
    }
}
function repeatPassword2(){
    var valor = false;
    if($('#password1').val() != $('#password2').val()){
        valor = true;
    }
    return valor;
}
function validateEmpty(){
    var valor = false;
    if ($('#nick_form').val() == '') {
        $('#nick_form').css('border', 'thin solid red');
        valor = true;
    }
    if ($('#nombre_form').val() == '') {
        $('#nombre_form').css('border', 'thin solid red');
        valor = true;
    }
    if ($('#apellidos_form').val() == '') {
        $('#apellidos_form').css('border', 'thin solid red');
        valor = true;
    }
    if ($('#email').val() == '') {
        $('#email').css('border', 'thin solid red');
        valor = true;
    }
    if ($('#password1').val() == '') {
        $('#password1').css('border', 'thin solid red');
        valor = true;
    }
    if ($('#password2').val() == '') {
        $('#password2').css('border', 'thin solid red');
        valor = true;
    }
    if ($('#telefono').val() == '') {
        $('#telefono').css('border', 'thin solid red');
        valor = true;
    }
    if ($('#nombre_form').val() == '') {
        $('#nombre_form').css('border', 'thin solid red');
        valor = true;
    }
    return valor;
}
function validateRegistry(){
    cleanEdges();
    if (validateEmpty() === true) {
        $('#error').css('display','block');
        $('#error').html('Existe uno o varios campos sin rellenar.');
        $('#password2').css('border', 'thin solid red');
        disappear();
    }
    else if((validateEmail2() === true) && (validatePhone2() === true) && (repeatPassword2() === true)){
        $('#error').css('display','block');
        $('#error').html('Existen varios errores, revise los campos marcados.');
        $('#email').css('border', 'thin solid red');
        $('#telefono').css('border', 'thin solid red');
        $('#password2').css('border', 'thin solid red');
        disappear();
    }
    else if((validateEmail2() === false) && (validatePhone2() === true) && (repeatPassword2() === true)){
        $('#error').css('display','block');
        $('#error').html('Existen varios errores, revise los campos marcados.');
        $('#telefono').css('border', 'thin solid red');
        $('#password2').css('border', 'thin solid red');
        disappear();
    }
    else if((validateEmail2() === true) && (validatePhone2() === false) && (repeatPassword2() === true)){
        $('#error').css('display','block');
        $('#error').html('Existen varios errores, revise los campos marcados.');
        $('#email').css('border', 'thin solid red');
        $('#password2').css('border', 'thin solid red');
        disappear();
    }
    else if((validateEmail2() === true) && (validatePhone2() === true) && (repeatPassword2() === false)){
        $('#error').css('display','block');
        $('#error').html('Existen varios errores, revise los campos marcados.');
        $('#telefono').css('border', 'thin solid red');
        $('#email').css('border', 'thin solid red');
        disappear();
    }
    else if((validateEmail2() === true) && (validatePhone2() === false) && (repeatPassword2() === false)){
        $('#error').css('display','block');
        $('#error').html('Revise el campo marcado.');
        $('#email').css('border', 'thin solid red');
        disappear();
    }
    else if((validateEmail2() === false) && (validatePhone2() === true) && (repeatPassword2() === false)){
        $('#error').css('display','block');
        $('#error').html('Revise el campo marcado.');
        $('#telefono').css('border', 'thin solid red');
        disappear();
    }
    else if((validateEmail2() === false) && (validatePhone2() === false) && (repeatPassword2() === true)){
        $('#error').css('display','block');
        $('#error').html('Revise el campo marcado.');
        $('#password2').css('border', 'thin solid red');
        disappear();
    }
    else{
        disappear();
        xajax_validationEmail($('#nombre_form').val(), $('#apellidos_form').val(), $('#email').val(), $('#password1').val(), $('#telefono').val());
    }
}
function loguear(){
    cleanEdges();
    if(validateMail() === false){
        $('#error').css('display','block');
        $('#error').html('Por favor escriba correctamente en email.');
        $('#mail').css('border', 'thin solid red');
    }else{
        var email = $('#mail').val();
        var password = $('#password').val();
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if((email === '') && (password === '')){
            $('#error').css('display','block');
            $('#error').html('Hay algún problema en tu formulario, revisa los campos o campo marcado.');
            $('#mail').css('border', 'thin solid red');
            $('#password').css('border', 'thin solid red');
            disappear();
        }else if((email !== '') && (password === '')){
            if(!re.test(email)){
                $('#error').css('display','block');
                $('#error').html('Hay algún problema en tu formulario, revisa los campos o campo marcado.');
                $('#mail').css('border', 'thin solid red');
                $('#password').css('border', 'thin solid red');
                disappear();
            }else {
                $('#error').css('display', 'block');
                $('#error').html('Hay algún problema en tu formulario, revisa los campos o campo marcado.');
                $('#password').css('border', 'thin solid red');
                disappear();
            }
        }else if((email === '') && (password !== '')){
            $('#error').css('display','block');
            $('#error').html('Hay algún problema en tu formulario, revisa los campos o campo marcado.');
            $('#mail').css('border', 'thin solid red');
            disappear();
        }else if((email !== '') && (password !== '')){
            if(!re.test(email)){
                $('#error').css('display','block');
                $('#error').html('Hay algún problema en tu formulario, revisa los campos o campo marcado.');
                $('#mail').css('border', 'thin solid red');
                disappear();
            }else {
                $('#mail').css('border', 'none');
                xajax_validationLogIn($('#mail').val(), $('#password').val());
                disappear();
            }
        }
    }

}
function sendEmail(){
    var email = $('#form_email').val();
    if((validateEmail3() === true) || (email === '')){
        $('#error').css('display','block');
        $('#error').css('z-index','2000');
        $('#error').html('Por favor introduzca correctamente su direccion de email.');
        $('#email').css('border', 'thin solid red');
        disappear();
    }else{
        var newPassword = generar_contrasenya();
        xajax_sendEmail(email, newPassword);
    }
}
function genera_aleatorio(i_numero_inferior, i_numero_superior){
    var     i_aleatorio  =   Math.floor((Math.random() * (i_numero_superior - i_numero_inferior + 1)) + i_numero_inferior);
    return  i_aleatorio;
}
function genera_caracter(tipo_de_caracter){

    var lista_de_caracteres	=	'$+=?@_23456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz';
    var caracter_generado	=	'';
    var valor_inferior		=	0;
    var valor_superior		=	0;

    switch (tipo_de_caracter){
        case 'minúscula':
            valor_inferior	=	38;
            valor_superior	=	61;
            break;
        case 'mayúscula':
            valor_inferior	=	14;
            valor_superior	=	37;
            break;
        case 'número':
            valor_inferior	=	6;
            valor_superior	=	13;
            break;
        case 'símbolo':
            valor_inferior	=	0;
            valor_superior	=	5;
            break;
        case 'aleatorio':
            valor_inferior	=	0;
            valor_superior	=	61;

    } // fin del switch

    caracter_generado	=	lista_de_caracteres.charAt(genera_aleatorio(valor_inferior, valor_superior));
    return caracter_generado;
}
function guarda_caracter_en_posicion_aleatoria(caracter_pasado_por_parametro){
    var guardado_en_posicion_vacia	=	false;
    var posicion_en_array			=	0;

    while(guardado_en_posicion_vacia	!=	true){
        posicion_en_array	=	genera_aleatorio(0, tamanyo_password-1);	// generamos un aleatorio en el rango del tamaño del password

        // el array ha sido inicializado con null en sus posiciones. Si es una posición vacía, guardamos el caracter
        if(array_caracteres[posicion_en_array] == null){
            array_caracteres[posicion_en_array]	=	caracter_pasado_por_parametro;
            guardado_en_posicion_vacia			=	true;
        }
    }
}
function generar_contrasenya(){

    while (letras_minusculas_conseguidas < numero_minimo_letras_minusculas){
        caracter_temporal	=	genera_caracter('minúscula');
        guarda_caracter_en_posicion_aleatoria(caracter_temporal);
        letras_minusculas_conseguidas++;
        caracteres_conseguidos++;
    }

    while (letras_mayusculas_conseguidas < numero_minimo_letras_mayusculas){
        caracter_temporal	=	genera_caracter('mayúscula');
        guarda_caracter_en_posicion_aleatoria(caracter_temporal);
        letras_mayusculas_conseguidas++;
        caracteres_conseguidos++;
    }

    while (numeros_conseguidos < numero_minimo_numeros){
        caracter_temporal	=	genera_caracter('número');
        guarda_caracter_en_posicion_aleatoria(caracter_temporal);
        numeros_conseguidos++;
        caracteres_conseguidos++;
    }

    while (simbolos_conseguidos < numero_minimo_simbolos){
        caracter_temporal	=	genera_caracter('símbolo');
        guarda_caracter_en_posicion_aleatoria(caracter_temporal);
        simbolos_conseguidos++;
        caracteres_conseguidos++;
    }

    while (caracteres_conseguidos < tamanyo_password){
        caracter_temporal	=	genera_caracter('aleatorio');
        guarda_caracter_en_posicion_aleatoria(caracter_temporal);
        caracteres_conseguidos++;
    }

    for(var i=0; i < array_caracteres.length; i++){
        password_definitivo	=	password_definitivo + array_caracteres[i];
    }
    return password_definitivo;
}
function cargarPaginador(valor, numero){
    getInformation(valor,numero);
}
function inicio(){window.location='./index.php;'}
function actionGuardar(id, action){
    //Categorias 3
    var nombreCategoria = $("#nombreCategoria-"+id).val();
    var descripcionCategoria = $("#descripcionCategoria-"+id).val();
    var activoCategoria = $("#activoCategoria-"+id).val();

    //Estados 4
    var nombreEstado = $('#nombreEstado-'+id).val();
    var descripcionEstado = $('#descripcionEstado-'+id).val();
    var activoEstado = $('#activoEstado-'+id).val();
    //Roles 5
    var nombreRoles = $('#nombreRoles-'+id).val();
    var descripcionRoles = $('#descripcionRoles-'+id).val();
    var siglasRoles = $('#siglasRoles-'+id).val();
    var activoRoles = $('#activoRoles-'+id).val();
    //Prioridad 6
    var nombrePrioridad = $('#nombrePrioridad-'+id).val();
    var descripcionPrioridad = $('#descripcionPrioridad-'+id).val();
    var activoPrioridad = $('#activoPrioridad-'+id).val();
    //Proyectos 7
    var nombreProyecto = $('#nombreProyecto-'+id).val();
    var descripcionProyecto = $('#descripcionProyecto-'+id).val();
    var creacionProyecto = $('#creacionProyecto-'+id).val();
    var inicioProyecto = $('#inicioProyecto-'+id).val();
    var finProyecto = $('#finProyecto-'+id).val();
    var actualizacionProyecto = $('#actualizacionProyecto-'+id).val();
    var activoProyecto = $('#activoProyecto-'+id).val();
    var idUsuarioProyecto = $('#idUsuarioProyecto-'+id).val();
    //Tareas 8
    var nombreTarea = $('#nombreTarea-'+id).val();
    var descripcionTarea = $('#descripcionTarea-'+id).val();
    var creacionTarea = $('#creacionTarea-'+id).val();
    var inicioTarea = $('#inicioTarea-'+id).val();
    var finTarea = $('#finTarea-'+id).val();
    var actualizacionTarea = $('#actualizacionTarea-'+id).val();
    var idCategoriaTarea = $('#idCategoriaTarea-'+id).val();
    var idEstadoTarea = $('#idEstadoTarea-'+id).val();
    var idProyectoTarea = $('#idProyectoTarea-'+id).val();
    var idPrioridadTarea = $('#idPrioridadTarea-'+id).val();
    var activoTarea = $('#activoTarea-'+id).val();
    //Usuarios 9
    var nombreUsuario = $('#nombreUsuario-'+id).val();
    var apellidoUsuario = $('#apellidoUsuario-'+id).val();
    var emailUsuario = $('#emailUsuario-'+id).val();
    var telefonoUsuario = $('#telefonoUsuario-'+id).val();
    var ultimoAcceso = $('#ultimoAcceso-'+id).val();
    var fechaRegistro = $('#fechaRegistro-'+id).val();
    var contadorAccesos = $('#contadorAccesos-'+id).val();
    var contadorAccesosFallidos = $('#contadorAccesosFallidos-'+id).val();
    var rolUsuario = $('#rolUsuario-'+id).val();
    var bloqueadoUsuario = $('#bloqueadoUsuario-'+id).val();
    var activoUsuario = $('#activoUsuario-'+id).val();

    if(action === 3){
        $.ajax({
            url: 'functions/php/functionShow.php',
            type: "POST",
            data: "id="+id+"&action="+action+"&nombreCategoria="+nombreCategoria+"&descripcionCategoria="+descripcionCategoria+"&activoCategoria="+activoCategoria+"&post_accion=updateCategoria",
            //dataType: "json",
            success: function(data){
                $("#results").html(data);
            }
        });
    }
    if(action === 4){
        $.ajax({
            url: 'functions/php/functionShow.php',
            type: "POST",
            data: "id="+id+"&action="+action+"&nombreEstado="+nombreEstado+"&descripcionEstado="+descripcionEstado+"&activoEstado="+activoEstado+"&post_accion=updateEstado",
            //dataType: "json",
            success: function(data){
                $("#results").html(data);
            }
        });
    }
    if(action === 5){
        $.ajax({
            url: 'functions/php/functionShow.php',
            type: "POST",
            data: "id="+id+"&action="+action+"&nombreRoles="+nombreRoles+"&descripcionRoles="+descripcionRoles+"&siglasRoles="+siglasRoles+"&activoRoles="+activoRoles+"&post_accion=updateRol",
            //dataType: "json",
            success: function(data){
                $("#results").html(data);
            }
        });
    }
    if(action === 6){
        $.ajax({
            url: 'functions/php/functionShow.php',
            type: "POST",
            data: "id="+id+"&action="+action+"&nombrePrioridad="+nombrePrioridad+"&descripcionPrioridad="+descripcionPrioridad+"&activoPrioridad="+activoPrioridad+"&post_accion=updatePrioridad",
            //dataType: "json",
            success: function(data){
                $("#results").html(data);
            }
        });
    }
    if(action === 7){
        $.ajax({
            url: 'functions/php/functionShow.php',
            type: "POST",
            data: "id="+id+"&action="+action+"&nombreProyecto="+nombreProyecto+"&descripcionProyecto="+descripcionProyecto+"&creacionProyecto="+creacionProyecto+
            "&inicioProyecto="+inicioProyecto+"&finProyecto="+finProyecto+"&actualizacionProyecto="+actualizacionProyecto+"&activoProyecto="+activoProyecto+
            "&post_accion=updateProyecto",
            //dataType: "json",
            success: function(data){
                $("#results").html(data);
            }
        });
    }
    if(action === 8){
        $.ajax({
            url: 'functions/php/functionShow.php',
            type: "POST",
            data: "id="+id+"&action="+action+"&nombreTarea="+nombreTarea+"&descripcionTarea="+descripcionTarea+"&creacionTarea="+creacionTarea+
            "&inicioTarea="+inicioTarea+"&finTarea="+finTarea+"&actualizacionTarea="+actualizacionTarea+"&idCategoriaTarea="+idCategoriaTarea+
            "&idEstadoTarea="+idEstadoTarea+"&idProyectoTarea="+idProyectoTarea+"&idPrioridadTarea="+idPrioridadTarea+"&activoTarea="+activoTarea+"&post_accion=updateTarea",
            //dataType: "json",
            success: function(data){
                $("#results").html(data);
            }
        });
    }
    if(action === 9){
        $.ajax({
            url: 'functions/php/functionShow.php',
            type: "POST",
            data: "id="+id+"&action="+action+"&nombreUsuario="+nombreUsuario+"&apellidoUsuario="+apellidoUsuario+"&emailUsuario="+emailUsuario+
            "&telefonoUsuario="+telefonoUsuario+"&ultimoAcceso="+ultimoAcceso+"&fechaRegistro="+fechaRegistro+"&contadorAccesos="+contadorAccesos+"&activoUsuario="+activoUsuario+
            "&contadorAccesos="+contadorAccesos+"&contadorAccesosFallidos="+contadorAccesosFallidos+"&rolUsuario="+rolUsuario+"&bloqueadoUsuario="+bloqueadoUsuario+"&post_accion=updateUsuario",
            //dataType: "json",
            success: function(data){
                $("#results").html(data);
            }
        });
    }
}
function actionCrear(action){

    $.ajax({
        url: 'functions/php/functionShow.php',
        type: "POST",
        data: "action="+action+"&post_accion=createNew",
        //dataType: "json",
        success: function(data){
            $("#results").html(data);
        }
    });

}
function getCalendar(){
    hoy=new Date();
    $("#results").html('');
    //console.log(escribirCalendario(hoy));
    $("#results").html(escribirCalendario(hoy));
}
function guardarRegistro(){
    var id = $('#user_id').val();
    var nombre = $('#user_nombre').val();
    var apellidos = $('#user_apellidos').val();
    var email = $('#user_email').val();
    var telefono = $('#user_telefono').val();

    $.ajax({
        url: 'functions/php/functionShow.php',
        type: "POST",
        data: "telefono="+telefono+"&email="+email+"&nombre="+nombre+"&apellidos="+apellidos+"&id="+id+"&post_accion=guardar",
        //dataType: "json",
        success: function(data){
            $("#results").html(data);
        }
    });
}
function showInformation(number){
    $.ajax({
        url: 'functions/php/functionShow.php',
        type: "POST",
        data: "number="+number+"&post_accion=informacion",
        //dataType: "json",
        success: function(data){
            $("#results").html(data);
        }
    });
}
function createChore(){
    var newChore = validateNewChore();

    var nameChore = $('#nameChore').val();
    var descriptionChore = $('#descriptionChore').val();
    var projectsChore = $('#projectsChore').val();
    var fecha_iniChore = $('#fecha_iniChore').val();
    var fecha_finChore = $('#fecha_finChore').val();
    var categoriesChore = $('#categoriesChore').val();
    var statesChore = $('#statesChore').val();
    var prioritiesChore = $('#prioritiesChore').val();

    if(newChore === false){
        $.ajax({
            url: 'functions/php/functionShow.php',
            type: "POST",
            data: "nameChore="+nameChore+"&descriptionChore="+descriptionChore+"&projectsChore="+projectsChore+"&fecha_iniChore="+fecha_iniChore
            +"&fecha_finChore="+fecha_finChore+"&categoriesChore="+categoriesChore+"&statesChore="+statesChore+"&prioritiesChore="+prioritiesChore+"&post_accion=createChore",
            //dataType: "json",
            success: function(data){
                $("#results").html(data);
            }
        });
    }
}
function createProject(){
    var newProject = validateNewProject();

    var project = $('#nameProject').val();
    var description = $('#description').val();
    var fecha_ini = $('#fecha_ini').val();
    var fecha_fin = $('#fecha_fin').val();

    if(newProject === false){
        $.ajax({
            url: 'functions/php/functionShow.php',
            type: "POST",
            data: "project="+project+"&description="+description+"&fecha_ini="+fecha_ini+"&fecha_fin="+fecha_fin+"&post_accion=createProject",
            //dataType: "json",
            success: function(data){
                $("#results").html(data);
            }
        });
    }
}
function crear(number){
    //console.log(number);
    $.ajax({
        url: 'functions/php/functionShow.php',
        type: "POST",
        data: "number="+number+"&post_accion=crear",
        //dataType: "json",
        success: function(data){
            $("#results").html(data);
        }
    });
}
function contact(){
    $.ajax({
        url: 'functions/php/functionShow.php',
        type: "POST",
        data: "&post_accion=contact",
        //dataType: "json",
        success: function(data){
            $("#results").html(data);
        }
    });
}
function reiniciarBordes(){
    $('#name').css('border', 'thin solid #cccccc');
    $('#surname').css('border', 'thin solid #cccccc');
    $('#nameProject').css('border', 'thin solid #cccccc');
    $('#description').css('border', 'thin solid #cccccc');
    $('#fecha_ini').css('border', 'thin solid #cccccc');
    $('#fecha_fin').css('border', 'thin solid #cccccc');
    $('#email').css('border', 'thin solid #cccccc');
    $('#message').css('border', 'thin solid #cccccc');
    $('#nameChore').css('border', 'thin solid #cccccc');
    $('#descriptionChore').css('border', 'thin solid #cccccc');
    $('#projectsChore').css('border', 'thin solid #cccccc');
    $('#fecha_iniChore').css('border', 'thin solid #cccccc');
    $('#fecha_finChore').css('border', 'thin solid #cccccc');
    $('#categoriesChore').css('border', 'thin solid #cccccc');
    $('#statesChore').css('border', 'thin solid #cccccc');
    $('#prioritiesChore').css('border', 'thin solid #cccccc');
    $('.error_legal').css('color', 'black');
}
function validateMail(){
    var email = $('#email').val();
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var valor = false;
    if(!re.test(email)){
        valor = true;
    }
    return valor;
}
function validateForm(){
    reiniciarBordes();

    var name = $('#name').val();
    var surname = $('#surname').val();
    var email = $('#email').val();
    var message = $('#message').val();
    var valor = false;
    var vaEmail = validateMail();
    var checkBox = document.getElementById("condiciones");

    if (checkBox.checked === false){
        $('.error_legal').css('color', 'red');
        valor = true;
    }

    if(name === ''){
        $('#name').css('border', 'thin solid red');
        valor = true;
    }
    if(surname === ''){
        $('#surname').css('border', 'thin solid red');
        valor = true;
    }
    if((email === '') || (vaEmail === true)){
        $('#email').css('border', 'thin solid red');
        valor = true;
    }
    if(message === ''){
        $('#message').css('border', 'thin solid red');
        valor = true;
    }
    return valor;
}
function validateNewChore(){
    reiniciarBordes();

    var nameChore = $('#nameChore').val();
    var descriptionChore = $('#descriptionChore').val();
    var projectsChore = $('#projectsChore').val();
    var fecha_iniChore = $('#fecha_iniChore').val();
    var fecha_finChore = $('#fecha_finChore').val();
    var categoriesChore = $('#categoriesChore').val();
    var statesChore = $('#statesChore').val();
    var prioritiesChore = $('#prioritiesChore').val();
    var valor = false;

    if(nameChore === ''){
        $('#nameChore').css('border', 'thin solid red');
        valor = true;
    }
    if(descriptionChore === ''){
        $('#descriptionChore').css('border', 'thin solid red');
        valor = true;
    }
    if(projectsChore === 'seleccione'){
        $('#projectsChore').css('border', 'thin solid red');
        valor = true;
    }
    if(fecha_iniChore === ''){
        $('#fecha_iniChore').css('border', 'thin solid red');
        valor = true;
    }
    if(fecha_finChore === ''){
        $('#fecha_finChore').css('border', 'thin solid red');
        valor = true;
    }
    if((Date.parse(fecha_iniChore)) > (Date.parse(fecha_finChore))){
        $('#fecha_finChore').css('border', 'thin solid red');
        $('#fecha_iniChore').css('border', 'thin solid red');
        valor = true;
    }
    if(categoriesChore === 'seleccione'){
        $('#categoriesChore').css('border', 'thin solid red');
        valor = true;
    }
    if(statesChore === 'seleccione'){
        $('#statesChore').css('border', 'thin solid red');
        valor = true;
    }
    if(prioritiesChore === 'seleccione'){
        $('#prioritiesChore').css('border', 'thin solid red');
        valor = true;
    }


    return valor;
}
function validateNewProject(){
    reiniciarBordes();

    var nameProject = $('#nameProject').val();
    var description = $('#description').val();
    var fecha_ini = $('#fecha_ini').val();
    var fecha_fin = $('#fecha_fin').val();
    var valor = false;

    if(nameProject === ''){
        $('#nameProject').css('border', 'thin solid red');
        valor = true;
    }
    if(description === ''){
        $('#description').css('border', 'thin solid red');
        valor = true;
    }
    if(fecha_ini === ''){
        $('#fecha_ini').css('border', 'thin solid red');
        valor = true;
    }
    if(fecha_fin === ''){
        $('#fecha_fin').css('border', 'thin solid red');
        valor = true;
    }
    if((Date.parse(fecha_ini)) > (Date.parse(fecha_fin))){
        $('#fecha_fin').css('border', 'thin solid red');
        $('#fecha_ini').css('border', 'thin solid red');
        valor = true;
    }
    return valor;
}
function recogerDatos(){
    var formulario = validateForm();

    var name = $('#name').val();
    var surname = $('#surname').val();
    var email = $('#email').val();
    var theme = $('#theme').val();
    var message = $('#message').val();

    if(formulario === false){
        xajax_saveIn(name, surname, email, theme, message, email);
    }else{
        alert('Revise los campos señalados en el formulario.');
    }
}
function actionDelete(id, tabla){
    var opcion = confirm("¿Está seguro que desea borrarel objeto señalado?");

    if(opcion === true){
        if(tabla === 1){
            var borrar = 'proyecto';
        }
        else
        {
            var borrar = 'tarea';
        }
        $.ajax({
            url: 'functions/php/functionShow.php',
            type: "POST",
            data: "id="+id+"&borrar="+borrar+"&post_accion=borrar",
            //dataType: "json",
            success: function(data){
                $("#results").html(data);
            }
        });
    }else{
        location.href = 'index.php';
    }
}
function actionEdit(id, tabla){

    var opcion = confirm("¿Está seguro que desea editar el objeto señalado?");
    if (opcion == true) {
        if(tabla === 1){
            var editar = 'proyecto';
        }
        else
        {
            var editar = 'tarea';
        }
        $.ajax({
            url: 'functions/php/functionShow.php',
            type: "POST",
            data: "id="+id+"&editar="+editar+"&post_accion=editar",
            //dataType: "json",
            success: function(data){
                $("#results").html(data);
            }
        });
    }else{
        location.href = 'index.php';
    }
}
function updateProject(){
    var newProject = validateNewProject();

    var nameProject = $('#nameProject').val();
    var description = $('#description').val();
    var fecha_ini = $('#fecha_ini').val();
    var fecha_fin = $('#fecha_fin').val();
    var id = $('#id_oculta').val();

    if(newProject === false){
        $.ajax({
            url: 'functions/php/functionShow.php',
            type: "POST",
            data: "nameProject="+nameProject+"&description="+description+"&fecha_ini="+fecha_ini+"&fecha_fin="+fecha_fin+"&id="+id+"&post_accion=updateProject",
            //dataType: "json",
            success: function(data){
                $("#results").html(data);
            }
        });
    }
}
function updateChore(){
    var newChore = validateNewChore();

    var nameChore = $('#nameChore').val();
    var descriptionChore = $('#descriptionChore').val();
    var projectsChore = $('#projectsChore').val();
    var fecha_iniChore = $('#fecha_iniChore').val();
    var fecha_finChore = $('#fecha_finChore').val();
    var categoriesChore = $('#categoriesChore').val();
    var statesChore = $('#statesChore').val();
    var prioritiesChore = $('#prioritiesChore').val();
    var id = $('#od_oculta_chore').val();

    if(newChore === false){
        $.ajax({
            url: 'functions/php/functionShow.php',
            type: "POST",
            data: "nameChore="+nameChore+"&id="+id+"&descriptionChore="+descriptionChore+"&projectsChore="+projectsChore+"&fecha_iniChore="+fecha_iniChore+"&fecha_finChore="+fecha_finChore+"&categoriesChore="+categoriesChore+"&statesChore="+statesChore+"&prioritiesChore="+prioritiesChore+"&post_accion=updateChore",
            //dataType: "json",
            success: function(data){
                $("#results").html(data);
            }
        });
    }
}
function openPDF(numero){
    var direccion = '';
    if(numero === 1){
        direccion = './documentacion/cookies.pdf';
    }
    if(numero === 2){
        direccion = './documentacion/privacidad.pdf';
    }
    if(numero === 3){
        direccion = './documentacion/legal.pdf';
    }
    if(numero === 4){
        direccion = './documentacion/help.pdf';
    }
    window.open(direccion,"_blank");
}