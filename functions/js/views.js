
// Inicializaci�n de Variables
Ancho = 140;						//ancho de la tabla

Fondo 			= "#1776fe";		//color de la celda
FondoFestivo 	= "#FEBF7D";		//color de la celda en d�a festivo
FondoTitulo 	= "#1776fe";		//color de fondo para el t�tulo
FondoDiasSemana = "#ffac13";		//color de fondo para los d�as de la semana

colorLinea 		= "#FFFFFF";		//color de la linea entre las celdas

cp = 1;								// cellpadding
cs = 1;								// cellspacing

Meses = new Array;
Meses[0]  = "Enero";
Meses[1]  = "Febrero";
Meses[2]  = "Marzo";
Meses[3]  = "Abril";
Meses[4]  = "Mayo";
Meses[5]  = "Junio";
Meses[6]  = "Julio";
Meses[7]  = "Agosto";
Meses[8]  = "Septiembre";
Meses[9]  = "Octubre";
Meses[10] = "Noviembre";
Meses[11] = "Diciembre";

DiasSemana = new Array ("L","M","X","J","V","S","D");
DiasMes = new Array(31,28,31,30,31,30,31,31,30,31,30,31);


function escribirCalendario(fecha){

    dia = fecha.getDate();
    diaSemana = fecha.getDay();
    mes = fecha.getMonth();
    anno = fecha.getYear();

    if (anno%4 == 0) {
        DiasMes[1] = 29

    }

    stTexto = "<div id='calendario'>";
    stTexto += "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\">";
    stTexto += "<tr>";
    stTexto += "	<td bgcolor=\"" + colorLinea + "\">";
    stTexto += "		<table cellpadding=\"" + cp +"\" cellspacing=\"" + cs + "\" border=\"0\" width=\"" + Ancho + "\">";
    stTexto += "		<tr>";
    stTexto += "			<td colspan=\"7\" bgcolor=\"" + FondoTitulo + "\" class=\"titulo\" align=\"center\"><b>&nbsp;" + Meses[mes].toUpperCase() + "&nbsp;</b></td>";
    stTexto += "		</tr>";
    stTexto += "		<tr>";
    
     for(i=0;i<7;i++){
        stTexto += "<td bgcolor=\"" + FondoDiasSemana + "\" class=\"txt\" align=\"center\"><b>&nbsp;" + DiasSemana[i] + "&nbsp;</b></td>";
    }


    stTexto  += "";
    stTexto += "		</tr>\n";

    aux = (dia - diaSemana + 1)%7

    if (aux > 1) {
        aux = aux - 7;
    }

    nS = Math.ceil((DiasMes[mes] - aux + 1)/7);
    for (var numSemana = 0; numSemana < nS; numSemana++){
        stTexto += "<tr>";
        for (i=0; i<7 ; i++){

            if (i > 4) {
                fondo = FondoFestivo;
                clase = "txtFestivo";
            } else {
                fondo = Fondo;
                clase = "txt";
            }

            iAux = aux + i + numSemana*7;
            if (iAux < 1){
                stTexto += "<td align=\"center\" bgcolor=\"" + fondo +"\" class=\"" + clase +"\">&nbsp;</td>";
            }else if (iAux < DiasMes[mes] + 1 ){

                if (iAux == dia){
                    stTexto += "<td align=\"center\" bgcolor=\"" + fondo +"\" class=\"" + clase +"\"><b>" + iAux + "</b></td>";
                }else{
                    stTexto += "<td align=\"center\" bgcolor=\"" + fondo +"\" class=\"" + clase +"\">" + iAux + "</td>";
                }
            }else{
                stTexto += "<td align=\"center\" bgcolor=\"" + fondo +"\" class=\"" + clase +"\">&nbsp;</td>";
            }
        }

        stTexto += "</tr>";
    }

    stTexto += "		</tr>";
    stTexto += "		</table>";
    stTexto += "	</td>";
    stTexto += "</tr>";
    stTexto += "</table>";
    stTexto += "</div>";
    
    return stTexto;
    
}
function GetCookie(name) {
    var arg=name+"=";
    var alen=arg.length;
    var clen=document.cookie.length;
    var i=0;

    while (i<clen) {
        var j=i+alen;

        if (document.cookie.substring(i,j)==arg)
            return "1";
        i=document.cookie.indexOf(" ",i)+1;
        if (i==0)
            break;
    }

    return null;
}

function aceptar_cookies(){
    var expire=new Date();
    expire=new Date(expire.getTime()+7776000000);
    document.cookie="cookies_surestao=aceptada; expires="+expire;

    var visit=GetCookie("cookies_surestao");

    if (visit==1){
        popbox3();
    }
}

$(function() {
    var visit=GetCookie("cookies_surestao");
    if (visit==1){ popbox3(); }
});

function popbox3() {
    $('#overbox3').css('display', 'none');
}