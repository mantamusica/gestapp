<?php
header('Content-Type: text/html; charset=UTF-8');
session_start();
include 'bookstores/xajax/xajax_core/xajax.inc.php';

$xajax = new xajax('FunctionsXajax.php');

$xajax->register(XAJAX_FUNCTION, "validationLogIn");
$xajax->register(XAJAX_FUNCTION, "validationEmail");
$xajax->register(XAJAX_FUNCTION, "sendEmail");
$xajax->register(XAJAX_FUNCTION, "compareKey");
$xajax->register(XAJAX_FUNCTION, "passwordEncoded");
$xajax->register(XAJAX_FUNCTION, "generateSession");
$xajax->register(XAJAX_FUNCTION, 'chainRandom');
$xajax->register(XAJAX_FUNCTION, 'saveIn');
$xajax->register(XAJAX_FUNCTION, 'sendMail');


$xajax->configure('javascript URI', './bookstores/xajax/');

//Debug xajax
//$xajax->configure('debug', true);

if (isset($_SESSION["login"])) {
    header("Location: index.php");
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Noto+Sans:400,300,600,700,800'>
        <link rel="stylesheet" href="css/reset.css">
        <link rel="stylesheet" href="css/login.css">
        <link rel="stylesheet" href="css/modal.css">
        <script src="bookstores/jquery/jquery-3.3.1.js" type="text/javascript"></script>
        <script src="functions/js/scripts.js" type="text/javascript"></script>
        <?php $xajax->printJavascript(); ?>
        <title>Login GestApp</title>
        <script>
            $(document).ready(function(){
                $('#open').click(function(){
                    $('#popup').fadeIn('slow');
                    $('.popup-overlay').fadeIn('slow');
                    $('.popup-overlay').height($(window).height());
                    return false;
                });
                $('#btn_cerrar').click(function(){
                    $('#popup').fadeOut('slow');
                    $('.popup-overlay').fadeOut('slow');
                    return false;
                });
            });
            $(document).ready(function(){
                 $("#forgetPassword").click(function(){
                    $(".modal").slideDown("slow"); 
                    $(".modal").css("display", "block");
                 });
             });
            $(document ).ready(function() { $(".modal").css("display", "none"); });
            $(document).ready(function(){
                 $(".close").click(function(){
                    $(".modal").slideUp("slow");
                    $(".modal").css("display", "none");
                 });
             });
        </script>
    </head>
    <body>
    <?php include 'modules/emergent/newPassword.php'; ?>
        <div class="login-wrap">
            <form name="formulario" id="formulario">
                <div id="error" class="error"></div>
                <div id="confirm" class="confirm"></div>
            <div class="login-html">
                <input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab"><?php echo _("Login");?></label>
                <input id="tab-2" type="radio" name="tab" class="sign-up"><label for="tab-2" class="tab"><?php echo _("Registro");?></label>
                <div class="login-form">
                    <div class="sign-in-htm">
                        <div class="group">
                            <label for="mail" class="label"><?php echo _("Mail");?></label>
                            <input id="mail" name="mail" type="mail" class="input" autofocus autocomplete="off" placeholder="Escriba aquí su Email.">
                        </div>
                        <div class="group">
                            <label for="password" class="label"><?php echo _("Password");?></label>
                            <input id="password" name="password" type="password" class="input" data-type="password" autocomplete="off" placeholder="Escriba aquí su Password.">
                        </div>
                        <div class="hr"></div>
                        <div class="group">
                            <a href="#" onclick="loguear()"><input id="btn_login" name="loguear" type="button" class="button" value="Loguear"></a>
                        </div>
                        <div class="hr"></div>
                        <div class="foot-lnk">
                            <a href="#" id="forgetPassword"><?php echo _("¿Olvidaste tu contraseña?");?></a>
                        </div>
                    </div>
                    <div class="sign-up-htm">
                        <div class="group">
                            <label for="nombre_form" class="label"><?php echo _("Nombre");?></label>
                            <input id="nombre_form" name="nombre_form" ype="text" class="input" autofocus placeholder="Escriba aquí su Nombre.">
                        </div>
                        <div class="group">
                            <label for="apellidos_form" class="label"><?php echo _("Apellidos");?></label>
                            <input id="apellidos_form" name="apellidos_form" type="text" class="input" placeholder="Escriba aquí sus Apellidos.">
                        </div>
                        <div class="group">
                            <label for="email" class="label"><?php echo _("Email");?></label>
                            <input id="email" name="email" type="email" class="input" onchange="validateEmail();" placeholder="Escriba aquí su Email.">
                        </div>
                        <div class="group">
                            <label for="password1" class="label"><?php echo _("Password");?></label>
                            <input id="password1" name="password1" type="password" class="input" data-type="password" placeholder="Escriba aquí su Password.">
                        </div>
                        <div class="group">
                            <label for="password2" class="label"><?php echo _("Repite Password");?></label>
                            <input id="password2" name="password2" type="password" class="input" data-type="password" onblur="repeatPassword();" placeholder="Confirme aquí su Password.">
                        </div>
                        <div class="group">
                            <label for="telefono" class="label"><?php echo _("Teléfono");?></label>
                            <input id="telefono" name="telefono" type="text" class="input"  onblur="validatePhone();" placeholder="Escriba aquí su Teléfono.">
                        </div>
                        <div class="hr"></div>
                        <div class="group">
                            <a href="#" onclick="validateRegistry()"><input id="btn_registrar" name="registrar" type="button" class="button" value="<?php echo _("Registrar");?>" data-loading-text="<?php echo _("Cargando...")?>"></a>
                         </div>
                        <div class="hr"></div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </body>
</html>
