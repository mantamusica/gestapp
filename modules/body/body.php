<!--     Cuerpo Inicio  -->
<div class="container">
    <div class="container_menu">
        <div id="menu2">
            <header>
                <h3><?php echo _("Panel de Control");?></h3>
            </header>
            <ul>
                <a href="#" onclick="getInformation('user',1);"><li class="clearfix">
                    <span class="entypo-user"></span><?php echo _("Usuario");?><span class="simbolo">-></span>
                </li></a>
                <a href="#" onclick="getInformation('project',1);"><li class="clearfix">
                    <span class="entypo-bookmark"></span><?php echo _("Proyectos");?><span class="simbolo">-></span>
                </li></a>
                <a href="#" onclick="getInformation('chore',1);"><li class="clearfix">
                     <span class="entypo-bookmarks"></span><?php echo _("Tareas");?><span class="simbolo">-></span>
                </li></a>
                <a href="#" onclick="getCalendar()"><li class="clearfix">
                    <span class="entypo-calendar"></span><?php echo _("Calendario");?><span class="simbolo">-></span>
                </li></a>
            </ul>
            <footer>
                <p><?php echo _("GestApp");?></p>
            </footer>
        </div>
    </div>
    <div class="container_cuerpo">
        <div id="results">
            <br>
                <div id="col_left">
                    <h2>Proyectos Abiertos</h2>
                    <?php
                        $link = $GLOBALS['link'];
                        $sql_project = $link->prepare("SELECT * FROM proyecto p WHERE p.usuario_id=:id AND p.activo = 1 LIMIT 25");
                        $sql_project->execute(array(':id'=>$id));
                        ?>
                    <br>
                    <ul style="list-style-type: none;">
                        <?php
                        while($data_project = $sql_project->fetch())
                        {
                        ?><li style="padding: 10px;"> - <?= $data_project['nombre'].' - '.$data_project['descripcion']?></li><?php
                        }
                    ?>
                    </ul>
                </div>
                <div id="col_right">
                    <h2>Tareas Abiertas</h2>
                    <?php
                    $link = $GLOBALS['link'];
                    $sql_chore = $link->prepare("SELECT t.* FROM tarea t INNER JOIN proyecto p ON p.id_proyecto = t.proyecto_id WHERE p.usuario_id= :id AND t.activo = 1 LIMIT 25");
                    $sql_chore->execute(array(':id'=>$id));
                    ?>
                    <br>
                    <ul style="list-style-type: none;">
                        <?php
                        while($data_chore = $sql_chore->fetch())
                        {
                            ?><li style="padding: 10px;"> - <?= $data_chore['nombre'].' - '.$data_chore['descripcion']?></li><?php
                        }
                        ?>
                    </ul>
                </div>
        </div>
    </div>
</div>
