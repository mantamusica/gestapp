<div id="overbox3">
    <div id="infobox3">
        <p>Al usar este sitio acepta el uso de cookies para el análisis, contenido personalizado y publicidad.
            Este sitio web utiliza cookies propias y de terceros para optimizar tu navegación, adaptarse a tus preferencias y realizar labores analíticas.
            <a href="#" onclick="showInformation(1);">Más información</a>
            <a href="#" onclick="aceptar_cookies();" style="cursor:pointer;"><img style="width:60px;" src="img/close-icon.png"</a></p>
    </div>
</div>
<div class="footer-distributed">
        <div class="footer-center">
            <p class="footer-links"><?php echo _("GestApp - &copy; 2018 JMCC All Rights Reserved - Proyecto CFGS 2018");?></p>
            <p class="enlace">
                <a href="#" id="showCookies" onclick="showInformation(1);"><?php echo _("Política de Cookies");?></a>
                    ·
                <a href="#" id="showPrivacidad" onclick="showInformation(2);"><?php echo _("Declaración de Privacidad");?></a>
                ·
                <a href="#" id="showLegal" onclick="showInformation(3);"><?php echo _("Aviso Legal");?></a>
            </p>
        </div>
</div>