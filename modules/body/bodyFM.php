<?php
/**
 * Created by PhpStorm.
 * User: Viavox
 * Date: 18/04/2018
 * Time: 17:46
 */
?>
<!--     Cuerpo Inicio  -->
<div class="container">
    <div class="container_menu">
        <div id="menu2">
            <header>
                <h3><?php echo _("Panel de Control");?></h3>
            </header>
            <ul>
                <a href="#" onclick="getInformation('categories',1);"><li class="clearfix">
                    <span class="entypo-vcard"></span><?php echo _("Categorías");?><span class="simbolo">-></span>
                </li></a>
                <a href="#" onclick="getInformation('states',1);"><li class="clearfix">
                    <span class="entypo-shareable"></span><?php echo _("Estados");?></a><span class="simbolo">-></span>
                </li>
                <a href="#" onclick="getInformation('priorities',1);"><li class="clearfix">
                    <span class="entypo-shareable"></span><?php echo _("Prioridades");?><span class="simbolo">-></span>
                </li></a>
                <a href="#" onclick="getInformation('projects',1);"><li class="clearfix">
                    <span class="entypo-layout"></span><?php echo _("Proyectos");?><span class="simbolo">-></span>
                </li></a>
                <a href="#" onclick="getInformation('chores',1);"><li class="clearfix">
                    <span class="entypo-bookmarks"></span><?php echo _("Tareas");?><span class="simbolo">-></span>
                </li></a>
                <a href="#" onclick="getInformation('users',1);"><li class="clearfix">
                    <span class="entypo-users"></span><?php echo _("Usuarios");?><span class="simbolo">-></span>
                </li></a>
                <li class="clearfix">
                    <span class="entypo-compass"></span><?php echo _("Información");?>
                </li>
                <a href="#" onclick="getInformation('logs',1);"><li class="clearfix">
                    <span class="entypo-publish"></span><?php echo _("Logs");?><span class="simbolo">-></span>
                </li></a>
                <a href="#" onclick="getInformation('sessions',1);"><li class="clearfix">
                    <span class="entypo-newspaper"></span><?php echo _("Sesiones");?><span class="simbolo">-></span>
                </li></a>
                <a href="#" onclick="getInformation('errors',1);"><li class="clearfix">
                    <span class="entypo-block"></span><?php echo _("Errores");?><span class="simbolo">-></span>
                </li></a>
                <a href="#" onclick="getInformation('stadistics',1);"><li class="clearfix">
                    <span class="entypo-chart-line"></span><?php echo _("Estadísticas");?><span class="simbolo">-></span>
                </li></a>
            </ul>
            <footer>
                <p><?php echo _("GestApp");?></p>
            </footer>
        </div>
    </div>
    <div class="container_cuerpo">
        <div id="results">
                <div id="avisos_legales">
                    <h1 id="administracion">Página Administración de GestApp</h1>
                    <div id="imagen" style="width: 100%"><img src="img/fondo_logo2.png" style="width: 100%; height: auto;"></div>
                </div>
        </div>
    </div>
</div>