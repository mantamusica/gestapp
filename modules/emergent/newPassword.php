<div id="myModal" class="modal">
        <!-- Modal content -->
        <div class="modal-content">
            <span class="close">&times;</span>
            <br>
            <div class="login-form">
                <h2>Introduce tu Email:</h2>
                <div class="group">
                    <label for="form_email" class="label"><?php echo _("Email");?></label>
                    <input id="form_email" name="form_email" type="email" class="input" autofocus>
                </div>
                <hr class="hr">
                <div class="group">
                    <a href="#" onclick="sendEmail()"><input id="btn_mail" name="btn_mail" type="button" class="button" value="<?php echo _("Enviar");?>"></a>
                    <br>
                    <p>En breve recibirás un email con su nueva contraseña.</p>
                    <p>No olvides que podrás cambiarla a tu gusto desde dentro de la aplicación.</p>
                </div>
            </div>
        </div>
</div>