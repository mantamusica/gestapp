<?php
/**
 * Created by PhpStorm.
 * User: Viavox
 * Date: 18/04/2018
 * Time: 17:44
 */
?>
<!--        Menú-->
<div id="menu">
    <div id="logo"><a href="index.php"><image src="img/logo.png"/></a></div>
    <nav class="nav" role='navigation'>
        <ul>
            <li><a href="index.php"><span class="entypo-home"></span> Home</a></li>
            <li><a href="#" onclick="showInformation(4)"><span class="entypo-help"></span></a></li>
            <li><a href="#" onclick="contact();"><span class="entypo-mail"></span> <?php echo _('Contacto'); ?></a></li>
            <li><a href="logout.php"><span class="entypo-users"> </span> <?php echo _('Logout'); ?></a></li>
        </ul>
    </nav>
</div>
