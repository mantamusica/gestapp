<?php
date_default_timezone_set('UTC');
include 'includes/include_base.php';
include 'functions/php/functions.php';
include 'bookstores/xajax/xajax_core/xajax.inc.php';

$xajax = new xajax();

$xajax->register(XAJAX_FUNCTION, "validationLogIn");
$xajax->register(XAJAX_FUNCTION, "validationEmail");
$xajax->register(XAJAX_FUNCTION, "sendEmail");
$xajax->register(XAJAX_FUNCTION, "compareKey");
$xajax->register(XAJAX_FUNCTION, "passwordEncoded");
$xajax->register(XAJAX_FUNCTION, "generateSession");
$xajax->register(XAJAX_FUNCTION, 'chainRandom');
$xajax->register(XAJAX_FUNCTION, 'saveIn');
$xajax->register(XAJAX_FUNCTION, 'sendMail');

$xajax->processRequest();


function saveIn($nombre, $apellidos, $email, $tema, $mensaje){

    $answer = new xajaxResponse();
    $link = $GLOBALS['link'];

    try
    {
        $link->beginTransaction();
        $insertFormulario = $link->prepare("INSERT INTO formulario (nombre, apellidos, email, tema, mensaje) VALUES (:nombre, :apellidos, :email, :tema, :mensaje)");
        $array_datos = array(':nombre' => $nombre, ':apellidos' => $apellidos, ':email' => $email, ':tema' => $tema, ':mensaje' => $mensaje);
//        var_dump(print_query($insertFormulario, $array_datos));
//        exit();
        $insertFormulario->execute($array_datos);
        $existe = $link->lastInsertId();
        if($existe)
        {
            $answer->script("alert('Su formulario ha sido enviado.')");
            sendMail($email,$array_datos);
        }
        $link->commit();
    }
    catch (PDOException $e)
    {
        $answer->script("error_sistema('.$e->getMessage().', '.$e->getFile().', '.$e->getLine().')");
        $answer->script("alert('Sigue sin funcionar.')");
        $link->rollBack();
    }
    return $answer;
}
function sendMail($email, $array_datos)
{
    $destinatario = 'gestappsl@gmail.com';

    require_once('bookstores/phpmailer/PHPMailerAutoload.php');

    $mail = new phpmailer();
    $mail->CharSet = "UTF-8";
    $mail->PluginDir = "";
    $mail->Mailer = 'smtp';
    $mail->SMTPAuth = true;
    $mail->Host = "smtp.gmail.com"; // SMTP a utilizar. Por ej. smtp.elserver.com
    $mail->Port = 587;
    $mail->Username = "gestappsl@gmail.com"; // Correo completo a utilizar
    $mail->Password = "xxxxxx"; // Contraseña


    $mail->From = 'gestappsl@gmail.com';
    $mail->FromName = 'xxxxxx';
    $mail->Timeout = 120;
    $mail->AddAddress($destinatario);
    $mail->AddReplyTo($email);

    $mail->Subject = 'Formulario Contacto';
    $mail->Body = '<html>
        <p>
          <h2>Formulario de Contacto GestApp</h2>
          <h3>Se ha registrado una nueva cuestión en nuestro formulario.</h3>
        </p>
        <h4>Datos personales</h4>
        <p><b>Nombre:</b> '.$array_datos[':nombre'] .'</p>
        <p><b>Apellidos:</b> '.$array_datos[':apellidos'] .'</p>
        <p><b>Email:</b> '.$array_datos[':email'] .'</p>
        <p><b>Tema:</b> '.$array_datos[':tema'] .'</p>
        <p><b>Mensaje:</b> '.$array_datos[':mensaje'] .'</p>
        <br>
      </div>
    <div>
  </html>';
    $mail->AltBody = 'Hola';

    if( $mail->Send() ) return 'ok';
    else return( $mail->ErrorInfo );
}
function validationLogIn($email, $password)
{
    $answer = new xajaxResponse();
    $link = $GLOBALS['link'];
    $answer->assign('error', 'style.display', 'hide');
    $answer->assign('error', 'innerHtml', '');
        try
        {
            $sql_user = $link->prepare("SELECT u.*, r.siglas, r.nombre as rol FROM usuario u INNER JOIN rol r ON r.id_rol=u.rol_id WHERE u.email=:email");
            $update_user_access = $link->prepare("UPDATE usuario SET ultimo_acceso='" . date("Y-m-d") . "', contador_accesos=contador_accesos+1 WHERE id_usuario=:id");
            $insert_user_log = $link->prepare("INSERT INTO log (fecha, hora, usuario_id) VALUES (:fecha, :hora, :id)");
            $insert_user_session = $link->prepare("INSERT INTO sesion (fecha, usuario_id) VALUES (:fecha, :id)");
            $update_failed_counter_reset = $link->prepare("UPDATE usuario SET contador_accesos_fallidos=0 WHERE id_usuario=:id");
            $update_user_blocked = $link->prepare("UPDATE usuario SET bloqueado=1 WHERE id_usuario=:id");
            $update_user_counter_failed = $link->prepare("UPDATE usuario SET contador_accesos_fallidos=contador_accesos_fallidos+1 WHERE id_usuario=:id");
            $link->beginTransaction();
            $sql_user->execute(array(':email' => $email));
            //echo print_query($sql_user);

            if ($data_user = $sql_user->fetch()) {
                if(compareKey($password, $data_user['password']) == true){
                    if($data_user['bloqueado'] == 1)
                    {
                        $message = "Tu usuario está bloqueado, ponte en contacto con el administrador para proceder a la recuperación del servicio enviando un correo a <a href='mailto:admin@Gestapp.com'><img class='admin' src='img/Admin.png'></a>";
                        $answer->setReturnValue($message);
                        $answer->assign('error', 'style.display', 'block');
                        $answer->assign('error', 'innerHTML', $message);
                        $answer->script('disappear()');
                    }
                    elseif ($data_user['bloqueado'] == 0)
                    {
                        session_unset();
                        generateSession($data_user);
                        $update_user_access->execute(array(':id' => $data_user['id_usuario']));
                        $insert_user_log->execute(array(':fecha' => date("Y-m-d"), ':hora' => date("H:i:s"), ':id' => $data_user['id_usuario']));
                        $insert_user_session->execute(array(':fecha' => date("Y-m-d H:i:s"), ':id' => $data_user['id_usuario']));
                        $update_failed_counter_reset->execute(array(':id' => $data_user['id_usuario']));
                        $answer->script('setTimeout(function() {
                                window.location.href = "http://localhost/GestApp/index.php";
                                }, 3000);');
                    }
                    else
                    {
                        $message = "Hay algún error en los datos, reviselos y vuelva a intentarlo.";
                        $answer->setReturnValue($message);
                        $answer->assign('error', 'style.display', 'block');
                        $answer->assign('error', 'innerHTML', $message);
                        $answer->script('disappear()');
                    }
                }
                else
                {
                    $update_user_counter_failed->execute(array(':id' => $data_user['id_usuario']));
                    if($data_user['contador_accesos_fallidos'] >= 4){
                        $update_user_blocked->execute(array(':id' => $data_user['id_usuario']));
                        $message = "Tu usuario ha sido bloqueado, ponte en contacto con el administrador para proceder a la recuperación del servicio enviando un correo a <a href='mailto:admin@Gestapp.com'><img class='admin' src='img/Admin.png'></a>";
                        $answer->setReturnValue($message);
                        $answer->assign('error', 'style.display', 'block');
                        $answer->assign('error', 'innerHTML', $message);
                        $answer->script('disappear()');
                    }else{
                        $message = "Introduzca el password correctamente.";
                        $answer->setReturnValue($message);
                        $answer->assign('error', 'style.display', 'block');
                        $answer->assign('error', 'innerHTML', $message);
                        $answer->script('disappear()');
                    }
                }
            }
            else
            {
                $message = "No existe este usuario, compruebe que haya metido bien sus datos";
                $answer->setReturnValue($message);
                $answer->assign('error', 'style.display', 'block');
                $answer->assign('error', 'innerHTML', $message);
                $answer->assign('mail', 'style.border', 'thin solid red');
                $answer->script('disappear()');
            }
            $link->commit();
        }
        catch (PDOException $e){
            $link->rollBack();
            $message = _("Se ha producido un error al tratar de leer la información en la base de datos.") . ': ';
            $answer->setReturnValue($message);
            $answer->assign('error', 'style.display', 'block');
            $answer->assign('error', 'innerHTML', $message);
            $answer->script('disappear()');
        }
        return $answer;
    }
function validationEmail($nombre, $apellidos, $email, $password, $telefono){
    $answer = new xajaxResponse();
    $link = $GLOBALS['link'];
    $aux_password = passwordEncoded($password);
    $fechaActual = date("Y-m-d H:i:s");
    $answer->assign('error', 'style.display', 'hide');
    $answer->assign('error', 'innerHtml', '');
    try {
        $sql_email = $link->prepare("SELECT u.* FROM usuario u  WHERE u.email=:email");
        $insert_registro = $link->prepare("INSERT INTO usuario (nombre, apellidos, email, password, telefono, fecha_registro, activo ) VALUES (:nombre, :apellidos, :email, :password, :telefono, :fecha_registro, :activo)");
        $link->beginTransaction();
        $sql_email->execute(array(':email' => $email));
        if ($data_email = $sql_email->fetch()) {
            $message = "El email que ha introducido ya existe en nuestra base de datos.";
            $answer->setReturnValue($message);
            $answer->assign('error', 'style.display', 'block');
            $answer->assign('error', 'innerHTML', $message);
            $answer->assign('email', 'style.border', 'thin solid red');
            $answer->script('disappear()');
        }else{
            $message = "Usted ya está registrado en nuestra aplicación, cuando quiera puede loguearse.";
            $answer->setReturnValue($message);
            $answer->assign('confirm', 'style.display', 'block');
            $answer->assign('confirm', 'innerHTML', $message);
            $answer->script('disappearConfirm()');
            $answer->script('self.setInterval("location.reload(true);",10000);');
            $insert_registro->execute(array(':nombre' => $nombre, ':apellidos' => $apellidos, ':email' => $email, ':password' => $aux_password, ':telefono' => $telefono, ':fecha_registro' => $fechaActual, ':activo' => 1));
        }
        $link->commit();
    }catch (PDOException $e){
        $link->rollBack();
        $message = _("Se ha producido un error al tratar de leer la información en la base de datos.") . ': ';
        $message .=  $e->getMessage();
        $answer->setReturnValue($message);
        $answer->assign('error', 'style.display', 'block');
        $answer->assign('error', 'innerHTML', $message);
    }
    return $answer;
}
function sendEmail($email, $newPassword){
    $answer = new xajaxResponse();
    $link = $GLOBALS['link'];
    try {
        $sql_email = $link->prepare("SELECT u.* FROM usuario u  WHERE u.email=:email");
        $update_password = $link->prepare("UPDATE usuario SET password=:password WHERE u.email=:email");
        $link->beginTransaction();
        $sql_email->execute(array(':email' => $email));
        if ($data_user = $sql_email->fetch())
        {
            if($data_user['bloqueado'] == 1)
            {
                $message = "Tu usuario está bloqueado, ponte en contacto con el administrador para proceder a la recuperación del servicio enviando un correo a <a href='mailto:admin@Gestapp.com'><img class='admin' src='img/Admin.png'></a>";
                $answer->setReturnValue($message);
                $answer->assign('error', 'style.display', 'block');
                $answer->assign('error', 'innerHTML', $message);
                $answer->script('disappear()');
            }
            elseif ($data_user['bloqueado'] == 0)
            {
                $newPasswordEncoded = passwordEncoded($newPassword);
                $update_password->execute(array(':email' => $email, ':password' => $newPasswordEncoded));
                renderMail($data_user, $newPassword);
                $message = _("Ya le hemos enviado su nueva contraseña, recuerde que dentro de la aplicación podrá cambiarla de nuevo.") . ': ';
                $answer->setReturnValue($message);
                $answer->assign('confirm', 'style.display', 'block');
                $answer->assign('confirm', 'innerHTML', $message);
                $answer->script('disappearConfirm()');
            }
            else
            {
                $message = "Hay algún error en los datos, reviselos y vuelva a intentarlo.";
                $answer->setReturnValue($message);
                $answer->assign('error', 'style.display', 'block');
                $answer->assign('error', 'innerHTML', $message);
                $answer->script('disappear()');
            }
        }
    }
    catch (PDOException $e){
        $link->rollBack();
        $message = _("Se ha producido un error al tratar de leer la información en la base de datos.") . ': ';
        $answer->setReturnValue($message);
        $answer->assign('error', 'style.display', 'block');
        $answer->assign('error', 'style.z-index', '3000');
        $answer->assign('error', 'innerHTML', $message);
        $answer->script('disappear()');
    }
    return $answer;

}
function passwordEncoded($aux_password){
    $salt = chainRandom(64);
    $hash = hash("sha256", $salt . $aux_password);
    $password = $salt . $hash;
    return $password;
}
function compareKey($passwordInserted, $passwordBD){
    $salt = substr($passwordBD, 0, 64);
    $validHash = substr($passwordBD, 64, 64);
    $testHash = hash("sha256", $salt.$passwordInserted);
    return ($validHash == $testHash) ? true : false;
}
function generateSession($array){
    $_SESSION['login']['id_usuario'] = $array['id_usuario'];
    $_SESSION['login']['nombre'] = $array['nombre'];
    $_SESSION['login']['apellidos'] = $array['apellidos'];
    $_SESSION['login']['email'] = $array['email'];
    $_SESSION['login']['rol'] = $array['rol'];
    $_SESSION['login']['siglas'] = $array['siglas'];
}
function chainRandom($length=10,$c=TRUE,$uc=TRUE,$n=TRUE,$sc=FALSE){
    $source = '';
    if($c==1) $source .= 'abcdefghijklmnopqrstuvwxyz';
    if($uc==1) $source .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    if($n==1) $source .= '1234567890';
    if($sc==1) $source .= '|@#~$%()=^*+[]{}-_';
    if($length>0){
        $rstr = "";
        $source = str_split($source,1);
        for($i=1; $i<=$length; $i++){
            mt_srand((double)microtime() * 1000000);
            $num = mt_rand(1,count($source));
            $rstr .= $source[$num-1];
        }
    }
    return $rstr;
}
function renderMail($dataUser, $newPassword){
    $nombre = $dataUser['nombre'].' '.$dataUser['apellidos'];
    $para  = $dataUser['email']; // atención a la coma
    $título = 'Nueva Contraseña de GestApp.';
    $mensaje = '
                <html>
                <head>
                  <title>Nueva Contraseña de GestApp para '.$nombre.'.</title>
                </head>
                <body>
                  <p>Muy buenas '.$nombre.'</p>
                  <p>Desde el equipo técnico de GestApp le remitimos una nueva contraseña como nos solicitó a su correo '.$para.' .</p>
                  <p>Esta es su nueva contraseña: '.$newPassword.'</p>
                  
                </body>
                </html>
                ';

    $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
    $cabeceras .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

    mail($para, $título, $mensaje, $cabeceras);
}
