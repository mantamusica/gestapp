-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 04-07-2018 a las 14:22:57
-- Versión del servidor: 5.7.19
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `1718d0_josem1`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

DROP TABLE IF EXISTS `administrador`;
CREATE TABLE IF NOT EXISTS `administrador` (
  `id` tinyint(100) NOT NULL AUTO_INCREMENT,
  `username` varchar(127) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(256) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(256) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` varchar(256) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(256) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`id`, `username`, `password`, `nombre`, `apellidos`, `email`) VALUES
(1, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'admin', 'admin', 'admin@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ambito`
--

DROP TABLE IF EXISTS `ambito`;
CREATE TABLE IF NOT EXISTS `ambito` (
  `id_ambito` int(11) NOT NULL AUTO_INCREMENT,
  `seleccionable` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_ambito`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla de Ambitos BBDD GestApp';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ambito_traduccion`
--

DROP TABLE IF EXISTS `ambito_traduccion`;
CREATE TABLE IF NOT EXISTS `ambito_traduccion` (
  `id_ambito_traduccion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(256) COLLATE utf8_spanish_ci NOT NULL,
  `ambito_id` int(11) NOT NULL,
  `idioma_id` int(11) NOT NULL,
  PRIMARY KEY (`id_ambito_traduccion`),
  KEY `ambito_id` (`ambito_id`),
  KEY `idioma_id` (`idioma_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla de Ambitos de Traducción BBDD GestApp';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

DROP TABLE IF EXISTS `categoria`;
CREATE TABLE IF NOT EXISTS `categoria` (
  `id_categoria` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(128) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_categoria`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla de Categorías BBDD GestApp';

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id_categoria`, `nombre`, `descripcion`, `activo`) VALUES
(1, 'familiar', 'datos familiares', 1),
(2, 'trabajo', 'tema relacionado con el trabajo', 1),
(3, 'personal', 'temas propios', 1),
(4, 'amistad', 'relacionado con algún amigo', 1),
(5, 'casa', 'tareas del hogar', 1),
(6, 'La tenemos de prueba', 'categoria de prueba', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `error`
--

DROP TABLE IF EXISTS `error`;
CREATE TABLE IF NOT EXISTS `error` (
  `id_error` int(11) NOT NULL AUTO_INCREMENT,
  `mensaje` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `pagina` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `linea` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_error`),
  KEY `FK_error_usuario` (`usuario_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `error`
--

INSERT INTO `error` (`id_error`, `mensaje`, `pagina`, `linea`, `fecha`, `usuario_id`) VALUES
(1, 'error base de datos', 'index', '456', '2018-06-13 17:29:38', 8),
(2, 'error conexión', 'index', '401', '2018-06-13 17:40:26', 3),
(3, 'error base de datos', 'index', '444', '2018-06-13 17:40:54', 5),
(4, 'error base de datos', 'login', '234', '2018-06-13 17:41:20', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

DROP TABLE IF EXISTS `estado`;
CREATE TABLE IF NOT EXISTS `estado` (
  `id_estado` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(128) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_estado`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla de Estados BBDD GestApp';

--
-- Volcado de datos para la tabla `estado`
--

INSERT INTO `estado` (`id_estado`, `nombre`, `descripcion`, `activo`) VALUES
(1, 'activa', 'en proceso de realización', 1),
(2, 'pendiente', 'tarea sin acabar', 1),
(3, 'retrasada', 'tarea retrasada para otro momento', 1),
(4, 'eliminada', 'eliminada en cualquier momento', 1),
(5, 'nueva', 'nueva', 0),
(6, 'estado nuevo', 'estado nuevo test', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `formulario`
--

DROP TABLE IF EXISTS `formulario`;
CREATE TABLE IF NOT EXISTS `formulario` (
  `id_formulario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `tema` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `mensaje` varchar(256) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_formulario`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `formulario`
--

INSERT INTO `formulario` (`id_formulario`, `nombre`, `apellidos`, `email`, `tema`, `mensaje`) VALUES
(1, 'dfh', 'dfhg', 'dfgh@sdf.com', 'improvements', 'sdg'),
(2, 'bn,m', 'bnm,', 'bnm@df.com', 'improvements', 'dffsdgfs'),
(3, 'test', 'testeando', 'test@sad.test', 'problems', 'afas fjalsjf ñfjañfdjgsjg m'),
(4, 'jose maria', 'cagigas cespedes', 'chemacagigas@gmail.com', 'improvements', 'mejorando el equipo'),
(5, 'Jose María', 'Cagigas Céspedes', 'chemacagigas@gmail.com', 'problems', 'Test de prueba');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `idioma`
--

DROP TABLE IF EXISTS `idioma`;
CREATE TABLE IF NOT EXISTS `idioma` (
  `id_idioma` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(256) COLLATE utf8_spanish_ci NOT NULL,
  `abreviatura` varchar(256) COLLATE utf8_spanish_ci NOT NULL,
  `bandera` varchar(256) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_idioma`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla de Idiomas BBDD GestApp';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `log`
--

DROP TABLE IF EXISTS `log`;
CREATE TABLE IF NOT EXISTS `log` (
  `id_log` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `usuario_id` int(11) NOT NULL,
  PRIMARY KEY (`id_log`),
  KEY `usuario_id` (`usuario_id`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla de Logs BBDD GestApp';

--
-- Volcado de datos para la tabla `log`
--

INSERT INTO `log` (`id_log`, `fecha`, `hora`, `usuario_id`) VALUES
(1, '2018-05-07', '19:44:15', 2),
(11, '2018-06-02', '17:04:03', 8),
(13, '2018-06-02', '17:07:07', 2),
(15, '2018-06-02', '17:10:07', 2),
(16, '2018-06-02', '17:10:35', 2),
(24, '2018-06-03', '12:04:16', 8),
(26, '2018-06-03', '10:11:57', 8),
(27, '2018-06-03', '10:12:37', 8),
(28, '2018-06-03', '10:12:55', 8),
(29, '2018-06-03', '10:13:46', 8),
(30, '2018-06-05', '16:55:42', 8),
(31, '2018-06-06', '15:04:59', 8),
(32, '2018-06-06', '15:16:20', 8),
(33, '2018-06-06', '15:19:14', 8),
(34, '2018-06-06', '15:20:02', 8),
(35, '2018-06-07', '16:43:13', 8),
(36, '2018-06-08', '13:08:41', 8),
(37, '2018-06-08', '17:06:39', 2),
(38, '2018-06-08', '17:07:55', 2),
(39, '2018-06-08', '17:12:03', 2),
(40, '2018-06-08', '17:15:19', 8),
(41, '2018-06-09', '06:32:42', 8),
(42, '2018-06-09', '06:37:19', 8),
(43, '2018-06-09', '06:38:05', 8),
(44, '2018-06-09', '13:44:05', 8),
(45, '2018-06-09', '14:24:28', 8),
(46, '2018-06-09', '14:29:32', 8),
(47, '2018-06-10', '05:53:01', 8),
(48, '2018-06-10', '13:59:21', 2),
(49, '2018-06-10', '14:01:57', 2),
(50, '2018-06-11', '19:07:52', 2),
(51, '2018-06-12', '04:58:08', 2),
(52, '2018-06-12', '12:41:52', 3),
(53, '2018-06-12', '12:46:12', 5),
(54, '2018-06-12', '14:24:50', 2),
(55, '2018-06-13', '09:52:29', 8),
(56, '2018-06-13', '09:53:54', 2),
(57, '2018-06-13', '12:24:43', 2),
(58, '2018-06-13', '13:57:54', 2),
(59, '2018-06-13', '14:08:52', 8),
(60, '2018-06-13', '16:05:09', 2),
(61, '2018-06-13', '16:14:04', 2),
(62, '2018-06-13', '16:17:16', 5),
(63, '2018-06-13', '16:18:55', 8),
(64, '2018-06-14', '17:48:08', 8),
(65, '2018-06-14', '17:52:50', 2),
(66, '2018-06-14', '17:59:46', 2),
(67, '2018-06-15', '13:15:55', 8),
(68, '2018-06-15', '13:33:30', 8),
(69, '2018-06-15', '13:36:28', 8),
(70, '2018-06-15', '14:04:03', 8),
(71, '2018-06-15', '14:24:38', 8),
(72, '2018-06-15', '14:35:58', 3),
(73, '2018-06-15', '15:58:01', 2),
(74, '2018-06-15', '16:11:59', 2),
(75, '2018-06-16', '07:55:45', 2),
(76, '2018-06-16', '09:44:33', 2),
(77, '2018-06-16', '09:57:23', 2),
(78, '2018-06-16', '10:01:23', 8),
(79, '2018-06-16', '10:48:12', 8),
(80, '2018-06-16', '11:01:05', 8),
(81, '2018-06-16', '13:52:46', 8),
(82, '2018-06-16', '14:49:36', 9),
(83, '2018-06-17', '08:55:13', 12),
(84, '2018-06-17', '09:03:43', 2),
(85, '2018-06-17', '09:23:43', 3),
(86, '2018-06-17', '09:45:25', 2),
(87, '2018-06-17', '09:55:44', 8),
(88, '2018-06-17', '10:22:33', 8),
(89, '2018-06-18', '14:28:35', 5),
(90, '2018-06-18', '14:28:59', 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nota`
--

DROP TABLE IF EXISTS `nota`;
CREATE TABLE IF NOT EXISTS `nota` (
  `id_nota` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(128) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `usuario_id` int(11) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_nota`),
  KEY `FK_nota_usuario` (`usuario_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

DROP TABLE IF EXISTS `noticias`;
CREATE TABLE IF NOT EXISTS `noticias` (
  `id` tinyint(100) NOT NULL AUTO_INCREMENT,
  `tema` varchar(127) COLLATE utf8_spanish_ci NOT NULL,
  `titulo` varchar(256) COLLATE utf8_spanish_ci NOT NULL,
  `noticia` mediumtext COLLATE utf8_spanish_ci NOT NULL,
  `fuente` varchar(128) COLLATE utf8_spanish_ci NOT NULL,
  `enlace` varchar(256) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` varchar(64) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`id`, `tema`, `titulo`, `noticia`, `fuente`, `enlace`, `fecha`) VALUES
(1, 'Hacienda', 'Lista de Morosos con Hacienda', 'La Agencia Tributaria ha publicado, en la Sede Electrónica de la A.E.A.T., el listado de deudores con el fisco. En comparación con la primera lista el número de deudores ha disminuido un 6,3% (306), y casi mil contribuyentes que aparecían en ese primer listado ya no están. En cuanto a la cuantía, se ha reducido un 1,2%. La salida de la lista se puede deber tanto a la cancelación total o parcial de las deudas objeto de publicación, como a la obtención de un aplazamiento o suspensión de la deuda antes de la fecha de toma de datos (cierre de 2016).\r\n', 'El Confidencial', 'https://www.elconfidencial.com/economia/2017-06-23/lista-morosos-hacienda-2017-deudores_1404276/', 'JUN 2017'),
(2, 'Autónomos', 'Las 15 claves de la nueva ley de autónomos: más ayudas y menos burocracia', 'La comisión de Empleo del Congreso de los Diputados ha dado este jueves luz verde a la Ley de Reformas Urgentes del Trabajo Autónomo, una iniciativa de Ciudadanos que finalmente sale adelante por unanimidad. Fue una de las primeras propuestas de ley que se presentaron en el Congreso en esta legislatura, pero su tramitación se ha prolongado por la negociación entre los partidos y con las asociaciones de autónomos. Finalmente, cuenta con el voto favorable del PP, PSOE, Podemos, PDeCAT, PNV y ERC. Estas son las 15 principales reformas que se introducen:\r\n\r\n- Se amplía la tarifa plana de 50 euros en la cotización a la Seguridad Social de seis meses a un año. Además, los autónomos que dejen su actividad durante dos años y vuelvan a trabajar podrán acogerse a la tarifa plana, ya que hasta ahora tenían que esperar cinco años.\r\n\r\n- Las mujeres tendrán una tarifa plana de 50 euros cuando se reincorporen tras la maternidad.\r\n\r\n- Se bonificará al 100% durante 12 meses la cuota de los autónomos que tengan que cuidar de menores o dependientes.\r\n\r\n- Se permitirá que los autónomos puedan deducirse el 20% de los gastos en suministros de agua, gas, electricidad y telefonía cuando trabajen desde su casa.\r\n\r\n- Los autónomos podrán deducirse hasta 26,67 euros diarios por gastos en dietas y manutención (48,08 euros si es en el extranjero), siempre que el pago esté hecho por medios telemáticos para que pueda quedar acreditado.\r\nJ. G. Jorrín\r\nTags\r\n\r\n    AutónomosEmpleoMinisterio de EmpleoSeguridad SocialCiudadanosPresupuestos Generales del EstadoCongreso de los Diputados\r\n\r\nTiempo de lectura3 min\r\n29.06.2017 – 17:28 H.\r\n\r\nLa comisión de Empleo del Congreso de los Diputados ha dado este jueves luz verde a la Ley de Reformas Urgentes del Trabajo Autónomo, una iniciativa de Ciudadanos que finalmente sale adelante por unanimidad. Fue una de las primeras propuestas de ley que se presentaron en el Congreso en esta legislatura, pero su tramitación se ha prolongado por la negociación entre los partidos y con las asociaciones de autónomos. Finalmente, cuenta con el voto favorable del PP, PSOE, Podemos, PDeCAT, PNV y ERC. Estas son las 15 principales reformas que se introducen:\r\n\r\n- Se amplía la tarifa plana de 50 euros en la cotización a la Seguridad Social de seis meses a un año. Además, los autónomos que dejen su actividad durante dos años y vuelvan a trabajar podrán acogerse a la tarifa plana, ya que hasta ahora tenían que esperar cinco años.\r\n\r\n- Las mujeres tendrán una tarifa plana de 50 euros cuando se reincorporen tras la maternidad.\r\n\r\n- Se bonificará al 100% durante 12 meses la cuota de los autónomos que tengan que cuidar de menores o dependientes.\r\n\r\n- Se permitirá que los autónomos puedan deducirse el 20% de los gastos en suministros de agua, gas, electricidad y telefonía cuando trabajen desde su casa.\r\n\r\n- Los autónomos podrán deducirse hasta 26,67 euros diarios por gastos en dietas y manutención (48,08 euros si es en el extranjero), siempre que el pago esté hecho por medios telemáticos para que pueda quedar acreditado.\r\nHasta cuándo puede la Seguridad Social costear la tarifa plana de los autónomos\r\nJavier Jorrín\r\nPP y Ciudadanos pactan prorrogar un año la ayuda a los nuevos afiliados que se seguirá financiando con cotizaciones en lugar de los Presupuestos como propuso Empleo\r\n\r\n- Los autónomos podrán cambiar de base de cotización hasta en cuatro ocasiones a lo largo del año, de modo que quede mejor adecuada a sus ingresos.\r\n\r\n- Se permite que los autónomos se den de alta y de baja en la Seguridad Social hasta tres veces al año, para así pagar solo en las épocas en las que tengan actividad.\r\n\r\n- Se podrá compatibilizar el cobro del 100% de la pensión con la prolongación de la vida laboral para el empleo por cuenta propia.\r\n\r\n- La cuota de los autónomos societarios se desvincula de las subidas del salario mínimo interprofesional (SMI) y quedará determinada año a año en los Presupuestos Generales del Estado tras dialogarlo con las organizaciones de autónomos.\r\n\r\n- Se reducen los recargos que penalizaban a los autónomos por el retraso en el pago de la cuota: durante el primer mes de desfase en el pago, la penalización pasa del 20% al 10%.\r\n\r\n- La Seguridad Social devolverá de oficio el exceso de cotización de las personas en situación del pluriactividad sin que esta tenga que realizar la solicitud de la devolución.\r\n\r\n- Se incluye el accidente &#39;in itinere&#39;, esto es, durante el desplazamiento de los profesionales autónomos a su puesto de trabajo.\r\n\r\n- Se aprueban mejoras para la formación en prevención de riesgos laborales.\r\n\r\n- Se mejoran las condiciones del emprendimiento para los trabajadores con discapacidad.\r\n\r\n- Se permite que los autónomos contraten a sus hijos que estén en situación de discapacidad.\r\n', 'El Confidencial', 'https://www.elconfidencial.com/economia/2017-06-29/reforma-ley-autonomos-novedades-cotizacion-seguridad-social_1407440/', 'JUN 2017'),
(3, 'Empresas', 'Se intensifica la fuga de empresas de Cataluña', 'Cerca de una treintena de compañías se han ido de Cataluña en apenas una semana, desde el referéndum ilegal de 1-O. Ayer, Colonial, Abertis y Cellnex, entre otras, se sumaron al grupo de empresas que han abandonado la región ante una posible declaración unilaterial de independencia hoy en el Parlament.\r\n\r\nLa huida de empresas de Cataluña va &#39;in crescendo&#39; ante el clima de inestabilidad que genera una posible declaración unilateral de independencia (DUI) que podría producirse esta misma tarde.\r\n\r\nLa resaca del referéndum ilegal en Cataluña continuó con una jornada de huelga general para tratar de reforzar los planes de secesión del gobierno catalán de Carles Puigdemont. Pero el martes, justo al día siguiente, comenzó el goteo, ahora imparable, de empresas que anuncian su salida de esta comunidad por miedo a un desenlace que afecte de lleno a sus finanzas.\r\n\r\nA partir del viernes esa tendencia se aceleró después de que el Gobierno aprobara un decreto ley para facilitar que las empresas que quieran trasladar su domicilio social lo hagan sin que la decisión requiera la aprobación de la junta de accionistas de la compañía.\r\n\r\nEl cambio de domicilio social se realiza para proteger a la empresa de cualquier legalidad paralela o intentos de control que intentara imponer Generalitat en caso de DUI.\r\nEl cambio de domicilio o sede social se realiza para proteger a la empresa de cualquier legalidad paralela o intentos de control que intentara imponer Generalitat en caso de DUI.\r\n\r\nSin embargo, el argumento que esgrime la mayor parte de las compañías es que se trata de un movimiento estratégico para proteger los intereses de sus accionistas, sus clientes y sus empleados. Muchas de las empresas tratan de vestir su decisión aludiendo a que el recrudecimiento del conflicto catalán no ha tenido un impacto directo en su decisión, pese a que ambos factores han coincidido en el tiempo.', 'Expansión', 'http://www.expansion.com/sociedad/2017/10/10/59dc72ba268e3e4e608b48e0.html', 'OCT 2017'),
(4, 'Vivienda', 'La compraventa de viviendas sube un 16% en agosto y encadena cuatro meses de alzas', 'La compraventa de viviendas subió en agosto el 16% y encadena cuatro meses seguidos al alza tras el bajón de abril por el efecto &#34;Semana Santa&#34;.\r\n\r\nLa compraventa de viviendas subió un 16% el pasado mes de agosto en relación al mismo mes de 2016, hasta sumar 41.282 operaciones, según se extrae hoy de la Estadística de Transmisiones y de Derechos de la Propiedad hecha pública hoy por el Instituto Nacional de Estadística (INE).\r\n\r\nCon el avance interanual de agosto, la compraventa de viviendas suma cuatro meses consecutivos de alzas. Aunque el repunte de agosto ha sido ligeramente inferior al de julio (16,8%), el número de compraventas de viviendas (41.282) ha marcado su tercer mejor registro desde febrero de 2011, sólo superado por el de mayo y junio de este año, cuando se hicieron más de 44.000 operaciones.\r\n\r\nEn términos mensuales (agosto sobre julio), la compraventa de viviendas se incrementó un 6,3%, su segundo mayor repunte en este mes en los últimos cinco años.\r\n\r\nEntre enero y agosto, la compraventa de casas acumuló un incremento del 14%, que reflejan que frente a julio el avance fue del 6%.\r\n\r\nOcho de cada diez son viviendas de segunda mano\r\nSólo en el octavo mes del año, del total de transacciones de viviendas inscritas en los registros de la propiedad, el 82,1% correspondió a casas usadas, con 33.886 operaciones y un avance interanual del 14,9% hasta totalizar 33.886, mientras que la compraventa de viviendas nuevas avanzó un 21,3% en tasa interanual, hasta 7.396 transacciones.\r\n\r\nEl 90,6% de las viviendas transmitidas fueron viviendas libres y el 9,4%, protegidas. La compraventa de viviendas libres se incrementó un 16,6% en agosto en tasa interanual, hasta sumar 37.412 transacciones, en tanto que las operaciones sobre viviendas protegidas subieron un 10,2%, con 3.870 transacciones.\r\n\r\nEn términos mensuales (agosto sobre julio), la compraventa de viviendas se incrementó un 6,3%, su segundo mayor repunte en este mes en los últimos cinco años.\r\n\r\nComunidad Valenciana, Baleares y Andalucía lideran los ascensos\r\nEl pasado mes de agosto el mayor número de compraventas de viviendas por cada 100.000 habitantes se dio en Comunidad Valenciana (163), Baleares (152) y Andalucía (125).\r\n\r\nEn cuanto a volumne, como es habitual Andalucía fue la región que más operaciones sobre viviendas realizó en el octavo mes del año, con 8.224 compraventas, seguida de Cataluña (6.720) y la Comunidad Valenciana (6.370).\r\n\r\nLas comunidades que realizaron un menor número de compraventas de viviendas fueron La Rioja (262), Navarra (477) y Cantabria (507).\r\n\r\nTodas las regiones registraron en agosto crecimientos interanuales en la compraventa de viviendas. Las comunidades que presentaron las mayores subidas fueron Castilla-La Mancha (+37,9%), Comunidad Valenciana (+32,5%) y Galicia (+23%), en tanto que Navarra (+1,9%) y País Vasco (+4%) registraron las tasas más bajas.\r\n\r\nFincas transmitidas\r\nSumando las fincas rústicas y las urbanas (viviendas y otros inmuebles de naturaleza urbana), las fincas transmitidas el pasado mes de agosto alcanzaron las 142.976, cifra un 5,9% superior a la del mismo mes de 2016.\r\n\r\nPor compraventa se transmitieron un 14,3% más de fincas que en agosto de 2016, mientras las transmisiones por donación cayeron un 14%, las operaciones por permuta subieron un 20,1% y las transmitidas por herencia decrecieron un 2,6%.\r\n\r\nSegún los datos del INE, el número de compraventas de fincas rústicas aumentó un 8,1% en agosto pasado, hasta un total de 9.703 operaciones, mientras que las compraventas de fincas urbanas se incrementaron un 15,2%, hasta 69.671 operaciones.\r\n\r\nEn agosto, el mayor número de compraventas de fincas transmitidas por cada 100.000 habitantes se dio en La Rioja (644), Castilla y León (553) y Aragón (542).', 'Expansión', 'http://www.expansion.com/economia/2017/10/10/59dc7528468aeb9f478b469d.html', 'OCT 2017'),
(5, 'Sociedades Civiles', 'Cambio radical en la tributación de las Sociedad Civiles', 'A aquellos que constituyeron una sociedad civilvan dirigidos los siguientes comentarios, a la vez que ponemos nuestro despacho profesional a su disposición para que aclaren las dudas que les puedan surgir.\r\n\r\nY es que, uno de los cambios de la reciente reforma del Impuesto de Sociedades que más importancia va a tener en las pequeñas empresas y micropymes es la modificación del Art. 7 de la  Ley 27/2014, en la que dice que:\r\n\r\n “Artículo 7. Contribuyentes.1. Serán contribuyentes del Impuesto, cuando tengan su residencia en territorio español: a) Las personas jurídicas, excluidas las sociedades civiles que no tengan objeto mercantil”.\r\n\r\nEste artículo aplicable con efectos 01/01/2016, conlleva múltiples consecuencias, analicémoslas a continuación de forma breve pero precisa:\r\n\r\nAl excluir las sociedades civiles de objeto mercantil, ya no afectará a las sociedades profesionales, ni agrícolas o ganaderas, ni otras de carácter no mercantil, pero sí al resto.\r\nPor supuesto, aquellas sociedades civiles que estén en estimación objetiva (MODULOS) no pueden continuar así.\r\nSegún diversos pronunciamientos de la Dirección General de Tributos tampoco afectará las comunidades de bienes aunque tengan objeto mercantil, cuestión está que parece que ha traído mucha controversia, pero de momento así ha quedado. De hecho, parece que en algunas administraciones de la AEAT ya no admiten alta censal de comunidades de bienes con la “singularidad” de que tengan objeto mercantil. Más descabellado parece tratar de “transformar” sociedades civiles en comunidades de bienes para eludir las nuevas obligaciones.\r\nEn cuanto al IVA, según el Art. 148 de la LIVA, en el momento que las sociedades civiles dejen de estar en el régimen de IRPF anterior (Atribución de Rentas) dejarán de aplicar el régimen especial de Recargo de Equivalencia, con las consecuencias que ello acarrea.\r\nEl hecho de que tributen por el Impuesto de Sociedades obliga a las SCP a llevar contabilidad conforme al Código de Comercio y obviamente a confeccionar el Impuesto de Sociedades, con la complejidad y el costo que eso supone para las empresas de menor dimensión.\r\nCambia la forma fiscal en que los socios de las sociedades civiles van a percibir sus remuneraciones, de la simple atribución de los beneficios o pérdidas de la sociedad a pasar a tributar por sus rendimientos de trabajo (nómina) o por actividades económicas (factura de socio a la sociedad), según el caso.\r\nAnte tanta complejidad y la equiparación de obligaciones de las SCP con el resto de las sociedades mercantiles, no son pocas las voces que recomiendan el cambio a la forma jurídica de sociedad limitada, ya que al menos así eludimos la responsabilidad ilimitada de nuestro patrimonio personal. Igualmente parece que se extingue la alternativa para los nuevos emprendedores a la hora de iniciar una actividad de la forma más simple y menos costosa.\r\n\r\nConsecuentemente, y como vemos que son muchos los cambios que vienen, estamos a tiempo durante los primeros meses de 2016 de analizar la situación y enfrentarnos a este pequeño reto.\r\n\r\nEn este sentido, insistimos, ponemos nuestro despacho a disposición de aquellas sociedades civiles que tengan dudas para resolver su situación, en una primera consulta gratuita y sin compromiso, si el domicilio de la sociedad civil, comunidad de bienes o emprendedor está en SANTANDER O CANTABRIA.', 'GDN', 'http://www.gdnasesoria.com/index.php/blog/74-las-sociedades-civiles-aclarando-el-panorama-tributario', 'DIC 2017'),
(6, 'Autónomos', 'Hacienda deja sin cambios la tributación por módulos para autónomos', 'El último Consejo de Ministros del año traerá buenas noticias para los autónomos. Aunque la legislación en vigor establecía que a partir del 1 de enero de 2018 cambiarían las reglas de juego en el sistema de tributación por módulos del que se benefician numerosos trabajadores por cuenta propia, el Ministerio de Hacienda ha decidido finalmente dar marcha atrás y no endurecer la norma, tal y como adelantó CincoDías.\r\n\r\n\r\nAsí, en un principio estaba previsto que el límite de facturación a partir del cual se impide tributar por módulos descendiera desde los 250.000 euros actuales a 150.000. Y en el caso de que el trabajador por cuenta propia facture a otras empresas, ese tope se moderaba de los 125.000 euros a 75.000. Fuentes del Gobierno confirmaron ayer que la intención es mantener inalterados esos límites, en principio, un año más.\r\n\r\nDe esta manera, se evita que más de 500.000 autónomos se vean obligados a abandonar este sistema de tributación, tal y como había advertido Lorenzo Amor, presidente de la Asociación de Trabajadores Autónomos (ATA). En este sentido, en el caso de que se hubiese decidido bajar los límites de facturación a las empresas de los citados 125.000 euros a solo 75.000, Amor consideró recientemente que eso hubiera obligado a muchos trabajadores por cuenta propia a “echar el cierre”.\r\n\r\nLa prórroga supone que un mayor número de autónomos pueda seguir acogiéndose a este mecanismo de tributación, que implica asumir menos obligaciones formales que el método de estimación directa. En opinión de ATA, tributar por módulos es más ágil y sencillo y beneficia sobre todo a los autónomos con ingresos más bajos, “ya que permite fijar una cuota fiscal por un criterio objetivo de manera que el trabajador se despreocupa de llevar la contabilidad”, recuerdan desde esta asociación.\r\n\r\nLas cifras oficiales apuntan que de los 3,2 millones de autónomos que existen en España, aproximadamente más de 1,6 millones tributan por el sistema de estimación directa, no llegan a 500.000 quienes lo hacen por el sistema de módulos y el resto como sociedades.\r\n\r\nFraude fiscal\r\n\r\nLa iniciativa de reducir los límites de facturación del sistema de tributación por módulos procede de la reforma fiscal que el Gobierno aprobó en 2015. Fue una de las medidas que se incluyeron para combatir el fraude fiscal. Entonces ya se decidió excluir del sistema de módulos a las actividades a las que se aplica un tipo de retención del 1%. Quedaron fuera negocios como las carpinterías, cerrajerías y profesionales como los albañiles, fontaneros o pintores.\r\n\r\nEl umbral de ingresos que se fijó entonces a partir del cual ya no se podría tributar por módulos pasó de 450.000 a 150.000 euros. No obstante, se determinó que hubiese un régimen transitorio en el que el límite para 2016 y 2017 quedaba establecido en 250.000 euros y no sería hasta 2018 cuando bajaría a los 150.000. Ahora, con la decisión del Consejo de Ministros, la actual situación se mantiene un año más.\r\n\r\n¿En qué consiste el sistema de tributación por módulos? Se trata de un mecanismo sencillo que genera muchas menos cargas administrativas a los trabajadores por cuenta propia en los dos grandes impuestos que han de abonar, como son el IRPF y el IVA. Son contribuyentes que no declaran en función de sus ingresos y gastos reales, sino que lo hacen a partir de variables objetivas como pueden ser los metros cuadrados que ocupa su negocio, el consumo de electricidad o el número de empleados y a partir de esos parámetros se determina la cuota a pagar.\r\n\r\nEn el caso de un bar, por ejemplo, la cuota a abonar a Hacienda variará en función del número de mesas o la longitud de la barra. Así, es frecuente que tributen por módulos numerosos restaurantes, peluquerías, comercios al por menor, pequeños negocios de transporte, enseñanza o mudanzas. Por el contrario, los autónomos que optan por el método de estimación directa deben tributar en función de sus ingresos y gastos reales y además pueden desgravarse los gastos inherentes a su actividad.\r\n\r\nLos inspectores de Hacienda vienen denunciando desde hace tiempo que el sistema de módulos es un importante nicho de fraude fiscal y de facturas falsas. Recuerdan que una práctica muy extendida entre algunos autónomos es la emisión por parte de un trabajador por cuenta propia que tributa por estimación directa de una factura a otro que lo hace por módulos. El primero se puede deducir ese gasto de su factura fiscal sin perjudicar al profesional que paga por módulos, ya que paga los mismos impuestos con independencia de sus ingresos.', 'CincoDías', 'https://cincodias.elpais.com/cincodias/2017/12/28/midinero/1514478334_527994.html?id_externo_rsoc=FB_CM_CD', 'DIC 2017'),
(7, 'Renta', 'Hoy comienza la campaña de la Renta', 'Como se viene anunciando en los medios de comunicación, desde hoy miercoles día 5 de Abril se puede presentar la declaración de la Renta de 2.016; al coincidirnos con el periodo de declaraciones trimestrales y con el cierre de SOCIEDADES nos vemos obligados a comenzar a presentar declaraciones a partir del mes de Mayo, como en años amteriores.\r\n  \r\nNo obstante, se puede ir recopilando la información según la guía adjunta de modo que a partir de Mayo lo podamos presentar cuanto antes. El plazo finaliza el 30 de Junio.\r\n \r\nSe mantienen las mismas deducciones que el año anterior incluidos los gastos médicos (estos también para 2.017), pero debemos justificarlos y no han debido abonarse en metálico./', 'Ecodiario', 'http://www.eleconomista.es/declaracion-renta/noticias/8035526/12/16/Economia-La-campana-de-la-renta-2016-empezara-el-5-de-abril-de-2017-con-la-desaparicion-definitiva-del-programa-Padre.html', 'DIC 2017'),
(8, 'Autónomos', 'Nuevas ayudas para los autónomos', 'Los trabajadores autónomos tendrán en poco tiempo a su disposición una serie de nuevas medidas que suponen el impulso más importante recibido por el colectivo en los últimos años, según las asociaciones del sector. Después de reclamar durante años la modificación de algunos aspectos de la regulación de sus cotizaciones y de su tratamiento fiscal, la negociación entre los principales grupos parlamentarios convertirá en realidad una parte de esas reivindicaciones en la votación de la ley del trabajo autónomo que tendrá lugar en el Senado después de recibir el visto bueno del Congreso.\r\n\r\nLos cambios supondrán una reducción de costes y más flexibilidad para adaptarlos a la actividad que tengan en cada momento. El objetivo es dar un nuevo impulso a los autónomos, cuya cifra se incrementó en el 2016 a un ritmo del 1% y que este año ya ha llegado a 3,2 millones de empleados por cuenta propia dados de alta en la Seguridad Social. Además, la nueva ley ampliará la participación de las asociaciones de autónomos, que han recibido con satisfacción las nuevas medidas. El presidente de la Asociación de Trabajadores Autónomos (ATA), Lorenzo Amor, felicitó a la ministra de Empleo, Fátima Báñez, por el trabajo realizado.\r\nBONIFICACIÓN\r\n\r\nLa tarifa plana pasa de 6 a 12 meses\r\n\r\nUno de los objetivos de las medidas consensuadas por una mayoría parlamentaria que incluye el PP, Ciudadanos, PDECat y PNV es el de favorecer que haya emprendedores por primera vez y como segunda oportunidad. Con esa filosofía, la duración de la tarifa plana de cotización a la Seguridad Social de 50 euros al mes se amplía desde los seis meses actuales hasta los 12 meses. Para las altas como autónomo que no sean por primera vez, se reduce la exigencia de estar de baja como trabajador por cuenta propia de cinco años a dos para tener derecho a la bonificación. Si el autónomo ya percibió la ayuda, el periodo de baja será de tres años. Los fondos para financiar esta bonificación saldrán de los Presupuestos del Estado y no de la Seguridad Social como hasta ahora, lo que contribuirá a aliviar el déficit que soporta el sistema público de pensiones.\r\nNUEVO FORMATO\r\n\r\nTrabajo autónomo a tiempo parcial\r\n\r\nLas modificaciones que se pondrán en marcha próximamente atienden la reivindicación histórica de las organizaciones de autónomos de flexibilizar las cotizaciones para que tengan en cuenta la marcha del negocio del emprendedor. Con esa finalidad, se pretende crear un nuevo formato de autónomos a tiempo parcial y, por tanto, con una cotización reducida a la Seguridad Social. La reforma permitirá también hasta tres altas y bajas en un año de un autónomo.\r\nJUBILACIÓN\r\n\r\nSe podrá cobrar la pensión y trabajar\r\n\r\nDe forma paralela, los autónomos que tengan trabajadores a su cargo y que hayan llegado a la edad legal de jubilación, que ahora va aumentando progresivamente hasta los 67 años, podrán cobrar la pensión y seguir trabajando. Para los autónomos que no tengan ningún empleado, se prevé que puedan seguir trabajando y percibir el 50% de la pensión. Además, se permitirá que los trabajadores por cuenta propia puedan cambiar hasta cuatro veces en un año su base de cotización.\r\nHACIENDA\r\n\r\nDeducción de gastos diarios y en casa\r\n\r\nCuando entren en vigor los cambios, Hacienda permitirá que los autónomos que trabajan en casa se deduzcan en el IRPF un 20% de los gastos de luz, gas, agua y teléfono para recoger la tendencia al alza del profesional &#39;freelance&#39;. Asimismo, se añade la posibilidad de que se puedan deducir hasta 12 euros al día por comer fuera de casa por motivos laborales, aunque con algunos controles y requisitos.\r\nPROTECCIÓN\r\n\r\nCobertura por los accidentes &#39;in itinere&#39;\r\n\r\nLos trabajadores por cuenta propia contarán con una nueva cobertura social al estar protegidos en caso de accidente laboral &#39;in itinere&#39;, es decir, durante el desplazamiento de casa al trabajo y viceversa, y no solo para los siniestros durante la jornada laboral. La modificación iguala la cobertura de los autónomos con la de los asalariados.\r\nEMPLEO\r\n\r\nMenos costes por contratar a familiares\r\n\r\nLas enmiendas consensuadas por una mayoría parlamentaria amplían las bonificaciones por contratar a familiares de primer y segundo grado. El autónomo quedará exento de cotización si formaliza un contrato indefinido y mantiene de forma estable la plantilla durante al menos seis meses, con excepción de despidos disciplinarios o por CAUSAS OBJETIVAS./', 'Europa Press', 'http://www.europapress.es/economia/noticia-nuevas-ayudas-autonomos-entraran-vigor-enero-2018-20171220085147.html', 'ENE 2018'),
(9, 'IVA', 'Aplazamiento del IVA: Se mantiene y no cambia nada', 'A pesar de las noticias publicadas el pasado fin de semana en muchos medios de comunicación haciéndose eco de unas instrucciones de la Agencia Tributaria sobre los aplazamientos a los autónomos, realmente no ha cambiado nada, y nos tememos que de aquí al 30 de Enero se quede todo igual.\r\n\r\nEsa instrucción es una mera explicación del Real Decreto-Ley. La única posibilidad, después de esto ya casi descartada, era que se hubiese publicado un desarrollo reglamentario, que realmente hubiera regulado unas condiciones más favorables para determinados colectivos (autónomos, pymes...) y que ya de paso se hubiera despejado cómo se justifica que las cuotas repercutidas no fueron cobradas.\r\n\r\nEl problema de tanta confusión ha sido que los medios de comunicación se han limitado replicar los titulares de la noticia sin analizar el fondo del asunto.\r\n\r\nEsperando que este mensaje le sea de utilidad, aprovechamos la ocasión para saludarles.', 'GDN', 'http://www.gdnasesoria.com/index.php/blog/75-aplazamiento-de-iva-se-mantiene-inaplazable-y-no-cambia-nada', 'ENE 2018'),
(10, 'Autónomos', 'Nueva normativa Autonomos', 'El Pleno del Senado aprobó este miércoles por unanimidad, con los votos favorables de los 239 senadores presentes, la proposición de Ley de reformas urgentes del trabajo autónomo.\r\n\r\nDe esta manera, al no incorporar ninguna de las 70 enmiendas presentadas en la Cámara Alta, se aprueba de manera definitiva la ley conforme al texto que salió del Congreso de los Diputados y se publicará directamente en el Boletín Oficial del Estado (BOE).\r\n\r\nDurante la sesión plenaria, en la que estuvieron presentes representantes de las organizaciones de autónomos ATA y UPTA, el senador del PP Octavio Adolfo López, quien preside la Comisión de Empleo y Seguridad Social del Senado, presentó la norma que, a su juicio, “resuelve los problemas de un sector importante de la economía” y es “oportuna”.\r\n\r\nPor su parte, desde Ciudadanos, que fue el impulsor de esta nueva ley, el senador Luis Crisol justificó que su grupo no puede aceptar ninguna de las enmiendas presentadas porque “estamos ante una muy buena ley” y, aunque “no es perfecta”, es una “gran conquista” para los autónomos.\r\n\r\nAdemás, dijo que “es ingenuo pensar que esta ley de medidas urgentes solucionaría todos los problemas de los autónomos” pero es “un eslabón” para mejorar la situación de los trabajadores por cuenta propia.\r\n\r\nDesde el Grupo Socialista, el senador Juan Francisco Martínez-Aldama aseguró que la ley tiene “avances que no vamos a negar, pero son avances insuficientes” y “no entran a analizar los problemas de fondo”.\r\n\r\n“Vaya urgencias”, dijo Martínez-Aldama dirigiéndose al PP, en referencia al año que lleva tramitándose la ley, para pedir “menos jabón, menos halagos a los autónomos, y más soluciones”.\r\n\r\nPor su parte, desde el grupo de Podemos, el senador óscar Guardingo criticó que las “prisas” aludidas por algunos grupos para que esta norma entre en vigor el 1 de enero de 2018 son “un poco extrañas”, ya que si se prorrogan los Presupuestos Generales del Estado (PGE) de 2017 estas medidas podrían no entrar en vigor.\r\n\r\nAsimismo, Guardingo señaló que esta ley aporta avances que son “pequeños y minúsculos” pero que “justifica” su apoyo.\r\n\r\nLas medidas de los autónomos\r\n\r\nEn el PP, la senadora Rosario Rodríguez defendió que es una norma “necesaria para avanzar en una regulación más justa para los trabajadores por cuenta propia” y “da voz a los autónomos”. “Es una buena ley” y “aplaudimos el consenso alcanzado en esta Cámara”, dijo Rodríguez.\r\n\r\nAlgunas de las medidas contempladas en esta ley son la ampliación de la tarifa plana; de 50 euros a un año, que los profesionales autónomos puedan cambiar hasta cuatro veces su base de cotización en un año, que puedan darse de alta y de baja en Régimen Especial de Trabajadores Autónomos (RETA) de la Seguridad Social hasta tres veces al año, o la posibilidad de compatibilizar el 100% del trabajo por cuenta propia con la pensión y de desgravarse gastos de manutención.\r\n\r\nOtros beneficios para los trabajadores por cuenta propia incluidos son la reducción a dos años del plazo de espera para que los autónomos que hayan cesado su actividad puedan volver a disfrutar de la tarifa plana, o la deducción en un 20% de los gastos de suministros, como el agua, la luz, la electricidad y la telefonía, siempre que se trabaje desde casa.\r\n\r\nEn paralelo, la subcomisión parlamentaria que estudia la reforma del Régimen Especial de Trabajadores Autónomos (RETA) debe abordar cambios más profundos en este sistema, como definir el tiempo parcial para los autónomos o revisar la figura del autónomo económicamente dependiente o ‘trade’.\r\n\r\nAdemás, el compromiso de los partidos políticos es que la subcomisión emita un informe sobre las modificaciones que se deben acometer en el régimen de los autónomos el año que viene.\r\n\r\nFuente: Diario ABC\r\n\r\n/', 'InfoAutónomos', 'https://infoautonomos.eleconomista.es/blog/novedades-2018-autonomos-pymes/', 'ENE 2018'),
(11, 'Autónomos', 'Autónomos: los grandes cambios en impuestos y Seguridad Social en 2018', 'Con la entrada en vigor de la nueva Ley de Autónomos, este tipo de profesionales enfrentan uno de los años con más cambios a nivel fiscal de los últimos tiempos. Vamos a recopilar a continuación las principales modificaciones a nivel impositivo, y en relación con los gastos en la Seguridad Social. Dos de las partidas que más modificaciones sufren, y que más afectan directamente al bolsillo de los autónomos en 2018.\r\n\r\nA nivel impositivo, las principales modificaciones que afectan a deducciones fiscales en este año son:\r\n\r\nSeguros por enfermedad. Los autónomos con seguro por enfermedad, así como los de su cónyuge o hijos menores de 25 años pueden deducirse el gasto. El máximo es de 500 euros por persona, y el límite 4 personas. Lo que significa que la ley permite deducirse el gasto de hasta 4 personas en seguros de salud, es decir, 2.000 euros.\r\nRecibos de hogar. Una de las medidas mejor acogidas. Los autónomos que trabajen desde su propio domicilio podrán deducirse hasta un 30% de los gastos en suministros del hogar, como luz, telefonía o agua. Para ello, se realiza una proporción entre los metros totales de la vivienda y los empleados a la actividad profesional.\r\nGastos de manutención. Aquellos autónomos que coman en un restaurante podrán deducirse el gasto. Siempre que se realice mediante pago electrónico y se solicite la factura correspondiente, los autónomos pueden deducirse hasta 26,67 euros al día en comida. En caso de estar en el extranjero, la cuantía máxima asciende a 48,08 euros.\r\nLos cambios en la Seguridad Social\r\n\r\nDesde el bufete de Asesoramiento Tributario VallRibera-Baqués, recuerdan que en el ámbito de la Seguridad Social, también se han producido una serie de cambios que afectan directamente a los autónomos. Aunque no son impuestos como tal, el bolsillo de los trabajadores por cuenta propia se verá aumentado, gracias a la puesta en marcha de estas medidas.\r\n\r\nMÁS INFORMACIÓN\r\nLa evolución actual de los autónomos pone en peligro sus propias pensiones\r\nReglas del IRPF en autónomos para 2018\r\nIncentivos para autónomos que ofrece la Seguridad Social en 2018\r\nCotizaciones RETA. Los autónomos pueden abonar sus cuotas por sus días trabajados y no por el mes entero. Además, se pueden dar de alta y de baja hasta tres veces en el año natural, para que puedan escoger en función de sus intereses. De igual manera, se permite cambiar la base de cotización hasta cuatro veces en el mismo ejercicio.\r\n\r\nRetrasos en los pagos. Se ha reducido el recargo por retraso en el pago de las cuotas. Concretamente se ha pasado del 20% al 10% en los ingresos fuera de plazo en el primer mes.\r\n\r\nPluriactividad. Los autónomos que, al mismo tiempo, estén trabajando como asalariados no tendrán que solicitar el exceso de cotización. Con la nueva ley, la devolución se realiza de forma automática, sin la necesidad de tener que solicitarlo.\r\n\r\nBonificación por cuidado de menores. La edad máxima para obtener una bonificación por el cuidado de menores pasa a los 12 años. Las condiciones en caso de maternidad, paternidad, acogimiento o adopción también han mejorado.\r\n\r\nContrato a familiares. Los autónomos que contraten a familiares de hasta segundo grado por consanguineidad o afinidad (incluso parejas de hecho) tendrán una bonificación durante un año.\r\n\r\nJubilación. Los autónomos podrán recibir el 100% de su pensión por jubilación y seguir trabajando. El requisito es tener, al menos, un empleado al cargo./', 'CincoDías', 'https://cincodias.elpais.com/cincodias/2018/01/12/autonomos/1515755550_190254.html', 'ENE 2018'),
(18, 'Hipotecas', 'Las ejecuciones hipotecarias sobre viviendas habituales bajan ', 'El número de ejecuciones hipotecarias sobre viviendas habituales se situó en 10.749 en 2017, cifra un 49,4% inferior a la de 2016, según la estadística de ejecuciones hipotecarias publicada este martes por el Instituto Nacional de Estadística (INE).\r\n\r\nDe este modo, las ejecuciones hipotecarias sobre viviendas habituales encadenan tres años de retrocesos después de que en 2016 y 2015 bajarán un 30,3% y un 12,6%, respectivamente.\r\n\r\nSólo en el cuarto trimestre de 2017 se registraron 2.103 ejecuciones hipotecarias sobre viviendas habituales, un 32,7% más que en el trimestre anterior, pero un 55,2% menos que en el cuarto trimestre de 2016. Según el INE, sólo el 0,01% de las viviendas familiares existentes en España (18.529.700) iniciaron una ejecución hipotecaria entre octubre y diciembre de 2017.\r\n\r\nEl objetivo principal de esta estadística es ofrecer trimestralmente el número de certificaciones de ejecuciones hipotecarias iniciadas e inscritas en los Registros de la Propiedad durante el trimestre de referencia. Estadística recuerda que no todas las ejecuciones de hipoteca terminan con el lanzamiento (desahucio) de sus propietarios.\\n\r\n\r\nEn 2017 se iniciaron 51.999 ejecuciones hipotecarias, un 28,9% menos que en 2016. De ellas, 48.716 afectaron a fincas urbanas (donde se incluyen las viviendas) y 3.283 a fincas rústicas.\r\n\r\nLas ejecuciones hipotecarias sobre fincas urbanas disminuyeron un 29,7% en relación a 2016. Dentro de las fincas urbanas, 21.171 ejecuciones correspondieron a viviendas, cifra que representa el 52,3% del total de ejecuciones hipotecarias registradas en 2017 y que es un 34,2% inferior a la de 2016.\r\n\r\nDentro de las viviendas, las ejecuciones hipotecarias sobre viviendas de personas físicas sumaron 14.102 en 2017 (-48,1%), de las que 10.749 (el 76,2% del total) son viviendas habituales en propiedad y 3.353 no son residencial habitual de los propietarios. Estas últimas bajaron un 43,6% respecto a 2016.\r\n\r\nPor su parte, las ejecuciones hipotecarias sobre viviendas de personas jurídicas totalizaron 13.069 el año pasado, un 7,3% menos que en 2016.', 'elEconomista.es', 'http://www.eleconomista.es/indicadores-espana/noticias/8984161/03/18/Las-ejecuciones-hipotecarias-sobre-viviendas-habituales-bajan-un-494-en-2017-hasta-las-10749.html', 'MAR 2018');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prioridad`
--

DROP TABLE IF EXISTS `prioridad`;
CREATE TABLE IF NOT EXISTS `prioridad` (
  `id_prioridad` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(128) COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `descripcion` varchar(256) COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_prioridad`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla para las prioridades de las tareas';

--
-- Volcado de datos para la tabla `prioridad`
--

INSERT INTO `prioridad` (`id_prioridad`, `nombre`, `descripcion`, `activo`) VALUES
(1, 'baja', 'importancia más baja', 1),
(2, 'medio baja', 'importancia algo superior a la baja', 1),
(3, 'media', 'importancia media', 1),
(4, 'superior media', 'importancia más baja que la superior', 1),
(5, 'superior', 'importancia superlativa', 1),
(6, 'urgente', 'la más importante de todas', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyecto`
--

DROP TABLE IF EXISTS `proyecto`;
CREATE TABLE IF NOT EXISTS `proyecto` (
  `id_proyecto` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(128) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_ini` date NOT NULL,
  `fecha_fin` date NOT NULL,
  `fecha_update` datetime NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_proyecto`),
  KEY `FK_proyecto_usuario` (`usuario_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla de Proyectos BBDD GestApp';

--
-- Volcado de datos para la tabla `proyecto`
--

INSERT INTO `proyecto` (`id_proyecto`, `nombre`, `descripcion`, `fecha_creacion`, `fecha_ini`, `fecha_fin`, `fecha_update`, `usuario_id`, `activo`) VALUES
(1, 'talleres', 'recoger el coche del taller', '0000-00-00 00:00:00', '2018-06-08', '2018-06-18', '0000-00-00 00:00:00', 3, 1),
(2, 'prueba', 'prueba', '2018-06-08 18:53:07', '2018-06-08', '2018-06-21', '2018-06-16 12:51:23', 8, 1),
(3, 'taller', 'recoger el coche del taller', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', '0000-00-00 00:00:00', 6, 1),
(4, 'pruebaxx', 'prueba', '2018-06-08 18:54:33', '2018-06-08', '2018-06-28', '2018-06-16 13:02:29', 8, 1),
(5, 'taller', 'recoger el coche del taller', '0000-00-00 00:00:00', '2018-06-11', '2018-06-18', '2018-06-15 16:18:43', 8, 1),
(6, 'taller', 'recoger el coche del taller', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', '0000-00-00 00:00:00', 8, 0),
(7, 'Test', 'test', '0000-00-00 00:00:00', '2018-08-20', '2018-08-30', '2018-06-16 15:06:10', 8, 1),
(8, 'prueba proyecto', 'prueba proyecto', '2018-06-09 11:57:08', '2018-06-09', '2018-06-09', '2018-06-09 11:57:08', 8, 0),
(9, 'prueba proyecto10 fantástica', 'haciendo ultima prueba para cración de pryeto', '2018-06-09 11:58:55', '2018-06-09', '2018-06-09', '2018-06-15 16:17:57', 8, 0),
(10, 'prueba final completa', 'prueba final completa', '2018-06-09 12:02:24', '2018-06-09', '2018-06-09', '2018-06-09 12:02:24', 8, 1),
(11, 'Proyecto prueba', 'semana x', '2018-06-13 16:09:52', '2018-06-13', '2018-06-13', '2018-06-13 16:10:05', 8, 1),
(12, 'Test', 'Sábado 16', '2018-06-16 13:03:31', '2018-06-16', '2018-06-16', '2018-06-16 13:03:31', 8, 1),
(13, 'test01', 'test01', '2018-06-16 15:07:03', '2018-06-16', '2018-06-16', '2018-06-16 15:07:03', 8, 1),
(14, 'test video', 'test video', '2018-06-16 15:14:22', '2018-03-16', '2018-06-16', '2018-06-16 15:14:22', 8, 1),
(15, 'pescado', 'muerto', '2018-06-16 15:16:43', '2018-06-16', '2018-06-16', '2018-06-16 15:16:43', 8, 1),
(16, 'test02', 'test02', '2018-06-16 15:17:59', '2018-06-16', '2018-06-16', '2018-06-16 15:17:59', 8, 1),
(17, 'test final ', 'test creación', '2018-06-16 15:19:45', '2018-06-20', '2018-06-30', '2018-06-16 15:19:45', 8, 1),
(18, 'test final', 'test final', '2018-06-16 15:53:55', '2018-10-20', '2018-10-25', '2018-06-16 15:53:55', 8, 1),
(19, 'Proyecto Test', 'Test Presentación', '2018-06-17 12:23:56', '2018-06-10', '2018-07-30', '2018-06-17 12:24:45', 8, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

DROP TABLE IF EXISTS `rol`;
CREATE TABLE IF NOT EXISTS `rol` (
  `id_rol` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(128) COLLATE utf8_spanish_ci NOT NULL,
  `siglas` varchar(128) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_rol`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla de Roles BBDD GestApp';

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`id_rol`, `nombre`, `siglas`, `descripcion`, `activo`) VALUES
(1, 'Project Manager', 'PM', 'Encargado de coordinar al equipo y gestionar para cumplir los objetivos de gestión del proyecto ', 1),
(2, 'Functional Manager', 'FM', 'Aprobar y vigilar la utilización de sus recursos contra sus objetivos de negocio', 1),
(3, 'Requester', 'RQ', 'Persona encargada de proponer proyectos internos, gestiona sus solicitudes de forma similar al Project Manager', 1),
(4, 'Resource Manager', 'RM', 'Gestionar el plan de capacidad para asegurar que hay recursos disponibles para atender las previsiones de los proyectos.', 0),
(5, 'Stakeholder', 'SH', 'Categoría futurible', 0),
(6, 'User', 'US', 'Rol para usar la aplicación', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sesion`
--

DROP TABLE IF EXISTS `sesion`;
CREATE TABLE IF NOT EXISTS `sesion` (
  `id_sesion` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime NOT NULL,
  `usuario_id` int(11) NOT NULL,
  PRIMARY KEY (`id_sesion`),
  KEY `usuario_id` (`usuario_id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla de Sesiones BBDD GestApp';

--
-- Volcado de datos para la tabla `sesion`
--

INSERT INTO `sesion` (`id_sesion`, `fecha`, `usuario_id`) VALUES
(1, '0000-00-00 00:00:00', 2),
(2, '2018-06-03 10:12:37', 8),
(3, '2018-06-03 10:12:55', 8),
(4, '2018-06-03 10:13:46', 8),
(5, '2018-06-05 16:55:42', 8),
(6, '2018-06-06 15:04:59', 8),
(7, '2018-06-06 15:16:20', 8),
(8, '2018-06-06 15:19:14', 8),
(9, '2018-06-06 15:20:02', 8),
(10, '2018-06-07 16:43:13', 8),
(11, '2018-06-08 13:08:41', 8),
(12, '2018-06-08 17:06:39', 2),
(13, '2018-06-08 17:07:55', 2),
(14, '2018-06-08 17:12:03', 2),
(15, '2018-06-08 17:15:19', 8),
(16, '2018-06-09 06:32:42', 8),
(17, '2018-06-09 06:37:19', 8),
(18, '2018-06-09 06:38:05', 8),
(19, '2018-06-09 13:44:05', 8),
(20, '2018-06-09 14:24:28', 8),
(21, '2018-06-09 14:29:32', 8),
(22, '2018-06-10 05:53:01', 8),
(23, '2018-06-10 13:59:22', 2),
(24, '2018-06-10 14:01:57', 2),
(25, '2018-06-11 19:07:52', 2),
(26, '2018-06-12 04:58:08', 2),
(27, '2018-06-12 12:41:52', 3),
(28, '2018-06-12 12:46:12', 5),
(29, '2018-06-12 14:24:50', 2),
(30, '2018-06-13 09:52:29', 8),
(31, '2018-06-13 09:53:54', 2),
(32, '2018-06-13 12:24:43', 2),
(33, '2018-06-13 13:57:54', 2),
(34, '2018-06-13 14:08:52', 8),
(35, '2018-06-13 16:05:09', 2),
(36, '2018-06-13 16:14:04', 2),
(37, '2018-06-13 16:17:16', 5),
(38, '2018-06-13 16:18:55', 8),
(39, '2018-06-14 17:48:08', 8),
(40, '2018-06-14 17:52:50', 2),
(41, '2018-06-14 17:59:46', 2),
(42, '2018-06-15 13:15:55', 8),
(43, '2018-06-15 13:33:30', 8),
(44, '2018-06-15 13:36:28', 8),
(45, '2018-06-15 14:04:03', 8),
(46, '2018-06-15 14:24:38', 8),
(47, '2018-06-15 14:35:58', 3),
(48, '2018-06-15 15:58:01', 2),
(49, '2018-06-15 16:11:59', 2),
(50, '2018-06-16 07:55:45', 2),
(51, '2018-06-16 09:44:33', 2),
(52, '2018-06-16 09:57:23', 2),
(53, '2018-06-16 10:01:23', 8),
(54, '2018-06-16 10:48:12', 8),
(55, '2018-06-16 11:01:05', 8),
(56, '2018-06-16 13:52:46', 8),
(57, '2018-06-16 14:49:36', 9),
(58, '2018-06-17 08:55:13', 12),
(59, '2018-06-17 09:03:43', 2),
(60, '2018-06-17 09:23:43', 3),
(61, '2018-06-17 09:45:25', 2),
(62, '2018-06-17 09:55:44', 8),
(63, '2018-06-17 10:22:33', 8),
(64, '2018-06-18 14:28:35', 5),
(65, '2018-06-18 14:28:59', 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarea`
--

DROP TABLE IF EXISTS `tarea`;
CREATE TABLE IF NOT EXISTS `tarea` (
  `id_tarea` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(128) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_final` date NOT NULL,
  `fecha_update` datetime NOT NULL,
  `categoria_id` int(11) NOT NULL,
  `estado_id` int(11) NOT NULL,
  `proyecto_id` int(11) NOT NULL,
  `prioridad_id` int(11) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_tarea`),
  KEY `categoria_id` (`categoria_id`),
  KEY `estado_id` (`estado_id`),
  KEY `tarea_ibfk_0` (`proyecto_id`),
  KEY `FK_tarea_prioridad` (`prioridad_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla de Tareas BBDD GestApp';

--
-- Volcado de datos para la tabla `tarea`
--

INSERT INTO `tarea` (`id_tarea`, `nombre`, `descripcion`, `fecha_creacion`, `fecha_inicio`, `fecha_final`, `fecha_update`, `categoria_id`, `estado_id`, `proyecto_id`, `prioridad_id`, `activo`) VALUES
(1, 'Prueba', 'prueba', '2018-06-09 08:24:49', '2018-06-09', '2018-06-09', '2018-06-15 15:31:06', 1, 2, 4, 4, 1),
(2, 'pisco', 'labis', '2018-06-09 08:27:13', '2018-06-09', '2018-06-09', '2018-06-09 08:27:15', 1, 2, 3, 3, 1),
(3, 'Taller', 'Recoger el miércoles el coche del taller', '2018-04-02 21:01:46', '0000-00-00', '0000-00-00', '0000-00-00 00:00:00', 3, 2, 1, 2, 1),
(4, 'coca', 'cola', '2018-06-09 08:27:53', '2018-06-09', '2018-06-09', '2018-06-09 08:27:53', 3, 1, 3, 1, 1),
(11, 'cachon', 'deos', '2018-06-09 08:28:53', '2018-06-10', '2018-06-12', '2018-06-16 15:05:21', 3, 1, 4, 4, 1),
(12, 'cosa', 'linda', '2018-06-09 08:29:25', '2018-06-09', '2018-06-09', '2018-06-09 08:29:26', 3, 1, 1, 2, 1),
(13, 'Prueba final', 'chore', '2018-06-09 08:29:52', '2012-12-12', '2033-12-14', '2018-06-16 15:05:02', 2, 1, 2, 4, 1),
(15, 'avisos', 'prueba para avisos', '2018-06-09 15:54:03', '2018-06-09', '2018-06-09', '2018-06-09 15:54:04', 3, 3, 10, 3, 1),
(16, 'sadf', 'sadf', '2018-06-09 17:24:11', '1212-12-12', '1213-12-12', '2018-06-09 17:24:11', 1, 1, 1, 1, 1),
(17, 'Prueba de fuego para craeción de tareas', 'creacion de tareas', '2018-06-09 17:30:23', '2018-01-31', '2019-12-01', '2018-06-09 17:30:23', 1, 4, 1, 1, 1),
(18, 'semanal', 'semanal de prueba', '2018-06-13 16:11:26', '2016-10-10', '2018-12-02', '2018-06-13 16:11:26', 1, 1, 1, 1, 1),
(19, 'Test', 'Test', '2018-06-16 13:05:44', '2018-08-18', '2018-08-25', '2018-06-16 13:05:44', 1, 1, 1, 1, 1),
(20, 'test', 'test1', '2018-06-16 15:08:06', '2012-12-15', '2015-10-10', '2018-06-16 15:08:06', 1, 1, 1, 1, 1),
(21, 'gsdf', 'sdfg', '2018-06-16 15:09:24', '2010-10-10', '2222-10-10', '2018-06-16 15:09:24', 1, 1, 1, 1, 1),
(22, 'ghjk', 'ghjk', '2018-06-16 15:10:08', '2018-05-30', '2018-06-16', '2018-06-16 15:10:08', 1, 1, 1, 1, 1),
(23, 'test', 'tes', '2018-06-16 16:16:35', '1212-12-12', '1212-12-13', '2018-06-16 16:16:35', 2, 1, 4, 1, 1),
(24, 'Test Tarea', 'Test', '2018-06-17 12:25:42', '2018-06-10', '2018-06-30', '2018-06-17 12:25:42', 3, 1, 7, 4, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE IF NOT EXISTS `usuario` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(128) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` varchar(256) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(128) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(256) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` int(11) DEFAULT NULL,
  `ultimo_acceso` date NOT NULL,
  `fecha_registro` date NOT NULL,
  `contador_accesos` int(11) NOT NULL,
  `contador_accesos_fallidos` int(11) NOT NULL,
  `rol_id` int(11) NOT NULL DEFAULT '6',
  `bloqueado` tinyint(4) NOT NULL DEFAULT '0',
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_usuario`),
  UNIQUE KEY `email` (`email`),
  KEY `FK_usuario_rol` (`rol_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla de Usuarios BBDD GestApp';

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `nombre`, `apellidos`, `email`, `password`, `telefono`, `ultimo_acceso`, `fecha_registro`, `contador_accesos`, `contador_accesos_fallidos`, `rol_id`, `bloqueado`, `activo`) VALUES
(1, 'pruebas', 'prueba', 'prueba@prueba.com', '', 789789789, '0000-00-00', '0000-00-00', 0, 0, 3, 0, 1),
(2, 'Jose María', 'Cagigas Céspedes', 'asdfjklñ@gmail.com', 'EScJjCa1FQYYNBqVd5t2G1UFpPStaxpO07m20OR5ibxTm8rvuo06g1kyYKCTdYIS13b05b30069fa4474555880dc92e8621a0d148cb5d85bf640b78d157d92a50c4', 942622390, '2018-06-17', '0000-00-00', 26, 0, 2, 0, 1),
(3, 'Chema', 'Cagigas Céspedes', 'facebuk@gmail.com', 'EScJjCa1FQYYNBqVd5t2G1UFpPStaxpO07m20OR5ibxTm8rvuo06g1kyYKCTdYIS13b05b30069fa4474555880dc92e8621a0d148cb5d85bf640b78d157d92a50c4', NULL, '2018-06-17', '0000-00-00', 3, 0, 3, 0, 1),
(4, 'Chema', 'adistancia', 'proyecto@adistancia.educantabria.es', '', 609116315, '0000-00-00', '0000-00-00', 0, 0, 4, 1, 1),
(5, 'Jose María', 'Cag Césp', 'cursojava@gmail.com', 'EScJjCa1FQYYNBqVd5t2G1UFpPStaxpO07m20OR5ibxTm8rvuo06g1kyYKCTdYIS13b05b30069fa4474555880dc92e8621a0d148cb5d85bf640b78d157d92a50c4', 666999933, '2018-06-18', '0000-00-00', 3, 0, 1, 0, 1),
(6, 'asdfasdf', 'asdfasdf', 'cagigas@gmail.com', '', 693693693, '0000-00-00', '0000-00-00', 0, 0, 6, 0, 1),
(7, 'Mitos', 'estudiantes', 'chemaca@gmail.com', 'TOdKknzdjSe5AEJjdvWXe8oLPP8LSEdCN9HUA7fZcyyjt0NQ9BekkOxEIp8n3uUSe234fdcb995b13fa0901f340ff485b6cc26d0d1f5237d89749399061d1e3af48', 10000909, '2018-00-00', '2018-06-02', 2, 3, 6, 1, 1),
(8, 'estudiante', 'estudiante', 'estudiante@asdf.com', 'kN4zcGQNqnOjPN6fPl0DPCDveJSVBP1Z10EYtCicnhtkB9e5ufmsqco7x2j9Or8x661d39328f24347c4ecb670ca04153d242b4a635cdfffc87700588443c11bb62', 609116315, '2018-06-18', '2018-06-02', 39, 0, 6, 0, 1),
(9, 'manuel', 'carrete', 'esto@toca.com', '8E73UfYdBwqKSju4RXjqrB5xmRRGguW2KFCvz9JBdfFy73UQorPW6zudpijCo78Pbf23ba63fcdc6d9098e2787247db21095548c72139f735659f5e65f1f47dd774', 2147483647, '2018-06-16', '2018-06-16', 1, 0, 6, 0, 1),
(10, 'Manuel', 'Garcia', 'manuel@garcia.com', 'LUESl5rZiWtETNtSW8gmjOQG5NAeusF6Ib71eyyWeNJfWfnPU4tRdokP8rU0nG3ldb3c7201fd73831621b62cc00f1de06a15f05370a01419f923b8950e002c71d9', 609148148, '0000-00-00', '2018-06-17', 0, 0, 6, 0, 1),
(11, 'Jesús ', 'Mariñas', 'jesus@gmail.com', '3Sndup9armYreyMVWalU0XGbG4zrs1cjHcW5B5fEpujc8KshAOVEXRmNupLiQZJKabef05cfa0736f7e107d4bb831958fe5f3c3e61db3875a1a91950384cd1af44c', 639639639, '0000-00-00', '2018-06-17', 0, 0, 6, 0, 1),
(12, 'Manuel', 'García', 'manuel@gmail.com', 'Ld7VPHG5Jc3tRoX4MlkWwNGMfz49D1MyeSVxmoEppTvup3Db3ztRgQx5zmh8DkDj0385d2270febf4ca73b9d75ec63a822e1449a7ef9495396c2a6e0637304615ac', 639639639, '2018-06-17', '2018-06-17', 1, 0, 6, 0, 1);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `ambito_traduccion`
--
ALTER TABLE `ambito_traduccion`
  ADD CONSTRAINT `ambito_traduccion_ibfk_1` FOREIGN KEY (`ambito_id`) REFERENCES `ambito` (`id_ambito`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ambito_traduccion_ibfk_2` FOREIGN KEY (`idioma_id`) REFERENCES `idioma` (`id_idioma`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `error`
--
ALTER TABLE `error`
  ADD CONSTRAINT `FK_error_usuario` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id_usuario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `log`
--
ALTER TABLE `log`
  ADD CONSTRAINT `log_ibfk_1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id_usuario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `nota`
--
ALTER TABLE `nota`
  ADD CONSTRAINT `FK_nota_usuario` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id_usuario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `proyecto`
--
ALTER TABLE `proyecto`
  ADD CONSTRAINT `FK_proyecto_usuario` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id_usuario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `sesion`
--
ALTER TABLE `sesion`
  ADD CONSTRAINT `sesion_ibfk_1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id_usuario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tarea`
--
ALTER TABLE `tarea`
  ADD CONSTRAINT `tarea_ibfk_0` FOREIGN KEY (`proyecto_id`) REFERENCES `proyecto` (`id_proyecto`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tarea_ibfk_1` FOREIGN KEY (`categoria_id`) REFERENCES `categoria` (`id_categoria`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tarea_ibfk_2` FOREIGN KEY (`estado_id`) REFERENCES `estado` (`id_estado`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tarea_ibfk_3` FOREIGN KEY (`prioridad_id`) REFERENCES `prioridad` (`id_prioridad`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `FK_usuario_rol` FOREIGN KEY (`rol_id`) REFERENCES `rol` (`id_rol`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
