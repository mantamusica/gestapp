<?php
include 'bookstores/xajax/xajax_core/xajax.inc.php';

$xajax = new xajax('FunctionsXajax.php');

$xajax->register(XAJAX_FUNCTION, "validationLogIn");
$xajax->register(XAJAX_FUNCTION, "validationEmail");
$xajax->register(XAJAX_FUNCTION, "sendEmail");
$xajax->register(XAJAX_FUNCTION, "compareKey");
$xajax->register(XAJAX_FUNCTION, "passwordEncoded");
$xajax->register(XAJAX_FUNCTION, "generateSession");
$xajax->register(XAJAX_FUNCTION, 'chainRandom');
$xajax->register(XAJAX_FUNCTION, 'saveIn');
$xajax->register(XAJAX_FUNCTION, 'sendMail');


$xajax->configure('javascript URI', './bookstores/xajax/');

//Debug xajax
//$xajax->configure('debug', true);

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_set_cookie_params(0,"/");
session_start();
header('Content-Type: text/html; charset=UTF-8');

include("includes/include_base.php");
include('functions/php/functions.php');

if (!isset($_SESSION["login"]) || $_SESSION["login"] == "") {
    header("location: login.php");
}else{
    if(!isset($_SESSION['login']['rol'])|| $_SESSION['login']['rol'] == "")
    {
        ?><<script>
        alert('Ha habido algún problema reconociendo sus datos, inténtelo de nuevo y si persiste el problema póngase en contacto con el administrador.')
    </script><?php
        session_unset();
        header("location: login.php");
    }
    else
    {
        $rol = $_SESSION['login']['siglas'];
        $id = $_SESSION['login']['id_usuario'];
        $_SESSION['cantidad'] = 5;
    }
}

?>
<!DOCTYPE html>
<html >
    <head>
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link rel="shortcut icon" href="img/favicon.ico">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <link rel="stylesheet" href="css/menu.css">
            <link rel="stylesheet" href="css/container.css">
            <link rel="stylesheet" href="css/footer.css">
            <link rel="stylesheet" href="css/form.css">
            <link rel="stylesheet" href="css/estilos.css">
            <script src="bookstores/jquery/jquery-3.3.1.min.js" type="text/javascript"></script>
            <script src="functions/js/views.js" type="text/javascript"></script>
            <script src="functions/js/scripts.js" type="text/javascript"></script>
            <script>
                $(document).ready(function() {
                    var id = <?php echo $id; ?>;
                    var rol = '<?php echo $rol; ?>';
                });
                function getInformation(action, numero){
                    {
                        var id = <?php echo $id; ?>;
                        var rol = '<?php echo $rol; ?>';

                        $.ajax({
                            url: 'functions/php/functionCargar.php',
                            type: "POST",
                            data: "rol="+rol+"&action="+action+"&numero="+numero+"&id="+id+"&post_accion=cargar",
                            //dataType: "json",
                            success: function(data){
                                $("#results").html(data);
                            }
                        });
                    }
                }
                function error_sistema(mensaje, archivo, linea){
                    var id = <?php echo $id; ?>;
                    $.ajax({
                        url: 'functions/php/functionShow.php',
                        type: "POST",
                        data: "mensaje="+mensaje+"&archivo="+archivo+"&linea="+linea+"&id="+id+"&post_accion=error",
                        //dataType: "json",
                        success: function(data){
                            $("#results").html(data);
                        }
                    });
                }
            </script>
            <?php $xajax->printJavascript(); ?>
            <title>GestApp</title>
    </head>
    <body>
    <?php 
        if($rol === 'PM')
        {
            include ('modules/menu/menuPM.php');
            include ('modules/body/bodyPM.php');
        }
        elseif($rol === 'FM')
        {
            include ('modules/menu/menuRQ.php');
            include ('modules/body/bodyFM.php');
        }
        elseif($rol === 'RQ')
        {
            include ('modules/menu/menuRQ.php');
            include ('modules/body/bodyFM.php');
        }
        else
        {
            include ('modules/menu/menu.php');
            include ('modules/body/body.php');
        }
    include('modules/body/footer.php');
   ?>

    </body>
</html>
