<?php
/**
 * Created by PhpStorm.
 * User: Jose María Cagigas Céspedes
 * Date: 07/04/2018
 * Time: 10:59
 */

$array_inyeccion_sql = array("select", "insert", "update", "delete", "union", "information_schema", "concat_ws", "order+by", "order%20by", "--");
$parametros_url = $_SERVER['QUERY_STRING'];

foreach ($array_inyeccion_sql as $clave=>$valor)
{
    if (stripos($parametros_url, $valor))
    {
        exit();
    }
}
?>