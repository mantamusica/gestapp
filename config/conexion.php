<?php
/**
 * Created by PhpStorm.
 * User: Jose María Cagigas Céspedes
 * Date: 07/04/2018
 * Time: 10:14
 */
try
{
    $link = new PDO('mysql:host='.$mysql_host.';dbname='.$mysql_bd.'', $mysql_user, $mysql_pass,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
    $link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch (PDOException $e)
{
    echo "Error conectando a la base de datos";
    exit();
}
?>