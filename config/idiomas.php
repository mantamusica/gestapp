<?php
/**
 * Created by PhpStorm.
 * User: Jose María Cagigas Céspedes
 * Date: 07/04/2018
 * Time: 11:40
 */

$sql_idioma=$link->prepare("SELECT * FROM idioma WHERE activo=1 ORDER BY id ASC");
$sql_idioma->execute();
$contador_idiomas = 0;

while($datos_idioma=$sql_idioma->fetch())
{
    $array_idiomas[$contador_idiomas]['id'] 					= $datos_idioma['id'];
    $array_idiomas[$contador_idiomas]['nombre'] 				= $datos_idioma['nombre'];
    $array_idiomas[$contador_idiomas]['abreviatura'] 			= $datos_idioma['abreviatura'];
    $array_idiomas[$contador_idiomas]['bandera'] 				= $datos_idioma['bandera'];

    ++$contador_idiomas;
}

if(!empty($_COOKIE['idioma_id'])) $idioma_id = $_COOKIE['idioma_id'];
else $idioma_id = $datos_parametros['idioma_defecto'];

for ($i=0; $i<count($array_idiomas); ++$i)
{
    if($array_idiomas[$i]['id'] == $idioma_id) $idioma_abreviatura = $array_idiomas[$i]['abreviatura'];
}

putenv("LC_ALL=$idioma_abreviatura");
setlocale(LC_ALL, $idioma_abreviatura);
bindtextdomain("messages", $datos_parametros['locale']);
textdomain("messages");
