<?php
/**
 * Created by PhpStorm.
 * User: Jose María Cagigas Céspedes
 * Date: 07/04/2018
 * Time: 10:36
 */
ini_set('display_errors',1);
error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING & ~E_DEPRECATED);

set_time_limit(0);
session_start();
header("Content-type: text/html; charset=utf-8");

//********************************************* BD ******************************************
// Producción
$mysql_host = "localhost";
$mysql_user = "root";
$mysql_pass = "";
$mysql_bd = "1718d0_josem1";
//*******************************************************************************************

include('inyeccion_sql.php');
?>
